---
layout: page
title: Announcements
menuOrder: 20
---

{% assign announcements = site.announcements | sort: 'post-date' | reverse %}

{% for page in announcements %}
- [{{ page.title }}]({{ page.url }}){% endfor %}