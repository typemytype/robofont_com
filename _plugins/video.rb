require_relative "helpers"

module Jekyll
  class VideoHelper < Liquid::Tag

    include HelpersTools

    def initialize(tag_name, text, tokens)
      super
      text = text.strip()
      parts = text.split(" ")
      @videoPath = parts[0]
    end

    def render(context)
      path = File.join("videos", @videoPath)
      path = context.evaluate(path)
      unless File.exist?(path)
        pinkWarning("Video not found: '#{path}' in #{context.registers[:page]["path"]}")
        ""
      end
      baseurl = context.registers[:site].baseurl

      # <video width="100%" controls loop muted>
      #   <source src="pathTo.mp4" type="video/mp4">
      # </video>

      output = "<video width=\"100%\" controls loop muted><source src=\"#{baseurl}/videos/#{@videoPath}\" type=\"video/mp4\"></video>"
      return output
    end

  end
end

Liquid::Template.register_tag('video', Jekyll::VideoHelper)

