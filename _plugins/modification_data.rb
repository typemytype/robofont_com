# modified from https://github.com/gjtorikian/jekyll-last-modified-at/blob/master/lib/jekyll-last-modified-at/determinator.rb

require_relative "executor"


module Jekyll
    class ModificationDate < Liquid::Tag

        include Executor

        def render(context)
            currentPage = context.registers[:page]
            @fileName = currentPage["path"]

            if File.exist?(@fileName)
                @site_source = context.registers[:site].source
                @absolute_path_to_article ||= Jekyll.sanitized_path(@site_source, @fileName)
                if is_git_repo?(@site_source)
                    modTime = Executor.sh(
                        'git',
                        '--git-dir',
                        top_level_git_directory,
                        'log',
                        '--format="%ct"',
                        '--',
                        relative_path_from_git_dir
                      )[/\d+/]
                      # modTime can be nil iff the file was not committed.
                      (modTime.nil? || modTime.empty?) ? File.mtime(@fileName) : modTime
                      modTime = Time.at(modTime.to_i)
                else
                    modTime = File.mtime(@fileName)
                end
            else
                modTime = Time.now
            end
            modTime.strftime("Last edited on %d/%m/%Y")
        end

        def is_git_repo?(site_source)
            @is_git_repo ||= begin
                Dir.chdir(site_source) do
                    Executor.sh("git", "rev-parse", "--is-inside-work-tree").eql? "true"
                end
            rescue
                false
            end
        end

        def top_level_git_directory
            @top_level_git_directory ||= begin
              Dir.chdir(@site_source) do
                top_level_git_directory = File.join(Executor.sh("git", "rev-parse", "--show-toplevel"), ".git")
                end
            rescue
                ""
            end
        end

        def relative_path_from_git_dir
            return nil unless is_git_repo?(@site_source)
            @relative_path_from_git_dir ||= Pathname.new(@absolute_path_to_article)
                .relative_path_from(
                    Pathname.new(File.dirname(top_level_git_directory))
                ).to_s
          end
    end
end

Liquid::Template.register_tag('modification_date', Jekyll::ModificationDate)