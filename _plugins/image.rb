require_relative "helpers"

module Jekyll
  class ImageHelper < Liquid::Tag

    include HelpersTools

    def initialize(tag_name, text, tokens)
      super
      text = text.strip()
      parts = text.split(" ")
      @imagePath = parts[0]
      @rawCaption = nil
      args = parts[1..-1].join(" ")
      args = args.strip().split("=")
      if args[0] == "caption"
        @rawCaption = args[1]
      end
    end

    def render(context)
      path = File.join("images", @imagePath)
      path = context.evaluate(path)
      unless File.exist?(path)
        pinkWarning("Image not found: '#{path}' in #{context.registers[:page]["path"]}")
        ""
      end
      baseurl = context.registers[:site].baseurl
      caption = get_value(context, @rawCaption)
      output = "<div><div class=\"image\"><img src=\"#{baseurl}/images/#{@imagePath}\">"
      unless caption.nil?
        output += "<div class=\"image-caption\">#{caption}</div>"
      end
      output += "</div></div>"
      return output
    end

  end
end

Liquid::Template.register_tag('image', Jekyll::ImageHelper)

