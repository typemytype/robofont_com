var Shuffle = window.Shuffle
var element = document.querySelector('.sorter')

var shuffleInstance = new Shuffle(element, {
  itemSelector: '.sorter-item',
  useTransforms: false,
  gutterWidth: 0,
  speed: 0,
  staggerAmount: 0,

})
shuffleInstance.layout()

function defaultFilterItems() {
    values = []
    $(".filter :checked").each(function () {
        values.push($(this).val())
    })
    shuffleInstance.filter(values)
    return values
}

filterItems = defaultFilterItems

function setFilterItems(func) {
    filterItems = func
}

$(".sorter-checkbox").click(function() {
    selected = filterItems()
    if (selected.length) {
        window.location.hash = "#" + selected.join("&")
    }
    else {
        history.pushState("", document.title, window.location.pathname + window.location.search);
    }
})
$(".reset").click(function () {
    $(".sorter-checkbox :checked").each(function () {
        this.checked = false
    })
  shuffleInstance.filter()
  history.pushState("", document.title, window.location.pathname + window.location.search);
})

activeTags = window.location.hash.replace("#", "").split("&")
found = 0
for (i = 0; i < activeTags.length; i++) {
    tag = $("#" + decodeURIComponent(activeTags[i]))
    if ( tag.length ) {
        tag.prop('checked', true)
        found = tag
    }
}
if ( found ) {
    filterItems()
}

function showHideElements(item, tag) {
  obj = $(tag)
  if ( item.checked ){
      obj.show()
    }
  else {
    obj.hide()
    $(tag + " .sorter-checkbox :checked").each(function () {
        this.checked = false
    })
    filterItems()
  }
}