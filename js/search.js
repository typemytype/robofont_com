---
excludeSearch: true
excludeNavigation: true
---
{%- assign items = site.pages -%}
{%- for collection in site.collections -%}
  {%- for item in collection.docs -%}
    {%- assign items = items | push:item -%}
  {%- endfor -%}
{%- endfor -%}
{%- assign tags = "" | split:"|" -%}
{%- for item in items -%}
  {%- for tag in item.tags -%}
    {%- assign tags = tags | push: tag -%}
  {%- endfor -%}
{%- endfor -%}
{%- assign tags = tags | uniq | sort -%}
tagColorMap = {
  {% for tag in tags %}
  "{{ tag }}": "{{ forloop.index | times: 300 | divided_by: forloop.length }}"{% unless forloop.last %},{% endunless %}{% endfor %}
}

searchData = {
  {%- for item in items -%}
    {%- if item.title -%}
    {%- unless item.draft-hidden -%}
    {%- unless item.excludeSearch -%}
    {%- assign parts = item.url | split:"/" -%}
    {%- assign partsCount = parts | size -%}
    {%- assign section = "" -%}
    {%- case parts[1] -%}
    {%- when "documentation" -%}
      {%- if partsCount > 2 -%}
        {%- assign section = parts[2] -%}
      {%- endif -%}
    {%- endcase -%}
    {{ item.url | slugify | jsonify }}: {
        "title": {{ item.title | jsonify }},
        "url": {{ item.url | remove: 'index' | prepend: site.baseurl | jsonify }},
        "tags": {{ item.tags | join: ", " | jsonify }},
        "section" : {{ section | jsonify }},
        "description": {% if item.description %}{{ item.description | jsonify }}{% else %}""{% endif %},
        {% if item.generatedPage %}
        "content": {{ item.documentation | append: item.objects | append: item.name | replace: "&#123;", " " | replace: "&#124;", " " | replace: "[", " " | replace: "]", " " | replace: "(", " " | replace: ")", " " | replace: ";", " " | replace: "=>", " " | replace: "&#x5c;", " " | replace: "`", " " | replace: '"', " " | replace: "\'", " " | strip_html | strip_newlines | jsonify }}
        {%- else -%}
        "content": {{ item.content | strip_html | strip_newlines | jsonify }}
        {%- endif -%}
    }{% unless forloop.last %},{% endunless %}
    {%- endunless -%}
    {%- endunless -%}
    {%- endif -%}
  {%- endfor -%}
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

$(document).ready(function () {
  // Set up search
  var index, store;
  index = lunr(function () {
    this.ref('id')
    this.field('title')
    this.field('content')
    // this.field('url')
    this.field('tags')

    for (id in searchData) {
        data = searchData[id];
        data.id = id;
        this.add(data);
    };
  });
  console.log(index)

  $("#search_box").keyup(function(){
      query = $("#search_box").val();
      results = index.search(query);
      display_search_results(results, query);
  });
  $("#s").keyup(function(){
      query = $("#s").val();
      results = index.search(query);
      display_search_results(results, query);
  });
  $("#site_search").submit(function(){
      event.preventDefault();
  });
  $("#search").submit(function(){
      event.preventDefault();
  });

  function display_search_results(results, query) {
      var search_results = $("#search_results");
      $("#search_term").html(": <span class='search-query'>" + query + "<span>");
      // Are there any results?
      search_results.empty();
      console.log(results)
      if (results.length) {
        search_results.empty();
        search_results.append("<p>Search finished, found " + results.length + " page(s) matching the search query.</p>")
        // Iterate over the results
        results.forEach(function(result) {
          var item = searchData[result.ref];
          // Build a snippet of HTML for this result
          var appendString = '<ul class="search"><li class="' + item.category + '"><a href="' + item.url + '?highlight=' + query + '">' + item.title + '</a></li></ul>';
          var tagColors = ""
          var tagString = ""
          tags = item.tags.split(", ")

          for (tagIndex = 0; tagIndex < tags.length; tagIndex++) {
            tag = tags[tagIndex]
            if ( tag ) {
              tagString += '<div class="tag"><a href="/documentation/tags/#' + tag.replaceAll(' ', '-') + '">' + tag + '</a></div>'
              tagColors += '<div style="background-color:hsla(' + tagColorMap[tag] +', 100%, 50%, 0.2);"></div>'
            }
            else {
              tagColors += '<div style="background-color:rgba(0,0,0,0.03);"></div>'
            }
          }

          // else {
          //   tagColors = '<div style="background-color:rgba(0,0,0,0.1);"></div>'
          // }
          var sectionString = ""
          if ( item.section ) {
            sectionString = '<a href="/documentation/' + item.section.replaceAll(' ', '-') + '">' + item.section + '</a>'
          }
          var descriptionString = ""
          if (item.description ) {
            descriptionString = '<div class="sorter-item-description">' + item.description + '</div>'
          }

          var appendString = '<div class="sorter-item"><div class="sorter-item-color">' + tagColors + '</div><div class="sorter-item-inner"><div class="sorter-item-title"><a href="' + item.url + '">' + item.title + '</a></div><div class="sorter-item-info"><div class="sorter-item-subinfo"><div class="sorter-item-section">' + sectionString +'</div><div class="sorter-item-tags">' + tagString + '</div></div></div></div>' + descriptionString + '</div>'
          // Add it to the results
          search_results.append(appendString);
      });

      } else if (query == "") {
          search_results.empty();
      }

      else {
          search_results.html('<div>No results found</div>');
      }
  };

  var query = getUrlParameter("search")
    if ( query ) {
      var item = $("#search_box")
      item.removeAttr('placeholder')
      query = query.replace(/\+/g, ' ')
      item.val(query)
      item.keyup()
    }
})
