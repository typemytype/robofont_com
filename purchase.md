---
excludeNavigation: true
excludeSearch: true
---

<html>
 <head>
     <meta charset="utf-8"/>
     <meta http-equiv="refresh" content="1;url={{ site.purchaselink }}"/>
     <link rel="canonical" href="{{ site.purchaselink }}"/>
     <script type="text/javascript">
             window.location.href = "{{ site.purchaselink}}"
     </script>
     <title>Page Redirection</title>
 </head>
 <body>
 If you are not redirected automatically, follow <a href='{{ site.purchaselink }}'>this link</a>.
 </body>
 </html>