---
layout: page
title: RoboFont 4.2
post-date: 2022-04-19
---

Dear users,
RoboFont 4.2 is out! Lots and lots of bug fixes, user interface improvements and a few changes to the APIs.

Here's a list with some of most important updates:

{% image announcements/layers-popover.png %}

- Layers management moves to a {% internallink "/reference/workspace/layers-popover" text="popover" %} accessible from the Glyph Editor and the Font Overview.
- It is now possible to {% internallink "/reference/workspace/layers-popover#global-layers-sync-settings" text="synchronize" %} mark color, unicode and width across layers
- In the glyph editor, you can now jump to a component base glyph with alt ⌥ down + triple click
- The Space Center supports the `/?@layerName` {% internallink "/reference/workspace/space-center#text-input-fields" text="syntax" %}. It shows the current glyph from a specific layer
- The Subscriber API gets two new shiny functions:
    - `mojo.subscriber.listRegisteredSubscribers` lists all the active subscribers
    - `with disableSubscriberEvents()` context disables all active subscribers while the context is active. This is especially useful to avoid circular events when a subscriber triggers another subscriber, that might trigger another one, and so on. Here an {% internallink "/how-tos/mojo/space-center#synchronizing-multiple-space-centers" text="example" %} on how to use it
- The SmartSet API gets a new editing context `with SubscriberEditor()`. Check {% internallink "/how-tos/mojo/smart-sets" text="here" %} a few examples on how to manipulate programmatically smart sets
- Save test installed fonts to a folder inside the application folder

{% image announcements/detached_first_contour_point-resized.png %}

- According to UFO3 spec, outlines can have an off-curve first point. It may sound silly, but there are reasons for that. Check the updated {% internallink "/reference/workspace/glyph-editor/contours#first-contour-point" text="docs" %}.


**Keep in mind that the minimum macOS version has been raised to 10.11**.

Check the [release notes](/version-history/)

Download available from [robofont.com](/download)
