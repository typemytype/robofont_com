---
layout: page
title: RoboFont 3.3 Beta
post-date: 2019-07-03
---


Four months after the release of [RoboFont3.2], we’re happy to announce that the first public beta of RoboFont 3.3 is out.

[Download it now from the Downloads page.][Downloads]

This release is packed with new features and bug fixes. Here are some of the highlights:

[RoboFont3.2]: http://forum.robofont.com/topic/564/robofont-3-2
[Downloads]: /downloads/#beta-releases

### New banner notifications

Messages are now shown using [macOS notifications] instead of alert windows. A small change which saves clicks and improves the user experience.

{% image announcements/3-3b-image.png %}

Notifications are *unobtrusive*: they appear at the top right of the screen and disappear automatically after a few seconds.

You can also use them in your tools and scripts:

```python
from mojo.UI import PostBannerNotification
PostBannerNotification("hello world", "Welcome to RoboFont!")
```

[macOS notifications]: http://support.apple.com/en-us/HT204079

### Support for anchor and guideline colors

This release adds support for displaying individual guideline and anchor colors in the [Glyph Editor].

{% image announcements/3-3b-image-2.png %}

The [Inspector] panel has a new *Guidelines* section, where color and other guideline attributes can be edited. Anchor colors can be edited in the Inspector too.

*This feature was added in response to a [question in the Forums].*

[Glyph Editor]: /documentation/workspace/glyph-editor/
[Inspector]: /documentation/workspace/inspector/
[question in the Forums]: http://forum.robofont.com/topic/589/guidelines-color-and-measurements

### New index color preferences

Speaking of colors: the index labels for different types of objects (points, segments, contours, components) can now have different colors.

{% image announcements/3-3b-image-3.png %}

The new color settings are available in *Preferences > Glyph View > Appearance*.

### New diff view for external glyph changes

The *Updates Found* window has been improved with a new diff view for visualising external changes to glyphs:

{% image announcements/3-3b-image-4.png %}

The diff popup can be opened by clicking on a glyph name in the list of changed glyphs. Green lines show the external `glif` XML data, red lines show the current glyph data which will be modified.

*This feature was added in response to a [request in the Forums].*

[request in the Forums]:
http://forum.robofont.com/topic/587/feature-request-visual-previews-in-the-updates-found-popup

### Extension expiration date and markdown docs

Extension metadata files can now include an *expiration date* attribute, allowing developers to create trial versions of their extensions.

The `ExtensionBundle` object now also supports documentation sources in [Markdown] format. If available, Markdown files are converted to HTML when the extension is built. A default stylesheet is applied, but you can also provide your own.

See the [Boilerplate Extension] and [Building Extensions] for updated documentation and examples.

[Markdown]: https://en.wikipedia.org/wiki/Markdown

- - -

Other changes in this version include:

- add missing glyphs as template glyphs when sorting by character set
- improvements to group kerning in the [Kern Center]
- support for drag & drop in canvas views
- new flat UI style for accordion views
- anchors snapping to contour points
- updated embedded libraries

See the [version history] for the complete list of changes.

*A big thanks to all beta testers and users providing feedback on the [Forum]!*

[Kern Center]: /documentation/workspace/kern-center/
[version history]: /version-history/
[Forum]: http://forum.robofont.com/
[Boilerplate Extension]: http://github.com/roboDocs/rf-extension-boilerplate/
[Building Extensions]: /documentation/building-tools/extensions/