---
layout: page
title: RoboFont 1.7
post-date: 2016-05-08
---

{% image announcements/updaterobofont1_7.gif %}

RoboFont became better than ever! FOUR times faster! and lots of improvements, new features and minor bug fixes...

Have a look at the [version history](/version-history) for the full list of changes and additions.

You should get an automatic update, if not you should [redownload](/download) the app.

This update supports OSX 10.9+

I would like to thank all the beta testers and all users posting on the [forums](http://forum.robofont.com) for their precious feedback and feature requests.

enjoy!