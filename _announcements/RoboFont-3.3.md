---
layout: page
title: RoboFont 3.3
post-date: 2019-11-06
---

Today we’re proud to announce the official release of RoboFont 3.3, wrapping up all the work which was done since the release of RF 3.2 in January.

[download RoboFont 3.3](/downloads/)

RoboFont 3.3 consolidates the move to Python3 and UFO3: the embedded Python was upgraded to 3.7, the codebase was cleaned up from legacy Python 2 code, and more UFO3 attributes can now be edited by the application.

Here’s an overview of the main changes in this release:

## Interface

### More colorful

{% image announcements/image.png %}

The {% internallink "/workspace/glyph-editor" text="Glyph Editor" %} now supports individual colors for guidelines and anchors, as defined by the UFO3 spec. Guideline colors can be edited in the Inspector with a new *Guidelines* panel.

The {% internallink "/workspace/preferences-window/glyph-view#appearance" text="Glyph View Preferences" %} now offers separate settings for point, segment, contour and component indexes.


### More elegant

The {% internallink "/how-tos/mojo/accordion-window" text="Accordion" %} widget – used by the Inspector panel, the Batch extension and many other tools – has a new flat style which makes the interface feel lighter and cleaner.

{% image announcements/screen-shot-2019-11-06-at-07.40.41.png %}

### More precise

{% image announcements/image-2.png %}

The Updates Found window has a new diff view to visualise external changes to glyphs – so you can know exactly where and how your [glif] data is changing.

[glif]: http://unifiedfontobject.org/versions/ufo3/glyphs/glif/

### More quiet

{% image announcements/image-3.png %}

RoboFont 3.3 adds support for [macOS notifications], which disappear automatically after a few seconds and can be configured in the macOS Preferences. A `PostBannerNotification` object was also added to `mojo.UI`, so you can use notifications in your tools too.

[macOS notifications]: http://support.apple.com/en-us/HT204079

Read more about the interface updates in the [RoboFont 3.3b anouncement](https://forum.robofont.com/topic/651/robofont-3-3b).

## APIs

{% image announcements/image-4.png %}

RoboFont3.3 upgrades the embedded Python from 3.6.5 to 3.7.4 – see [What’s New In Python 3.7] for an overview of the latest changes in the language. All [embedded modules] have also been updated to their latest versions.

With the official [retirement of Python 2] scheduled for the end of this year, the RoboFont codebase was cleaned-up from legacy py2 code and is now py3 only. The same applies to many core libraries  embedded in RF. *Goodbye py2, long live py3!* (See also the note about extensions containing .pyc files below.)

RoboFont now embeds GoogleFont’s [ufo2ft] and [fontmake] libraries, providing users an alternative path for generating OpenType and TrueType fonts from UFOs. (Binary fonts are still generated using [ufo2fdk] and Adobe’s [makeotf] by default.)

As announced in the [3.2 release], ufoLib is now [part of fontTools][fontTools.ufoLib]. In RoboFont 3.3, the standalone version of ufoLib is no longer available – so if you use `ufoLib` in your scripts, make sure to use `fontTools.ufoLib` instead from now on. Most extensions which use ufoLib have already been updated, [let us know] if you come across one which hasn’t.

The latest update of vanilla adds support for a new positioning mode, [auto layout], which uses rules and constraints instead of absolute position and size. This model is a bit more complex, but it’s very powerful: it allows you to build user interfaces that dynamically respond to changes, and makes it (typically) faster and easier to implement revisions in complex interface layouts. See the updated [documentation] for explanation and examples of the new auto layout methods.

[embedded modules]: /documentation/building-tools/toolkit/libraries/
[fontTools.ufoLib]: http://github.com/fonttools/fonttools/tree/master/Lib/fontTools/ufoLib
[What’s New In Python 3.7]: http://docs.python.org/3/whatsnew/3.7.html
[retirement of Python 2]: http://www.python.org/dev/peps/pep-0373/
[ufo2ft]: http://github.com/googlefonts/ufo2ft
[fontmake]: http://github.com/googlefonts/fontmake/
[ufo2fdk]: http://github.com/robotools/ufo2fdk
[makeotf]: http://github.com/adobe-type-tools/afdko
[3.2 release]: http://forum.robofont.com/topic/564/robofont-3-2
[let us know]: http://forum.robofont.com/category/13/extensions
[auto layout]: http://developer.apple.com/library/archive/documentation/UserExperience/Conceptual/AutolayoutPG/index.html
[documentation]: http://vanilla.robotools.dev/

## Extensions

RoboFont 3.3 introduces several new features which will get users and developers of extensions excited.

### For users

The [Mechanic 2 extension] has undergone a major revision and is now *a lot faster* – huge thanks to [Antonio Cavedoni] for this invaluable contribution.

The [Feature Preview] extension has also been partially rewritten and now uses [Harfbuzz] instead of [compositor] for shaping. The new version is faster and can handle shaping of non-latin scripts.

Here is an overview of new extensions released during the last release cycle:

- [ThemeManager](http://github.com/connordavenport/Theme-Manager) by Andy Clymer & Connor Davenport
  A tool for reading, writing, and storing custom `.roboFontTheme` files and switching Glyph View themes.

- [ToolManager](http://github.com/RafalBuchner/tool-manager) & [GlyphPalette](http://github.com/RafalBuchner/glyph-palette) by Rafał Buchner
  A special palette to activate/deactivate tools in the Glyph Editor, and a tool to visualize references to glyphs in components.

- [SideBear](http://github.com/ryanbugden/Sidebear) by Ryan Bugden
  An Inspector panel to modify the current glyph’s sidebearings.

- [PasteGlyph](http://github.com/typesupply/pasteglyph) by Tal Leming
  An interface for quickly pasting a glyph into the one you are currently drawing.

- [Kern-A-Lytics](http://github.com/adobe-type-tools/kernalytics-rf-ext) by Adobe Type Tools
  A tool for analyzing and fixing kerning consistency across several master fonts.

- [AngleRatioTool](http://github.com/LettError/angleRatioTool) by LetteError
  Shows the ratio between the length of incoming and outgoing sections of BCPs and tangents. Useful in preparing masters for interpolation.

- [GroupSpacing](http://github.com/gferreira/groupSpacing) by Gustavo Ferreira
  A tool to enable group spacing in the Space Center.

These previously existing tools are now also available via Mechanic:

- [LetterMeter](http://github.com/gferreira/lettermeter) by Just van Rossum & Peter Biľak
  A text-analysis tool for comparing multilingual texts and measuring the frequency of particular glyphs.

- [EventObserver](http://github.com/roboDocs/eventObserverExtension) by Frederik Berlaen
  A debugging helper which observes events and displays attributes which are available in the callback info dict.

[Feature Preview]: https://github.com/typemytype/featurePreviewRoboFontExtension
[Harfbuzz]: https://github.com/harfbuzz/harfbuzz
[compositor]: https://github.com/robotools/compositor

[Mechanic 2 extension]: http://github.com/robofont-mechanic/mechanic-2
[Antonio Cavedoni]: http://github.com/verbosus
[Extension Folder Spec]: /documentation/building-tools/extensions/extension-file-spec/
[Boilerplate extension]: http://github.com/roboDocs/rf-extension-boilerplate/

### For developers

The [Extension Folder Spec] was updated to version 3.0, with the addition of an *expiration date* attribute to enable trial extensions, and support for version-specific `lib` folders for binary extensions (which means that `.pyc`-only extensions can now work across different major versions of Python). Also, the `ExtensionBuilder` supports documentation sources in markdown format now.

See the updated [Boilerplate extension] for examples of these new features.

## Enjoy RoboFont 3.3!

RoboFont 3.3 is the first release which is [notarized][Notarizing Your App Before Distribution] by Apple, following the new security measures introduced in macOS 10.15.

See the [RoboFont3.3 release notes](/version-history/) for the full list of changes.

*Big thanks to all designers and developers who contribute to the continuous improvement of RoboFont!*

[Notarizing Your App Before Distribution]: http://developer.apple.com/documentation/xcode/notarizing_your_app_before_distribution?language=objc
