---
layout: page
title: RoboFont 1.0
post-date: 2011-09-03
---

{% image announcements/robofontrelease.png %}

RoboFont is released, version 1.0 is the first public version available for ordering.

Enjoy!