---
layout: page
title: RoboFont 3.2
post-date: 2019-01-21
---

_RoboFont 3.2 is out…!_

[Upgrade now](/downloads/) to get access to the latest improvements, bug fixes and updates to embedded libraries.

See the full list of changes in the [RoboFont 3.2 release notes](/version-history/#robofont-32).

Here are some of the highlights in this release:

### Support for macOS 10.14 (Mojave)

RoboFont 3.2 has been tested and updated to work with [macOS 10.14 Mojave]. Users who wish to upgrade to Mojave can do so without being worried about compatibility problems.

Mojave’s new Dark Mode, however, is not yet supported by default due to performance issues – but it can be turned on with a [script](http://forum.robofont.com/topic/565/activating-dark-mode-in-macos-10-14-rf-3-2). We hope to improve support for Dark Mode in the next release.

[macOS 10.14 Mojave]: http://www.apple.com/macos/mojave/

### Extensions

Work on the Extensions infrastructure continues with improved validation of extension bundles, handling of dependencies between extensions, a new icon for `.mechanic` files, improved support for keyboard shortcuts, updated developer documentation, etc.

Since the previous release, new extensions have been published on Mechanic 2 ([Blue Zone Editor] by Andy Clymer, [deCasteljau] by Lukas Schneider, [CheckParallel] by Jesen Tanadi, [EditThatNextMaster] by Erik van Blokland), and others were updated to RoboFont 3 ([CornerTools], [ScaleFast], [PenBall Wizard], [Interpolation Matrix] and [Ground Control] by Loïc Sander). Erik’s [GlyphNameFormatter] also includes important updates which affect the generation of glyph names in RoboFont (using GNFUL).

[Blue Zone Editor]: http://github.com/andyclymer/BlueZoneEditor-roboFontExt
[deCasteljau]: http://github.com/luke-snider/de-casteljau
[CheckParallel]: http://github.com/jtanadi/CheckParallelTool
[EditThatNextMaster]: http://github.com/LettError/editThatNextMasterRoboFontExtension
[CornerTools]: http://github.com/roboDocs/CornerTools
[ScaleFast]: http://github.com/roboDocs/ScaleFast
[PenBall Wizard]: http://github.com/roboDocs/PenBallWizard
[Interpolation Matrix]: http://github.com/roboDocs/InterpolationMatrix
[Ground Control]: http://github.com/roboDocs/GroundControl
[GlyphNameFormatter]: http://github.com/LettError/glyphNameFormatter

### Glyph Editor


The Glyph Editor is faster and includes many small improvements related to layers, such as a {% internallink "/reference/workspace/glyph-editor/layers#layers-popup" text="new dialog" %} to copy glyphs between layers, and better handling of template glyphs, metrics, unicode, anchors and images across layers.

### Updated core packages

All [embedded packages] have been updated to their latest versions.

Some changes are worth mentioning: the `ufoLib` module (used to read and write UFO files) has been moved to `fontTools.ufoLib`; FontParts now includes a new {% internallink "/topics/robofab-fontparts#object-selection-api" text="object selection API" %}; and PostScript autohinting is now done using the standalone [psautohint] module (instead of the AFDKO autohinter).

[embedded packages]: /technical-specification/#embedded-libraries
[psautohint]: http://github.com/adobe-type-tools/psautohint

### Support for single-file UFOs

RoboFont can now read and write [single-file UFOs] in the `.ufoz` format. This flavor of the UFO format is useful if you work with file sharing tools like DropBox, which don’t play very well with UFO packages containing lots of files.

Single-file UFOs, however, are *considerably slower to read and write*.

[single-file UFOs]: http://unifiedfontobject.org/roadmap/#single-file-ufo

### Website updates

The RoboFont homepage has a new icons menu for quick access to the main parts of the documentation:

{% image announcements/screen_shot_2018-12-21_at_07.29.35.png %}

The documentation is being continuously updated by adding new pages and improving existing content. The process is open and can be followed on the [robofont_com] repository.

[robofont_com]: http://gitlab.com/typemytype/robofont_com

### Public betas

RoboFont 3.2 is the first release of RoboFont with public betas during development. This change allowed us to have more users testing the beta and providing valuable feedback before the official release. We will continue to offer public betas for RoboFont 3.3.

---

_Special thanks to all beta testers & users for their precious feedback and feature requests._

Enjoy!