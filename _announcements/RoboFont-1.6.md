---
layout: page
title: RoboFont 1.6
post-date: 2014-10-15
---

{% image announcements/updaterobofont1_6.gif %}

RoboFont just got a lot better with small and big improvements, new features and some minor bug fixes :)

Have a look at the [version history](/version-history) for the full list of changes and additions.

You should get an automatic update, if not you should [redownload](/download) the app.

This will be the last update supporting OSX 10.6 - 10.8, the download will be available for older OS's but your next state of the art font editor will only run on 10.9+.

The automatic update supports only 10.9+, if you are on an older os please visit the [download](/download) page and manually update to RoboFont 1.6.

I would like to thank all the beta testers and all users posting on the [forums](http://forum.robofont.com) for their precious feedback and feature requests.

enjoy!