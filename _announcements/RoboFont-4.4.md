---
layout: page
title: RoboFont 4.4
post-date: 2023-10-10
---

Dear Users,
We are excited to announce the release of RoboFont 4.4! This version is packed with numerous new features, substantial improvements, and a multitude of bug fixes. Here’s a selection of the most important updates:
- **Python Upgrade**: RoboFont has transitioned from Python 3.7 to Python 3.9. Please be aware that some extensions may require updates to be compatible with this version. You might wonder why we chose Python 3.9 when Python 3.11 is available. The reason is that Python 3.9 is the last version that allows for the convenient bundling of embedded packages, ensuring compatibility with both Intel and Apple Silicon machines. Once updated to 3.11, our focus will shift exclusively to Apple Silicon. For more information, consult the [RoadMap](/technical-specification/#roadmap) section of the Technical Specifications page.

{% image announcements/designSpaceEditor.png %}

- **Design Space Document v5 Support**: RoboFont now provides full support for all the new features of the design space document version 5. Erik van Blokland and Frederik Berlaen rewrote [Batch](https://github.com/typemytype/batchRoboFontExtension/tree/master) and [DesignSpaceEditor](https://github.com/LettError/designSpaceRoboFontExtension) completely to take full advantage of these enhancements. Developers can also utilize the new [`UFOOperator`](https://github.com/LettError/ufoProcessor/blob/master/Lib/ufoProcessor/ufoOperator.py) object from the [`ufoProcessor`](https://github.com/LettError/ufoProcessor) package to manage their design space documents through Python. Many thanks to [Erik van Blokland](https://letterror.com) for his contribution!

- **Glyph Editor Improvements**: The glyph editor has undergone substantial improvements since the last public release. Numerous enhancements have been made in areas such as outline management, guidelines, and image handling, resulting in a smoother and more consistent drawing experience.

- **Under-the-Hood Enhancements**: Many changes have occurred under the hood of the application, some of which impact the public APIs utilized by developers. Notably, the subscriber module has received new add-ons, the list of embedded packages has been revised, and two new mojo submodules, pipTools, and ezuiViews, have been introduced.

- Update internal GNFUL list.

Check the [release notes](/version-history/)

Download available from [robofont.com](/download)

(and before you ask, yes we tested it on macOS Sonoma)
