---
layout: page
title: RoboFont 3.0
post-date: 2018-04-06
---

RoboFont **3** has landed. Finally!

From the outside, very little has changed…
but inside:

- UFO **3**
- Python **3**

With RF3 comes also a [new website], a [new documentation], and a [new forum].

Read more about the changes in RF3 [here][RF1-RF3 changes].

See the [version history] for the full list of changes and additions.

*RoboFont 3 requires a new license...*

If you bought RoboFont 1.X after 08/Mar/2017, you will get a full discount. [Contact the team] with your order number for details.

As always, many thanks to all the beta testers & all users posting on the forums for their precious feedback and feature requests.

enjoy!

[new website]: /
[new documentation]: /documentation/
[new forum]: http://forum.robofont.com/
[RF1-RF3 changes]: /documentation/RF3-changes/
[version history]: /version-history/
[Contact the team]: mailto:info@robofont.com

- - -

The official RF 3.0 release was held back by a few months because of an issue which affected updating extensions using Mechanic. This issue was solved with Mechanic 2 – read more about it in the [Robofont 3.1 release notes](http://forum.robofont.com/topic/490/robofont-3-1).

*original announcement date: 06 March 2018*