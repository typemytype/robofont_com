---
layout: page
title: RoboFont 3.1
post-date: 2018-07-05
---

**RoboFont 3.1 is out!**

### Main changes in RF 3.1

- *Preferences > Menus* now shows a warning for duplicated keyboard shortcuts.
- EditingTool is easier to subclass and disable editing but keep selection.
- Added support for custom menu items in any contextual menu.
- Any layer can be the top layer (removed restriction to have `foreground` at the top).
- Added new keyboard shortcut *Ctrl+Alt+Cmd+O* to reveal the log file in Finder.
- Added [ufoProcessor] (successor of designSpaceDocument) to the embedded packages.
- Updates to [ufoLib] to allow fine-grained control over UFO validation.
- Added new Preferences settings:
  - `spaceCenterMaxGlyphsForLiveUpdate`
  - `loadFontGlyphLimit`
  - `defaultGuidelineShowMeasurment`
  - `defaultGuidelineMagnetic`
  - `ufoLibReadValidate`
  - `ufoLibWriteValidate`

### New extensions infra-structure

The biggest change introduced by RF 3.1 is not in RoboFont itself, but in the infra-structure to distribute extensions. We have collaborated closely with Mechanic creator Jack Jennings to develop [Mechanic 2], a complete rewrite of Mechanic which fixes py3 bugs and introduces powerful new features. Read all about it in the [Mechanic 2 release notes].

### LOTS of bug fixes

RoboFont 3.1 includes **a lot** of fixes for bugs introduced by the RF3 rewrite – most of them related to Python 3 and to the switch to fontParts. *Many thanks to all RF3 pioneers for submitting bug reports and trusting the process!*

### UFO validation is now optional

RF3 introduced reading/writing UFOs using the [ufoLib], which also performs validation of UFO data. This validation was very strict, and was creating problems with UFOs containing non-standard or incorrect data. In order to improve interoperability between different UFO-based tools, UFO validation is now optional (use the settings `ufoLibReadValidate` and `ufoLibWriteValidate` to turn it on/off). Turning off validation also makes the process of reading/writing UFOs faster.

### Changes to the release process

Last but not least, RF 3.1 also introduces one big change to the release process: **Beta versions are now public** and can be downloaded from the [Downloads] page. We hope this will make the development process more transparent, and will make important bug fixes more quickly available to users.


*As always, many thanks to all the beta testers & all users posting on the forums for their precious feedback and feature requests.*

Enjoy!

[ufoProcessor]: https://github.com/LettError/ufoProcessor
[ufoLib]: https://github.com/unified-font-object/ufoLib
[Mechanic 2]: http://robofontmechanic.com/
[Mechanic 2 release notes]: /documentation/extensions/introducing-mechanic-2/
[Downloads]: /downloads/
