---
layout: page
title: RoboFont 1.2
post-date: 2012-09-26
---

RoboFont 1.2 is out and released today, just in time before [Robothon](http://robofab.com/robothon2012/).

This update includes a number of improvements, bug fixes and new features. The biggest change happened under the hood in the bridge between RoboFont and FDK.

Some new feature highlights:

* Space Centers accepts drag'n drop of glyphs from the Font Overview.
* Inspecter window glyph info also accepts smart side-bearing input *=e+10*
* Cleaner UI
* Open python files natively
* Open folders with python files by dragging on top of the app icon.
* New license validation system and [web shop](/download).
* Have a look at the [version history](/version-history) for the full list of changes and additions.

Existing users should have received an email with a direct download.
Student licenses will be resent after RoboThon.

I would like to thank all the beta testers and all users posting on the [forums](http://forum.robofont.com) for their precious feedback and feature requests.

Enjoy!