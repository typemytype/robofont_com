---
layout: page
title: Important notice for macOS 15, Sequoia
post-date: 2024-09-20
---

Dear Users,

Please be aware that the latest release of macOS 15 (Sequoia) is **not** compatible with RoboFont version 4.4 or older.
To fix this, you can download the latest beta version of RoboFont from the [RoboFont Discord server](https://discord.com/channels/1052516637489766411/1052529920993132544).

A new RoboFont release (4.5), compatible with macOS 15, will be released soon.