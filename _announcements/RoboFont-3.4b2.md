---
layout: page
title: RoboFont 3.4 Beta2
post-date: 2020-03-30
---

Hello RoboFont World! RoboFont3.4 is almost ready…

Because this version contains some deep changes to make the app compatible with Apple’s [updated notarization requirements](http://developer.apple.com/news/?id=09032019a), we’re making the release candidate available as a **public beta** to allow for one last round of testing and feedback.

If you can, please download [RoboFont 3.4 beta](/download), take it for a spin, and let us know if you come across anything weird. *Feedback from users running older versions of macOS (10.10—10.12) is specially appreciated!*

The updated documentation for 3.4 is also available online at [robofont.com/beta](/beta). See also the [release notes](/beta/version-history/).

The final 3.4 release will follow within one or two weeks. Until then, stay healthy and safe!