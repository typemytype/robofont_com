---
layout: page
title: RoboFont 1.8
post-date: 2016-12-06
---

A big maintenance update by updating internal packages and some small fixes. There is almost nothing left to fix!

Have a look at the [version history](/version-history) for the full list of changes and additions.

You should get an automatic update, if not you should [redownload](/download) the app.

This update supports OSX 10.9+

I would like to thank all the beta testers and all users posting on the [forums](http://forum.robofont.com) for their precious feedback and feature requests.

enjoy!