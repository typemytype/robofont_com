---
layout: page
title: RoboFont 3.4 Beta
post-date: 2019-12-23
---

RoboFont development never stops. Today we’re happy to release a first public beta of RoboFont 3.4 – just in time for your Christmas & New Year’s break.

[Download RoboFont 3.4b](/downloads/)

Some highlights in this version:

## Updated Preferences

The *Character Set* section of the Preferences has been renamed to *Font Overview*, and includes a new *Appearance* tab with new settings for adjusting the colors used in the glyph cells.

{% image announcements/screen-shot-2019-12-23-at-06.09.06.png %}

The *Menus* section has also been renamed to *Short Keys*, and includes a new search bar which makes it easier to find a particular short key or menu item.

## Package Installer

The *Python* menu now includes the *Package Installer*, a command line interface to install Python packages from the [Python Package Index (PyPI)][PyPI] using [pip].

{% image announcements/package-installer.png %}

See the updated [documentation](/documentation/workspace/package-installer/) for usage instructions.

*Packages installed with the Package Installer become available only inside RoboFont*!

[PyPI]: http://pypi.org/
[pip]: http://pip.pypa.io/

## SkipExportGlyphs

RoboFont 3.4 adds support for the new standard lib key `public.skipExportGlyphs`, which was recently added to the [UFO3 specification][public.skipExportGlyphs]. This key can be used to store a list of glyph names representing glyphs that should **not** be exported to generated font files.

The following interfaces to `public.skipExportGlyphs` have been added:

- *Font Info > RoboFont* includes a new text input field where the list of skippped glyphs can be defined as space separated list of glyph names.

  {% image announcements/font-info_robofont_.png %}

- *Inspector > Glyph* includes a new checkbox labeled *Exclude during export*, which can be used to individually add/remove glyphs to the list of skipped glyphs.

  {% image announcements/screen-shot-2019-12-23-at-14.18.09.png %}

- The Font Overview’s glyph cells now display a red X for glyphs which should be skipped, and the *skip export* flag can be used as a filter when searching.

  {% image announcements/screen-shot-2019-12-23-at-10.06.52.png %}

[public.skipExportGlyphs]: http://unifiedfontobject.org/versions/ufo3/lib.plist/#publicskipexportglyphs

## Updated API docs

RoboFont’s {% internallink "/reference/mojo" text="mojo API documentation" %} has been reviewed and updated.

The [vanilla documentation] has also been updated with screenshots and missing examples.

[vanilla documentation]: http://vanilla.robotools.dev/

- - -

As always, see the [release notes] for the full list of changes.

Huge thanks to all RoboFont users for feedback, bug reports, feature requests, etc.

Enjoy + Happy 2020!!

[release notes]: /version-history/
