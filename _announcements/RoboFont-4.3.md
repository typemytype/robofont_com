---
layout: page
title: RoboFont 4.3
post-date: 2022-10-19
---

Dear users,

RoboFont 4.3 is mature enough to be shared with you all! As usual, lots of bug fixes, user interface improvements, and a few adjustments on the APIs. Here's the list with the most important updates:

* FontTools has moved to DesignSpaceDocument v5. Tools like Batch and Skateboard work with the new format, but they do not support discrete axes.

* Add support for ctrl ⌃ + alt ⌥ + delete to completely remove a glyph from a UFO (including layers, kerning and groups).

* Keep unsaved glyphs into account while testing external changes. It avoids loosing data not saved on disk when reverting.

* Add support for dark mode and new toolbars on macOS 11 and 12.

{% image announcements/screenshot-2022-10-13-at-09.29.00-resized.png %}

{% image announcements/screenshot-2022-10-13-at-09.28.54-resized.png %}

{% image announcements/screenshot-2022-10-13-at-09.28.46-resized.png %}

{% image announcements/screenshot-2022-10-13-at-09.28.36-resized.png %}

* Point coordinates are typeset in italic if "Slant Point Coordinates" is on in the Preferences

* Adding a guideline with shift + g + click with an italic angle will apply the angle to the new guideline

{% image announcements/screenshot-2022-10-13-at-09.46.19-resized.png %}

* Display dragging lines with the italic angle if "Slant Point Coordinates" is on in the Preferences.

{% image announcements/screenshot-2022-10-13-at-09.43.04-resized.png %}

* Add anchor popover accept slanted coordinates, again, if "Slant Point Coordinates" is on in the Preferences.

{% image announcements/screenshot-2022-10-13-at-09.53.39-resized.png %}

* Massive reorganization of the {% internallink "/reference/api/mojo/mojo-subscriber" text="subscriber" %} docs. It's now easier to share single methods with HTML anchors.

* General improvement on the {% internallink "/reference/api/mojo/mojo-ui" text="mojo.UI" %} docs page: deeper hierarchy, categories, overall review of docstrings

* Reload operation in scripting window are now undoable.

* Add Reset Beam to space center settings menu as alternate of "Beam" (alt ⌥).

{% image announcements/screenshot-2022-10-11-at-10.29.44-resized.png %}

* Add Omit Mac Names in RoboFont tab in the font info sheet.

{% image announcements/screenshot-2022-10-11-at-10.47.15-resized.png %}

* Update internal GNFUL list.

* Remove local FDK support.

**Be aware that the minimum macOS version has been raised to 10.12.**

Check the [release notes](/version-history/)

Download available from [robofont.com](/download)

