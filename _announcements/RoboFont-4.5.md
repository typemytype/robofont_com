---
layout: page
title: RoboFont 4.5
post-date: 2024-10-15
---

Dear Users,

We are happy to announce the release of RoboFont 4.5! As always this version introduces numerous new features, substantial improvements, and lots of bug fixes. Below is a highlight of the most important updates:


- **Universal Build**: RoboFont 4.5 now supports **both Apple Silicon and Intel** processors natively. This means faster performance on Apple Silicon devices, while continuing to run seamlessly on Intel-based machines.


- **Python Upgrade**: RoboFont has upgraded from Python 3.9 to Python 3.12, bringing faster code execution, improved memory management, and more precise error reporting. Please be aware that some extensions may require updates to be compatible with this version!


- **Animated Zoom**: Enjoy a more fluid zooming experience with Animated Zoom, allowing smoother and more responsive transitions when zooming in and out in the glyph editor.

{% image reference/workspace/animated-zoom.gif %}


- **New Extension Bundle API**: We’ve introduced a brand new Extension Bundle API that simplifies the creation and management of extensions. This gives developers more flexibility and control over the packaging and distribution of their RoboFont extensions.


- **Force download of cloud storage files while opening**: RoboFont now supports the automatic force download of files from cloud storage when opening files stored on services like Dropbox or Google Drive. This fixes a number of issues related to reading from these platforms, ensuring files sync correctly without any hiccups.

- **Add support for compile filters**: We’ve added support for compile filters, allowing users to fine-tune and control specific aspects of how fonts are compiled during the export process. This gives you more granular control over how your fonts are built and output.

Check the [release notes](/version-history/) for a more detailed overview of all the changes.