---
layout: page
title: RoboFont 4.1
post-date: 2021-11-17
---

Dear users,

RoboFont 4.1 is OUT! Lots of bug fixes, a few tweaks to the UI and better support for BigSur & Monterey. Here's a list with some of most important updates:

+ Anchors and Guidelines can now be edited through a popover instead of a sheet
{% image announcements/guidelines_menu.png %}
{% image announcements/glyph-editor_add-anchor.png %}

+ The {% internallink "/reference/api/mojo/mojo-subscriber" text="subscriber docs" %} were improved since 4.0 release

+ The guideline angle knob now rotates in the same direction as the guide it edits
{% image announcements/knob.mp4 %}

+ Join selection got a little bit smarter. If you select two end-points and some other points, instead of closing the open contours, RoboFont will still join the selected end-points
{% image announcements/joinselection.mp4 %}

+ “xHeight cut” display option in Space Center has been renamed “Cut Off”. You can now edit it with cmd ⌘ when no glyph is selected
{% image announcements/cutoff.mp4 %}

+ 4.1 adds support for space center underline selection, you can activate it through the {% internallink "/reference/workspace/preferences-editor" text="Preferences Editor" %}

{% image announcements/under1-resized.png %}
{% image announcements/under2-resized.png %}

+ The docs fancy a new shiny Custom Lib Keys {% internallink "/reference/api/custom-lib-keys" text="page" %}

Check the [release notes](/version-history/)

Download available from [robofont.com](/download)
