---
layout: page
title: RoboFont 1.7.3
post-date: 2016-10-11
---

Ready to serve you and all your variations!

Have a look at the [version history](http://www.robofont.com/version-history) for the full list of changes and additions.

You should get an automatic update, if not you should [redownload](http://www.robofont.com/download) the app. MacOS 10.12 must update manually.

This update supports OSX 10.9+

I would like to thank all the beta testers and all users posting on the [forums](http://forum.robofont.com) for their precious feedback and feature requests.

enjoy!