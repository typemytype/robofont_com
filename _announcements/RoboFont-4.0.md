---
layout: page
title: RoboFont 4.0
post-date: 2021-07-15
---

After an intensive rewrite the RoboFont Team is happy to present your RoboFont 4!

RoboFont 4 is a payed upgrade. If you have a license for RoboFont 3 you will receive an email with a discount code. If you bought RoboFont 3 after July 2020, get in contact for a full discount code.

A special thanks to Tal, Gustavo and Roberto.

## Main changes in RoboFont 4

### Merz

RoboFont 4.0 has a completely new drawing back-end based on Apple's [Core Animation Layer](https://developer.apple.com/documentation/quartzcore) framework. It is significantly faster than `mojo.drawingTools`, and it has an object-oriented approach, meaning that it will be easy to integrate with your existing tools. Check the {% internallink "/topics/merz" text="introductory article" %} on the RoboFont website and the module [documentation](https://typesupply.github.io/merz/). Merz is a joint effort by [Tal Leming](https://www.typesupply.com) and [Frederik Berlaen](/).

There is a [Merz Playground extension](https://github.com/typesupply/merzplayground) you can install in RoboFont to start experimenting with the library.

{% image announcements/screenshot-2021-05-13-at-17.23.53.png %}

Many plugins have not been updated to Merz yet, so users will probably see some blurred contours here and there.

{% image announcements/wurst500.png %}

It happens because RoboFont sets a resolution limit to mojo.drawingTools to avoid annoying rendering delays. The RoboFont team is working hard to help plugin developers to update their tools in the near future.

The shift to Merz will happen in stages:
+ RoboFont 4.0 updates the Glyph Editor to Merz and it makes Merz accessible to extension developers as a module
+ RoboFont 4.1 will use Merz to render the Space Center
+ finally, RoboFont 4.2 will use Merz to render the Font Overview

### Subscriber

RoboFont 4 provides a brand new notification manager class called Subscriber. It is meant to work along with Merz, and it is super easy to integrate with your existing tools. You just need to subclass the Subscriber class and you are ready to set all the callbacks you want. With Subscriber you can observe every major application component (space center, glyph editor, and so on) and defcon objects. Did we already mention that subscriber takes care to unsubscribe all notifications once you close your tool? Yes, that's right. No phantom notifications lying around in the application. Efficient subscribing to events has never been easier! Subscriber is a joint effort by [Tal Leming](https://www.typesupply.com) and [Frederik Berlaen](/).

You will find an introductory article and a detailed reference page on the RoboFont documentation website.

### New docs organization

[Roberto Arista](http://projects.robertoarista.it) became the new *RoboFont's Community Manager*, welcome Roberto! The RoboFont team would like to thank [Gustavo Ferreira](https://www.hipertipo.com) for his enormous efforts and clear ideas on the documentation and far beyond!

The RoboFont documentation features a major reorganisation. Initiated by Gustavo Ferreira and now in the hands of Roberto Arista, the docs have been remodelled in four distinct sections:
+ **Topics**. Extensive articles with general knowledge like Pens, Why Scripting? and The UFO format.
+ **Tutorials**. Learning-oriented articles with practical steps like Submitting bug reports, Setting font info, and Using Git.
+ **How-Tos**. Step-by-step guides assessing specific problems like using the mojo AccordionView, Generating WOFFs, and Building accented glyphs with Glyph Construction.
+ **Reference**. Detailed documentation of all the application's major components and APIs. Here you'll find all you need to know about RoboFont's Glyph Editor, FontParts extras, and the mojo modules.

### Full quadratic support

By popular request, RoboFont 4.0 offers full support for quadratic bezier curves! You can finally use an infinite amount of control points to draw your crazy curves.

{% image announcements/screenshot-2021-05-19-at-12.12.13-resized.png %}

Did you know that a quadratic outline can be drawn without any anchor point? Anchor points are implicitly derived from the position of the control points.

{% image announcements/screenshot-2021-05-19-at-12.11.34-resized.png %}

The full support of quadratic bezier curves will benefit the development of fonts for variable fonts technology

## Some of the most relevant updates
**Updated internal packages** — Several embedded packages have been updated, including fontTools, fontMake, booleanOperations, fontMath and fontPens. [cffsubr] is now also included as an embedded library.

**UFO version as tuple** — The UFO format has a new versioning scheme with [major and minor version numbers](https://unifiedfontobject.org/versions/ufo3/metainfo.plist/). This is now documented in the UFO specification and implemented in `fontTools.ufoLib`.

**Adjustments to Glyph View colors** — Based on feedback we received from users of [Dark Mode], there have been several small changes to the way colors are composed in the Glyph View.

**Improved interaction with Smart Sets and tabs** — Interaction with Smart Sets has been fixed and improved, and native support for document tabs has gotten better.

**Better auto unicodes** — A small but very useful change: the `glyph.autoUnicodes()` function now also works with `uniXXX` and `uXXXX` names.

**Generate features with FontTools** — The Font Info sheet has a new option to generate OpenType features with FontTools. This prevents overflow issues and offers better support for RTL kerning.

You can check all the new features in detail [here](/beta/version-history/)
