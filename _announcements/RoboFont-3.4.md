---
layout: page
title: RoboFont 3.4
post-date: 2020-02-18
---

The beginning of the year has been very productive, and today we’re happy to announce the release of RoboFont 3.4.

[Download RoboFont 3.4](/downloads/)

Here are some of the highlights in this version:


Font-level Smart Sets
---------------------

In addition to the default Smart Sets, which are saved in the application settings, RoboFont now also supports *font-level Smart Sets*, which are saved in the font. RoboFont 3.4 has a more powerful {% internallink "reference/workspace/font-overview/smart-sets-panel" text="Smarts Sets panel" %} with drag & drop copying, nested folders, icons, contextual menu, etc.

{% image announcements/screen-shot-2020-02-28-at-11.20.29.png %}

All functions related to Smart Sets are now available from the new `mojo.smartSet` module, including new functions to get and set font-level Smart Sets – see {% internallink "/reference/api/mojo/mojo-smartset" text="Scripting Smart Sets" %} for examples.


Improved RTL text preview
-------------------------

The Space Center’s right-to-left mode has been improved with basic arabic shaping rules, so that glyphs are selected automatically based on their `init` / `medi` / `fina` suffix.

{% image announcements/screenshot.png %}

This feature is currently provided as an [extension][Basic Shaping], as it is still considered experimental.

Thanks to [Bahman Eslami] for his precious help.

[Basic Shaping]: http://github.com/typemytype/basicShapingRoboFontExtension
[Bahman Eslami]: http://bahman.design


New preferences for glyph cell colors
-------------------------------------

The Font Overview’s glyph cell colors can now be fully customized in the Preferences.

{% image announcements/screen-shot-2020-02-17-at-07.16.22.png %}


Package Installer
-----------------

The *Python* menu now includes the *Package Installer*, a command-line interface to install Python packages from the [Python Package Index (PyPI)][PyPI] using [pip].

{% image announcements/package-installer.png %}

See the updated {% internallink "reference/workspace/package-installer" text="documentation" %} for usage instructions.

*Packages installed with the Package Installer become available only inside RoboFont!*

[PyPI]: http://pypi.org/
[pip]: http://pip.pypa.io/


Support for skip export glyphs and production names
---------------------------------------------------

UFO3’s new standard lib key [public.skipExportGlyphs] – which can be used to store a list of glyph names that should not be exported to generated font files – is now supported. The Font Overview’s glyph cells will now display a red X for glyphs which should be skipped, and a new *Skip Export* flag can be used as a filter when searching glyphs.

{% image announcements/screen-shot-2019-12-23-at-10.06.52.png %}

Skipped glyphs are defined as a list of glyph names in *Font Info > RoboFont*. Individual glyphs can be added/removed from this list using a new checkbox in the *Glyph* section of the Inspector.

{% image announcements/skip-export-ui.png %}

An interface for production names has also been added to *Font Info > RoboFont*. This table corresponds to UFO3’s standard lib key [public.postscriptnames], which was already supported when generating fonts.

{% image announcements/font-info_robofont-copy_.png %}

[public.skipExportGlyphs]: http://unifiedfontobject.org/versions/ufo3/lib.plist/#publicskipexportglyphs
[public.postscriptnames]: http://unifiedfontobject.org/versions/ufo3/lib.plist/#publicpostscriptnames


APIs
----

The {% internallink "/reference/fontparts" text="FontParts map" %} has been updated – it turns out *Groups* was missing all this time!

![](https://raw.githubusercontent.com/gferreira/fontPartsMap/master/fontPartsMap.gif)

The {% internallink "reference/api/mojo/mojo-events#mojo.events.EditingTool" text="EditingTool" %} has new attributes that offer more control over the selection of individual glyph parts when subclassing. This makes it easier to create tools to edit only anchors, for example, or only guidelines, images, components, etc.

It is now possible to validate a value before {% internallink "/reference/api/mojo/mojo-ui" text="saving it to the defaults" %} – this helps to prevent from accidentally corrupting the preferences while scripting.

Generating fonts from layers is now supported with `font.generate(layerName)`.


Extensions
----------

### For users

Some new extensions have been released on [Mechanic] during the last months:

- [Mark Tool] by Bahman Eslami
  An interactive tool to adjust mark positioning using anchors.

- [Extreeeme45] by Jan Šindler
  A useful script to insert extreme points at 45° angles.

- [Edit Font Dimensions] and [EyeLiner] by Ryan Bugden
  An interactive tool for quickly manipulating font dimensions, and a drawing helper which highlights points that are aligned to vertical metrics, guides or blue zones.

- [Marky Mark] by Jackson Cavanaugh
  Shows a glyph’s mark color in the top right side of the Glyph View.

These previously existing tools are now also available via Mechanic:

- [RoboFont Project] by Frederik Berlaen
  A tool to record the position of open documents as projects and restore them later.

- [SpeedPunk] by Yanone
  The original curvature visualization tool is now free and open-source.

[Mechanic]: http://robofontmechanic.com/
[Mark Tool]: http://gitlab.com/typoman/robofont-mark-tool
[Edit Font Dimensions]: http://github.com/ryanbugden/Edit-Font-Dimensions
[EyeLiner]: http://github.com/ryanbugden/Eyeliner
[Extreeeme45]: http://github.com/jansindl3r/Extreeeme45
[SpeedPunk]: http://github.com/yanone/speedpunk
[RoboFont Project]: http://github.com/typemytype/projectRoboFontExtension
[Marky Mark]: http://github.com/okay-type/Markymark-roboFontExt

### For developers

There were also a few relevant updates for extension developers:

- The {% internallink "/reference/api/mojo/mojo-extensions#mojo.extensions.ExtensionBundle" text="ExtensionBundle" %} has some improvements to the default stylesheet for extension docs written in Markdown format (for example, large images now scale to fit into window). A new `pycExclude` argument was also added in order to keep some files as `.py` in binary extensions (needed for creating custom menus, for example).

- The [Boilerplate Extension] now includes example `.mechanic` files for GitHub, GitLab and BitBucket, showing the different URL schemes used by each platform.

- Instructions for submitting new extensions to the [Mechanic 2 server] have been updated with a few tips, recommendations and links.

[Boilerplate Extension]: http://github.com/roboDocs/rf-extension-boilerplate
[Mechanic 2 server]: http://github.com/robofont-mechanic/mechanic-2-server


Documentation
-------------

The *Workspace* section of the documentation has been restructured to make the hierarchy of pages match the hierarchy of windows in the app.

{% image announcements/screen-shot-2020-02-19-at-07.11.53.png %}

The docs now follows the application’s development model with parallel ‘release’ and ‘beta’ versions, and a new beta documentation is available at [robofont.com/beta]. This setup will give us more freedom to rearrange content and introduce breaking changes between versions.

The {% internallink "/tutorials/python" text="Introduction to Python" %} has been completely revised and updated to Python 3.

The [long-awaited] FontTools documentation is now also [finally online]. (thanks!)

[RoboFont documentation]: http://gitlab.com/typemytype/robofont_com
[robofont.com/beta]: /beta
[long-awaited]: http://github.com/fonttools/fonttools/pull/1333
[finally online]: http://fonttools.readthedocs.io/en/latest/


A note about Apple’s updated notarization requirements
------------------------------------------------------

[Notarization] is an automated process designed to protect users from malicious software – it scans code for malicious content and checks software components for code-signing issues.

MacOS Catalina (10.15), released in October 2019, introduced the requirement that all newly-built apps and command tools distributed outside the App Store must follow Apple’s [notarization prerequisites]. RoboFont3.3, [released in November][RoboFont3.3], was the first version of RoboFont to be notarized.

Since the 3rd February, Apple’s notarization requirements became [stricter] and all new apps must now be both [hardened and properly notarized]. We’re relieved to announce that RoboFont 3.4 passes the new requirements and has been sucessfully notarized.

The whole process was hard, painful and time consuming, and pushed back the release date by several weeks. In our experience, the new strict notarization requirements are a big burden for independent developers who build apps outside of Apple’s Xcode environment.

The good news is that the knowledge required to properly notarize PyObjC-based apps is now publicly available in the source code of [DrawBot] and [FontGoggles]. A gigantic thanks to Just van Rossum for his assistance!

[Notarization]: https://developer.apple.com/documentation/xcode/notarizing_macos_software_before_distribution
[notarization prerequisites]: https://developer.apple.com/news/?id=09032019a
[RoboFont3.3]: https://forum.robofont.com/topic/725/robofont-3-3
[hardened and properly notarized]: https://eclecticlight.co/2020/02/03/hardening-and-notarization-finally-arrive-in-catalina/
[stricter]: https://developer.apple.com/news/?id=12232019a
[FontGoggles]: http://github.com/justvanrossum/fontgoggles/blob/master/.github/workflows/buildapp.yml
[DrawBot]: http://github.com/typemytype/drawbot/blob/master/.github/workflows/build.yml

Enjoy RoboFont 3.4!
-------------------

As always, see the [release notes] for the full list of changes.

*Big thanks to all designers and developers who contribute to the continuous improvement of RoboFont!*

[release notes]: /version-history/#robofont-34
