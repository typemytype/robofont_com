---
layout: page
title: RoboFont 3.2 Beta
post-date: 2018-06-25
---

As part of a change in the release process, RoboFont offers beta versions to make the development process more transparent, and make important bug fixes more quickly available to users.


A first public Beta version of RoboFont 3.2 is now available from the [Downloads](/downloads/#beta-releases) page.

Main changes in this version:

- big speed improvement when generating font binaries
- better error handling when validating extensions
- improve loading extensions which require other extensions (load required extensions first)
- use `allFonts` as keyword in `SelectFont` dialog
- added a pop-up window to copy metrics when copying between layers
- several bug fixes!
- Update of internal packages.

Newer 3.2 Beta builds will become available on the website as they are released.

_Special thanks to all beta testers & users for their precious feedback and feature requests._

Enjoy!

-----------

A fresh public beta is available for download: [RoboFont 3.2b](/downloads/#beta-releases).

This looks like it will be the Golden Master or Release Candidate for RoboFont 3.2.

Main changes in this version:

* Support for single file `.ufoz`: a compressed UFO3. This will be slower in reading and writing but works better with services like dropBox.
* several bug fixes both in external packages as internally.
* Update of internal packages

*Special thanks to all beta testers & users for their precious feedback and feature requests.*

Enjoy!