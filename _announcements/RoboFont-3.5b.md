---
layout: page
title: RoboFont 3.5 Beta
post-date: 2020-11-18
---

Hello RoboFont World !

A first beta version of RoboFont 3.5 has just been released.

[Download RoboFont 3.5b](/downloads/#beta-releases)

Here are some of the updates in this version:

**Updated internal packages** — Several embedded packages have been updated, including fontTools, fontMake, booleanOperations, fontMath and fontPens. [cffsubr] is now also included as an embedded library.

**UFO version as tuple** — The UFO format has a new versioning scheme with [major and minor version numbers]. This is now documented in the UFO specification and implemented in `fontTools.ufoLib`.

**Adjustments to Glyph View colors** — Based on feedback we received from users of [Dark Mode], there have been several small changes to the way colors are composed in the Glyph View.

**Improved interaction with Smart Sets and tabs** — Interaction with Smart Sets has been fixed and improved, and native support for document tabs has gotten better.

**Better auto unicodes** — A small but very useful change: the `glyph.autoUnicodes()` function now also works with `uniXXX` and `uXXXX` names.

**Generate features with FontTools** — The Font Info sheet has a new option to generate OpenType features with FontTools. This prevents overflow issues and offers better support for LTR kerning.

…and last but not least:

## Compatility with macOS Big Sur

*We recommend users not to upgrade to macOS Big Sur yet!*

[macOS Big Sur], released earlier this month, introduced major changes to the way views are drawn to the screen – which prevented RoboFont 3.4 from working properly. This release makes RoboFont compatible with Big Sur, but also makes it slower.

The good news is that we have been working since July with Tal Leming on a whole new drawing system using [CALayers], which fixes the issues and offers lots of new possibilities. We are very excited about it, and can’t wait to make it available to you.

- - -

As usual, see the [Release Notes] for the full list of changes.

And a big thanks to all RoboFont users for feedback, bug reports, feature requests, etc.

[cffsubr]: http://github.com/adobe-type-tools/cffsubr
[major and minor version numbers]: https://github.com/unified-font-object/ufo-spec/issues/78
[CALayers]: http://developer.apple.com/documentation/quartzcore/calayer
[macOS Big Sur]: http://www.apple.com/macos/big-sur/
[Dark Mode]: https://forum.robofont.com/topic/565/activating-dark-mode-in-macos-10-14-rf-3-2
[Release Notes]: /beta/version-history/#version-35b-build-2011161841