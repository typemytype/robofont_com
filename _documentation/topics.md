---
layout: page
title: Topics
levels: 1
tree:
    - welcome-to-robofont
    - robofont-design-principles
    - robofont-features-overview
    - robofont-pre-history
    - robofont-history
    - the-ufo-format
    - workspace-overview
    - drawing-environment
    - scripting-environment
    - accented-glyphs
    - production-names
    - smartsets
    - interpolation
    - preparing-for-interpolation
    - kerning
    - variable-fonts
    - robofont-3-changes
    - recommendations-upgrading-RF3
    - robofab-fontparts
    - why-scripting
    - fontparts
    - vanilla
    - merz
    - subscriber
    - window-controller
    - drawbot
    - understanding-contours
    - glyphmath
    - pens
    - transformations
    - boolean-glyphmath
    - interactive-tools
    - the-observer-pattern
    - observers
    - extensions
    - mechanic-2
    - upgrading-code-to-RoboFont4
    - boilerplate-extension
---

*theoretical knowledge · understanding-oriented · useful when studying*


<div class='row'>
<div class='col' markdown='1'>

### Introduction

- {% internallink 'topics/welcome-to-robofont' %}
- {% internallink 'topics/robofont-design-principles' %}
- {% internallink 'topics/robofont-features-overview' %}
- {% internallink 'topics/robofont-pre-history' %}
- {% internallink 'topics/robofont-history' %} <span class='yellow'>✗</span>
- {% internallink 'topics/the-ufo-format' %}

### Workspace

- {% internallink 'topics/workspace-overview' %}
- {% internallink 'topics/drawing-environment' %}
- {% internallink 'topics/scripting-environment' %}
- {% internallink 'topics/appearance' %}

### Making fonts

- {% internallink 'topics/accented-glyphs' %}
- {% internallink 'topics/production-names' %}
- {% internallink 'topics/smartsets' %}
- {% internallink 'topics/interpolation' %}
- {% internallink 'topics/preparing-for-interpolation' %}
- {% internallink 'topics/kerning' %}
- {% internallink 'topics/variable-fonts' %}

### Upgrading

- {% internallink 'topics/robofont-3-changes' %}
- {% internallink 'topics/recommendations-upgrading-RF3' %}
- {% internallink 'topics/upgrading-code-to-RoboFont4' %}
- {% internallink 'topics/robofab-fontparts' %}

</div>
<div class='col' markdown='1'>

### Scripting

- {% internallink 'topics/why-scripting' %}
- {% internallink 'topics/fontparts' %}
- {% internallink 'topics/vanilla' %}
- {% internallink 'topics/merz' %}
- {% internallink 'topics/subscriber' %}
- {% internallink 'topics/defcon-representations' %}
- {% internallink 'topics/drawbot' %}
- {% internallink 'topics/understanding-contours' %}
- {% internallink 'topics/glyphmath' %}
- {% internallink 'topics/pens' %}
- {% internallink 'topics/transformations' %}
- {% internallink 'topics/deprecated-apis' %}
- {% internallink 'topics/boolean-glyphmath' %}
- {% internallink 'topics/interactive-tools' %}
- {% internallink 'topics/the-observer-pattern' %}
- {% internallink 'topics/observers' %}
- {% internallink 'topics/window-controller' %}

### Extensions

- {% internallink 'topics/extensions' %}
- {% internallink 'topics/mechanic-2' %}
- {% internallink 'topics/boilerplate-extension' %}
- {% internallink 'topics/building-extensions' %} 
- {% internallink 'topics/publishing-extensions' %} 
- <a href="https://extensionstore.robofont.com/certified-developers/">Publish extensions in the Extension Store</a>

</div>
</div>
