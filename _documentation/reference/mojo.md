---
layout: page
title: mojo
tags:
  - scripting
  - mojo
tree:
  - ../api/mojo/*
treeCanHide: true
---

The `mojo` module gives access to RoboFont-specific objects, events and tools.

{% tree "/documentation/reference/api/mojo/*" levels=2 "%}
