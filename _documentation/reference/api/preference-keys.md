---
layout: page
title: Preferences Keys
tags:
  - scripting
---

{% comment %}

default value is fallback if something goes wrong.

this is a row:
scriptableKey, description (what user see in preferences), default value

categories:
- font overview:
    - colors
- glyph view:
    - colors
    - hot keys
- space center:
    - colors
    - hot keys

small snippets to get and set values
and to update views related to them

{% endcomment %}