---
layout: page
title: Custom lib keys
tags:
  - scripting
---

Here follows a list of custom font lib keys supported by RoboFont.
If you are looking the TrueType hinting keys check the {% internallink '/tutorials/truetype-hinting' %}

## Glyph Lib Keys

**public.markColor**
: This key is used for representing the mark color.
: See also [public.markColor on the UFO website](https://unifiedfontobject.org/versions/ufo3/glyphs/glif/#publicmarkcolor)


## Font Lib Keys

**com.typemytype.robofont.italicSlantOffset**
: number, default is `0`
: See also {% internallink "tutorials/making-italic-fonts" text='setting the italic slant offset' %}

{% internallink '/tutorials/making-italic-fonts/#setting-the-italic-slant-offset' text='setting the italic slant offset' %}

**public.glyphOrder**
: This key is used for representing the user’s preferred glyph order in the font.
  The glyph order is stored as a list of glyphs names. Glyph names must not occur more than once. Glyph names in the font may not appear in the order. The order may contain glyphs that are not in the font.
: See also [Public Glyphorder on the Unified Font Object website](https://unifiedfontobject.org/versions/ufo3/lib.plist/#publicglyphorder)

**com.defcon.sortDescriptor**
: list, default is empty
: Sort descriptions used for representing glyph order in the font editor.
: Fallback for `public.glyphOrder`
: See also [sortGlyphNames on defcon.robotools.dev](https://defcon.robotools.dev/en/latest/objects/unicodedata.html#defcon.UnicodeData.sortGlyphNames)


# The 'roundTolerance' argument controls the rounding of point coordinates.
# It is defined as the maximum absolute difference between the original
# float and the rounded integer value.
# The default tolerance of 0.5 means that all floats are rounded to integer;
# a value of 0 disables rounding; values in between will only round floats
# which are close to their integral part within the tolerated range.
# check https://fonttools.readthedocs.io/en/latest/pens/t2CharStringPen.html
font.lib["com.typemytype.robofont.roundTolerance"] = 0.5

# boolean, default is True
font.lib["com.typemytype.robofont.shouldAddPointsInSplineConversion"] = True

**com.robofont.generateFeaturesWithFontTools**
: boolean, default is `False`

**com.robofont.generateCheckComponentMatrix**
: boolean, default is `True`
: If this lib key is set to `True`, components with a transformation impling more than translation
  will be decomposed, otherwise they will stay as components.
: Only applies when the global generate decompose is set to `False`.

**com.robofont.generateTurnOnSubroutinizations**
: boolean, default is `False`
: Adds subroutinization in FDK makeotf.

**com.robofont.generateOmitNotMentionedGlyphs**
: boolean, default is `False`
: See also `-gs flag` on [MakeOTF OpenType/CFF compiler - User Guide](http://adobe-type-tools.github.io/afdko/MakeOTFUserGuide.html#makeotf-options)

**com.robofont.fontCompilerTool**
: string, either `"fontmake"` or `"afdko"`, default is `"afdko"`
: Which compiler is used for generating binaries.

**com.typemytype.robofont.compileSettings.autohint**
: boolean, default is `True`

**com.typemytype.robofont.compileSettings.checkOutlines**
: boolean, default is `True`
: Performs a remove overlap when set to `True`.

**com.typemytype.robofont.compileSettings.createDummyDSIG**
: boolean, default is `True`
: It creates a dummy digital signature open type table necessary for using the font in Microsoft Office applications
: See also [Digital Signature Table on the Microsoft docs](https://docs.microsoft.com/en-us/typography/opentype/spec/dsig)
  and [Making OT/TTF layout features work in MS Word 2010](https://typedrawers.com/discussion/192/making-ot-ttf-layout-features-work-in-ms-word-2010)

**com.typemytype.robofont.compileSettings.decompose**
: boolean, default is `False`
: Decompose components during export.

**com.typemytype.robofont.compileSettings.generateFormat**
: string, either `"ttf"` or `"otf"` or `"pfa"`
: The format use to generate binaries.

**com.typemytype.robofont.compileSettings.layerName**
: string, if non existing the default layer will be used
: The layer to be used for export.

**com.typemytype.robofont.compileSettings.path**
: string, path where the last binary has been stored
: Overriden every time a binary is generated.

**com.typemytype.robofont.compileSettings.releaseMode**
: boolean, default is `False`

: See also `-f flag` on [MakeOTF Options on Adobe Type Tools](http://adobe-type-tools.github.io/afdko/MakeOTFUserGuide.html#makeotf-options)

**com.typemytype.robofont.roundTolerance**
: float and the rounded integer value.
: The roundTolerance argument controls the rounding of point coordinates.
  It is defined as the maximum absolute difference between the original
  The default tolerance of 0.5 means that all floats are rounded to integer;
  a value of 0 disables rounding; values in between will only round floats
  which are close to their integral part within the tolerated range.
: See also [t2CharStringPen on the fonttools website](https://fonttools.readthedocs.io/en/latest/pens/t2CharStringPen.html)

**com.typemytype.robofont.shouldAddPointsInSplineConversion**
: boolean, default is `True`
: When set to `True` points will be added during conversion from bezier to TrueType splines to preserve the curve.
  When Set to `False` not points will be added and the amounts of points will be preserved.

**com.typemytype.robofont.smartSets**
: list of dictionaries (default is empty), with the following keys:
  * `uniqueKey`: unique key to identify the smart set item, must be unique, string.
  * `smartSetName`: name of the smart set item, string.
  * `query`: a predicate string, there is no query when glyphNames are set, see also [Predicate Format String Syntax](https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/Predicates/Articles/pSyntax.html#//apple_ref/doc/uid/TP40001795-SW1).
  * `glyphNames`: a list of glyph names (strings) no need to set a query (build automatically when glyphNames are set).
  * `group`: a list of smart sets items.

**com.typemytype.robofont.splineConversionFuzz**
: number, default is `2`
: Amount of distance in UPM between 1/4 of the curve and 3/4 of the curve that is accepted before adding an extra point.

**com.typesupply.feaPyFoFum.compileFeatures**
: boolean, default is `False`
: If `True` features will be compiled with [feaPyFoFum](https://github.com/typesupply/feaPyFoFum).

**com.typesupply.ufo2fdk.glyphOrderAndAliasDB**
: string, default is empty
: When a font has the `public.postscriptNames` lib this will not be used. Otherwise, RoboFont will look for this one and it will send it to FDK.
: See also [MakeOTF OpenType/CFF compiler User Guide](http://adobe-type-tools.github.io/afdko/MakeOTFUserGuide.html)

**public.postscriptNames**
: dictionary, glyphs names as keys and Postscript glyph names as values
: This defines a preferred glyph name to Postscript glyph name mapping for glyphs in the font.
: Both keys and values must be strings. The values must conform to the Postscript glyph naming specification.
: The dictionary may contain glyph names that are not in the font. The dictionary may not contain a key, value pair for all glyphs in the font. If a glyph’s name is not defined in this mapping, the glyph’s name should be used as the Postscript name.
: See also [public.postscriptNames on the UFO website](https://unifiedfontobject.org/versions/ufo3/lib.plist/#publicpostscriptnames)

**public.skipExportGlyphs**
: This key is a list of glyph names used for representing glyphs that the user does not want exported to the final font file.
: See also [public.skipExportGlyphs on the UFO website](https://unifiedfontobject.org/versions/ufo3/lib.plist/#publicskipexportglyphs)

**public.truetype.instructions**
: This key provides a dict defining a set of TrueType instructions assembly code for a glyph.
: See also [public TrueType instructions](https://unifiedfontobject.org/versions/ufo3/glyphs/glif/#publictruetypeinstructions)