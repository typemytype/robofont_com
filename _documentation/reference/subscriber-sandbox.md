---
layout: page
title: Subscriber Sandbox
excludeSearch: true
excludeNavigation: true
draft-hidden: true
---

{::options toc_levels='1,2,3' /}

* Table of Contents
{:toc}

<div markdown="1" class="class">

## class Subscriber()

This class allows you to easily subscribe to events within RoboFont.
Subclass `Subscriber` and indicate what events you want to subscribe to by overriding callbacks that correspond to events. For example:

```python
from mojo.subscriber import Subscriber

class ExampleSubscriber(Subscriber):

    def glyphEditorDidSetGlyph(self, info):
        glyph = info["glyph"]
        print("A glyph editor was set with this glyph.", glyph)

    def glyphEditorDidMouseDown(self, info):
        location = info["locationInGlyph"]
        print("Mouse down in glyph editor at:", location)
```

## Debugging

Always give your subclass a unique name. The class name is used
as an identifier within the low-level event systems.
When developing your subscriber, set the `debug` flag in the class
to `True`. This will instruct the subscriber to scan through the
low-level event systems for references to any other instances of
your subclass.

```python
class ExampleSubscriber(Subscriber):

    debug = True
```

Be careful with this. If you are trying to debug something not being
updated and are completely stumped, set `debug = False`, restart RoboFont and then try your tool again.


## Event Coalescing

`Subscriber` uses event coalescing to reduce redundant event
posting. This is done by waiting for a specified amount of time
after a low-level event has been posted before calling the
corresponding subclass method. If a related low-level event
is posted before the time has expired, the waiting starts over.
Tools that need instant calling of any or all of the subclass
methods may set the delay of any methods to `0`. Likewise, tools
that can tolerate a small lag set the delay of any methods to
the appropriate time. All delay times are defined as `float`
values representing seconds. Any event with `Will` or `Wants`
in its name should always have a delay of `0` to prevent
unexpected asynchronous behavior.

The default delay for RoboFont event is `0`.
For defcon event the default delay is `0.033`.

To specify a custom delay for a method in your subclass, create
an attributes with the same method name plus `Delay` and set the
value to a `float`.

```python
from mojo.subscriber import Subscriber

class ExampleSubscriber(Subscriber):

    glyphEditorDidSetGlyphDelay = 0.2

    def glyphEditorDidSetGlyph(self, info):
        glyph = info["glyph"]
        print("A glyph editor was set with this glyph.", glyph)

    glyphEditorDidMouseDownDelay = 0

    def glyphEditorDidMouseDown(self, info):
        location = info["location"]
        print("Mouse down in glyph editor at:", location)
```

## Callback Arguments

Each method must take one `info` argument. The value for
this argument will be a dictionary. Events may have their own specific keys,
but you can expect to find in all `info` dictionaries:

- `subscriberEventName` : The subscriber event name.
- `iterations` : An ordered list of the iterations of the
  important data from the `lowLevelEvents`. The items in
  the list will be dictionaries.
- `lowLevelEvents` : An ordered list of low-level events.

Events triggered by mouse of key events may have a `deviceState` dictionary with this structure:

- `clickCount` : The number of mouse clicks.
- `keyDown` : The pressed character.
- `keyDownWithoutModifiers` : The pressed characters with modifiers.
- `up` : A bool indicating if the *up arrow* key is pressed.
- `down` : A bool indicating if the *down arrow* key is pressed.
- `left` : A bool indicating if the *left arrow* key is pressed.
- `right` : A bool indicating if the *right arrow* key is pressed.
- `shiftDown` : A bool indicating if the *Shift* modifier key is pressed.
- `capLockDown` : A bool indicating if the *Caps Lock* modifier key is pressed.
- `optionDown` : A bool indicating if the *Option* modifier key is pressed.
- `controlDown` : A bool indicating if the *Control* modifier key is pressed.
- `commandDown` : A bool indicating if the *Command* modifier key is pressed.
- `locationInWindow` : The location of the event in window coordinates.
- `locationInView` : The location of the event in view coordinates.

Any objects outside of `lowLevelEvents` that can be represented
with fontParts objects will be.


## Interaction Callbacks

### RoboFont Application

A group of callbacks that targets the main scope of the application

#### <method>roboFontDidFinishLaunching<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.applicationDidFinishLaunching` event is posted. Default delay set to 0.00 seconds.

#### <method>roboFontDidBecomeActive<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.applicationDidBecomeActive` event is posted. Default delay set to 0.00 seconds.

#### <method>roboFontWillResignActive<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.applicationWillResignActive` event is posted. Default delay set to 0.00 seconds.

#### <method>roboFontDidChangeScreen<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.applicationScreenChanged` event is posted. Default delay set to 0.00 seconds.

#### <method>roboFontWillTerminate<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.applicationWillTerminate` event is posted. Default delay set to 0.00 seconds.

#### <method>roboFontWantsOpenUnknownFileType<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.applicationOpenFile` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `fileHandler`
   + `fileExtension`
   + `path`


#### <method>roboFontDidSwitchCurrentGlyph<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.currentGlyphChanged` event is posted. Default delay set to 0.00 seconds.

#### <method>roboFontDidBuildExtension<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.extensionDidGenerate` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `path`

#### <method>roboFontWillRunExternalScript<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.externalLaunchEvent` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `data`

#### <method>roboFontWantsInspectorViews<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.inspectorWindowWillShowDescriptions` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `viewDescriptions`

#### <method>roboFontWantsNamespaceAdditions<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.namespaceCallbacks` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `namespace`

#### <method>roboFontDidChangePreferences<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.preferencesChanged` event is posted. Default delay set to 0.00 seconds.

### Font Document

Callbacks that target a whole UFO document, most of the time paired with a window

#### <method>fontDocumentWillOpenNew<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.newFontWillOpen` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `font`

#### <method>fontDocumentDidOpenNew<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.newFontDidOpen` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `font`

#### <method>fontDocumentWillOpenBinaryFont<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.binaryFontWillOpen` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `font`
   + `format`
   + `source`

#### <method>fontDocumentWillOpen<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontWillOpen` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `font`

#### <method>fontDocumentDidOpen<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontDidOpen` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `font`

#### <method>fontDocumentWillSave<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontWillSave` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `font`
   + `path`

#### <method>fontDocumentDidSave<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontDidSave` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `font`
   + `path`

#### <method>fontDocumentWillAutoSave<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontWillAutoSave` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `font`

#### <method>fontDocumentDidAutoSave<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontDidAutoSave` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `font`

#### <method>fontDocumentWillGenerate<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontWillGenerate` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `font`
   + `testInstall`
   + `format`
   + `path`
   + `layerName`

#### <method>fontDocumentDidGenerate<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontDidGenerate` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `font`
   + `format`
   + `path`
   + `layerName`

#### <method>fontDocumentWillTestInstall<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontWillTestInstall` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `font`
   + `success`
   + `format`
   + `report`

#### <method>fontDocumentDidTestInstall<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontDidTestInstall` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `font`
   + `format`

#### <method>fontDocumentWillTestDeinstall<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontWillTestDeinstall` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `font`

#### <method>fontDocumentDidTestDeinstall<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontDidTestDeinstall` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `font`

#### <method>fontDocumentDidChangeExternally<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontDidChangeExternally` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `font`

#### <method>fontDocumentDidBecomeCurrent<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontBecameCurrent` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `font`

#### <method>fontDocumentDidResignCurrent<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontResignCurrent` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `font`

#### <method>fontDocumentWillClose<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontWillClose` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `font`

#### <method>fontDocumentDidClose<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontDidClose` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `font`

#### <method>fontDocumentWindowDidOpen<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontWindowDidOpen` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `font`

#### <method>fontDocumentWindowWillOpen<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontWindowWillOpen` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `font`

#### <method>fontDocumentWantsToolbarItems<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontWindowWillShowToolbarItems` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `font`
   + `singleWindowMode`
   + `itemDescriptions`

### Font Overview

Callbacks targeting the font collection view

#### <method>fontOverviewWillOpen<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontOverviewWillOpen` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `fontOverview`
   + `font`

#### <method>fontOverviewDidOpen<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontOverviewDidOpen` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `fontOverview`
   + `font`

#### <method>fontOverviewWillClose<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontOverviewWillClose` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `fontOverview`
   + `font`

#### <method>fontOverviewDidCopy<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontOverviewCopy` event is posted. Default delay set to 0.00 seconds.

#### <method>fontOverviewDidRedo<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontOverViewRedo` event is posted. Default delay set to 0.00 seconds.

#### <method>fontOverviewDidUndo<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontOverViewUndo` event is posted. Default delay set to 0.00 seconds.

#### <method>fontOverviewDidPaste<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontOverviewPaste` event is posted. Default delay set to 0.00 seconds.

#### <method>fontOverviewWantsContextualMenuItems<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.fontOverviewAdditionContextualMenuItems` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:

   + `itemDescriptions`

### Current Font/Glyph

#### <method>currentFontDidSetFont<arguments>(self, info)</arguments></method>

Called when the current font is set.

#### <method>currentGlyphDidSetGlyph<arguments>(self, info)</arguments></method>

Called when the current glyph is set.

### Glyph Editor

#### <method>glyphEditorWillOpen<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.glyphWindowWillOpen` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`

#### <method>glyphEditorDidOpen<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.glyphWindowDidOpen` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`

#### <method>glyphEditorWillClose<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.glyphWindowWillClose` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`

#### <method>glyphEditorWillSetGlyph<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.viewWillChangeGlyph` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`

#### <method>glyphEditorDidSetGlyph<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.viewDidChangeGlyph` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`

#### <method>glyphEditorDidKeyDown<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.keyDown` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`

#### <method>glyphEditorDidKeyUp<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.keyUp` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`

#### <method>glyphEditorDidChangeModifiers<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.modifiersChanged` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`

#### <method>glyphEditorWillShowPreview<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.viewWillShowPreview` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`

#### <method>glyphEditorWillHidePreview<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.viewWillHidePreview` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`

#### <method>glyphEditorDidMouseDown<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.mouseDown` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`

#### <method>glyphEditorDidMouseUp<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.mouseUp` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`
   + `offset`

#### <method>glyphEditorDidMouseMove<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.mouseMoved` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`
   + `offset`

#### <method>glyphEditorDidMouseDrag<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.mouseDragged` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`
   + `offset`
   + `delta`

#### <method>glyphEditorDidRightMouseDown<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.rightMouseDown` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`

#### <method>glyphEditorDidRightMouseDrag<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.rightMouseDragged` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`
   + `offset`
   + `delta`

#### <method>glyphEditorDidRightMouseUp<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.rightMouseUp` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`
   + `offset`

#### <method>glyphEditorDidScale<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.viewDidChangeScale` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `scale`

#### <method>glyphEditorWillScale<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.viewWillChangeScale` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `scale`

#### <method>glyphEditorDidCopy<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.copy` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`

#### <method>glyphEditorDidCopyAsComponent<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.copyAsComponent` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`

#### <method>glyphEditorDidCut<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.cut` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`

#### <method>glyphEditorDidPaste<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.paste` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`

#### <method>glyphEditorDidPasteSpecial<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.pasteSpecial` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`

#### <method>glyphEditorDidDelete<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.delete` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`

#### <method>glyphEditorDidSelectAll<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.selectAll` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`

#### <method>glyphEditorDidSelectAllAlternate<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.selectAllAlternate` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`

#### <method>glyphEditorDidSelectAllControl<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.selectAllControl` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`

#### <method>glyphEditorDidDeselectAll<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.deselectAll` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`

#### <method>glyphEditorDidAddUndoItem<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.addedUndoItem` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`

#### <method>glyphEditorDidUndo<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.didUndo` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`

#### <method>glyphEditorWantsContextualMenuItems<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.glyphAdditionContextualMenuItems` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`
   + `itemDescriptions`

#### <method>glyphEditorWantsPointContexualMenuItems<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.pointAdditionContextualMenuItems` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`
   + `itemDescriptions`

#### <method>glyphEditorWantsAnchorContextualMenuItems<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.anchorAdditionContextualMenuItems` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`
   + `itemDescriptions`

#### <method>glyphEditorWantsGuidelineContextualMenuItems<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.guideAdditionContextualMenuItems` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`
   + `itemDescriptions`

#### <method>glyphEditorWantsImageContextualMenuItems<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.imageAdditionContextualMenuItems` event is posted. Default delay set to 0.00 seconds. The `info` dictionary contains:
   + `glyph`
   + `glyphEditor`
   + `locationInGlyph`
   + `deviceState`
   + `NSEvent`
   + `itemDescriptions`

#### <method>glyphEditorWantsToolbarItems<arguments>(self, info)</arguments></method>

Available when registered as a RoboFont subscriber: `registerRoboFontSubscriber`. The `info` dictionary contains:
   + `itemDescriptions`

#### <method>spaceCenterWillOpen<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.spaceCenterWillOpen` event is posted. Default delay set to 0.00 seconds.

#### <method>spaceCenterDidOpen<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.spaceCenterDidOpen` event is posted. Default delay set to 0.00 seconds.

#### <method>spaceCenterWillClose<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.spaceCenterWillClose` event is posted. Default delay set to 0.00 seconds.

#### <method>spaceCenterDidKeyDown<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.spaceCenterKeyDown` event is posted. Default delay set to 0.00 seconds.

#### <method>spaceCenterDidKeyUp<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.spaceCenterKeyUp` event is posted. Default delay set to 0.00 seconds.

#### <method>spaceCenterDidChangeSelection<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.spaceCenterSelectionChanged` event is posted. Default delay set to 0.00 seconds.

#### <method>spaceCenterDidChangeText<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.spaceCenterInputChanged` event is posted. Default delay set to 0.00 seconds.

#### <method>spaceCenterWantsContextualMenuItems<arguments>(self, info)</arguments></method>

This will be called when a `mojo.events.spaceCenterAdditionContextualMenuItems` event is posted. Default delay set to 0.00 seconds.


## Data Manipulation Callbacks

This collection of callbacks is helpful in tracking edits over font data. Callbacks are composed in a predictive way, using three different elements:

- environment filter (optional)
   - `glyphEditor`
   - `currentFont`
   - `currentGlyph`
   - `adjunctFont`
   - `adjunctGlyph`

- font data
   - `Font`
   - `Layer`
   - `Glyph`

- event
   - `didSetGlyph`
   - `didRemoveGlyph`
   - `didChangeMetrics`
   - and so on...

To leverage the environment filters you will need to activate your subscriber object through a registering function like `registerGlyphEditorSuscriber`.

For example, RoboFont will trigger `glyphEditorFontDidChangeGuidelines` callback after a guideline is edited anywhere in the font where the glyph set in the glyph editor belongs. Or the `currentFontLayerDidChangeName` will be triggered immediately after a layer from the current font changes its name.

### No environment filter

Only font data plus event. Use with caution as many of these callbacks are triggered often.

#### <method>fontDidChange<arguments>(self, info)</arguments></method>

This will be called when any of `Font.Changed` are posted. Default delay: 0.033 seconds.

#### <method>fontDidReloadGlyphs<arguments>(self, info)</arguments></method>

This will be called when any of `Font.ReloadedGlyphs` are posted. Default delay: 0.033 seconds.

#### <method>fontDidChangeGlyphOrder<arguments>(self, info)</arguments></method>

This will be called when any of `Font.GlyphOrderChanged` are posted. Default delay: 0.033 seconds.

#### <method>fontDidChangeGuidelines<arguments>(self, info)</arguments></method>

This will be called when any of `Font.GuidelinesChanged` are posted. Default delay: 0.033 seconds.

#### <method>fontInfoDidChange<arguments>(self, info)</arguments></method>

This will be called when any of `Info.Changed` are posted. Default delay: 0.033 seconds.

#### <method>fontInfoDidChangeValue<arguments>(self, info)</arguments></method>

This will be called when any of `Info.ValueChanged` are posted. Default delay: 0.033 seconds.

#### <method>fontKerningDidChange<arguments>(self, info)</arguments></method>

This will be called when any of `Kerning.Changed` are posted. Default delay: 0.033 seconds.

#### <method>fontKerningDidChangePair<arguments>(self, info)</arguments></method>

This will be called when any of `Kerning.PairSet`, `Kerning.PairDeleted` are posted. Default delay: 0.033 seconds.

#### <method>fontKerningDidClear<arguments>(self, info)</arguments></method>

This will be called when any of `Kerning.Cleared` are posted. Default delay: 0.033 seconds.

#### <method>fontKerningDidUpdate<arguments>(self, info)</arguments></method>

This will be called when any of `Kerning.Updated` are posted. Default delay: 0.033 seconds.

#### <method>fontGroupsDidChange<arguments>(self, info)</arguments></method>

This will be called when any of `Groups.Changed` are posted. Default delay: 0.033 seconds.

#### <method>fontGroupsDidChangeGroup<arguments>(self, info)</arguments></method>

This will be called when any of `Groups.GroupSet`, `Groups.GroupDeleted` are posted. Default delay: 0.033 seconds.

#### <method>fontGroupsDidClear<arguments>(self, info)</arguments></method>

This will be called when any of `Groups.Cleared` are posted. Default delay: 0.033 seconds.

#### <method>fontGroupsDidUpdate<arguments>(self, info)</arguments></method>

This will be called when any of `Groups.Updated` are posted. Default delay: 0.033 seconds.

#### <method>fontFeaturesDidChange<arguments>(self, info)</arguments></method>

This will be called when any of `Features.Changed` are posted. Default delay: 0.033 seconds.

#### <method>fontFeaturesDidChangeText<arguments>(self, info)</arguments></method>

This will be called when any of `Features.TextChanged` are posted. Default delay: 0.033 seconds.

#### <method>fontLayersDidChange<arguments>(self, info)</arguments></method>

This will be called when any of `LayerSet.LayersChanged` are posted. Default delay: 0.033 seconds.

#### <method>fontLayersDidChangeLayer<arguments>(self, info)</arguments></method>

This will be called when any of `LayerSet.LayerChanged` are posted. Default delay: 0.033 seconds.

#### <method>fontLayersDidSetDefaultLayer<arguments>(self, info)</arguments></method>

This will be called when any of `LayerSet.DefaultLayerChanged` are posted. Default delay: 0.033 seconds.

#### <method>fontLayersDidChangeOrder<arguments>(self, info)</arguments></method>

This will be called when any of `LayerSet.LayerOrderChanged` are posted. Default delay: 0.033 seconds.

#### <method>fontLayersDidAddLayer<arguments>(self, info)</arguments></method>

This will be called when any of `LayerSet.LayerAdded` are posted. Default delay: 0.033 seconds.

#### <method>fontLayersDidRemoveLayer<arguments>(self, info)</arguments></method>

This will be called when any of `LayerSet.LayerDeleted` are posted. Default delay: 0.033 seconds.

#### <method>fontLayersDidChangeLayerName<arguments>(self, info)</arguments></method>

This will be called when any of `LayerSet.LayerNameChanged` are posted. Default delay: 0.033 seconds.

#### <method>layerDidChange<arguments>(self, info)</arguments></method>

This will be called when any of `Layer.Changed` are posted. Default delay: 0.033 seconds.

#### <method>layerDidChangeGlyphs<arguments>(self, info)</arguments></method>

This will be called when any of `Layer.GlyphsChanged` are posted. Default delay: 0.033 seconds.

#### <method>layerDidChangeGlyph<arguments>(self, info)</arguments></method>

This will be called when any of `Layer.GlyphChanged` are posted. Default delay: 0.033 seconds.

#### <method>layerDidAddGlyph<arguments>(self, info)</arguments></method>

This will be called when any of `Layer.GlyphAdded` are posted. Default delay: 0.033 seconds.

#### <method>layerDidRemoveGlyph<arguments>(self, info)</arguments></method>

This will be called when any of `Layer.GlyphDeleted` are posted. Default delay: 0.033 seconds.

#### <method>layerDidChangeGlyphName<arguments>(self, info)</arguments></method>

This will be called when any of `Layer.GlyphNameChanged` are posted. Default delay: 0.033 seconds.

#### <method>layerDidChangeGlyphUnicodes<arguments>(self, info)</arguments></method>

This will be called when any of `Layer.GlyphUnicodesChanged` are posted. Default delay: 0.033 seconds.

#### <method>layerDidChangeName<arguments>(self, info)</arguments></method>

This will be called when any of `Layer.NameChanged` are posted. Default delay: 0.033 seconds.

#### <method>layerDidChangeColor<arguments>(self, info)</arguments></method>

This will be called when any of `Layer.ColorChanged` are posted. Default delay: 0.033 seconds.

#### <method>glyphDidChange<arguments>(self, info)</arguments></method>

This will be called when any of `Glyph.Changed` are posted. Default delay: 0.033 seconds.

#### <method>glyphDidChangeInfo<arguments>(self, info)</arguments></method>

This will be called when any of `Glyph.NameChanged`, `Glyph.UnicodesChanged`, `Glyph.NoteChanged`, `Glyph.MarkColorChanged` are posted. Default delay: 0.033 seconds.

#### <method>glyphDidChangeMetrics<arguments>(self, info)</arguments></method>

This will be called when any of `Glyph.WidthChanged`, `Glyph.HeightChanged`, `Glyph.LeftMarginDidChange`, `Glyph.RightMarginDidChange`, `Glyph.TopMarginDidChange`, `Glyph.BottomMarginDidChange`, `Glyph.VerticalOriginChanged` are posted. Default delay: 0.033 seconds.

#### <method>glyphDidChangeOutline<arguments>(self, info)</arguments></method>

This will be called when any of `Glyph.ContoursChanged`, `Glyph.ComponentsChanged` are posted. Default delay: 0.033 seconds.

#### <method>glyphDidChangeContours<arguments>(self, info)</arguments></method>

This will be called when any of `Glyph.ContoursChanged` are posted. Default delay: 0.033 seconds.

#### <method>glyphDidChangeComponents<arguments>(self, info)</arguments></method>

This will be called when any of `Glyph.ComponentsChanged` are posted. Default delay: 0.033 seconds.

#### <method>glyphDidChangeAnchors<arguments>(self, info)</arguments></method>

This will be called when any of `Glyph.AnchorsChanged` are posted. Default delay: 0.033 seconds.

#### <method>glyphDidChangeGuidelines<arguments>(self, info)</arguments></method>

This will be called when any of `Glyph.GuidelinesChanged` are posted. Default delay: 0.033 seconds.

#### <method>glyphDidChangeImage<arguments>(self, info)</arguments></method>

This will be called when any of `Glyph.ImageChanged` are posted. Default delay: 0.033 seconds.

#### <method>glyphDidChangeSelection<arguments>(self, info)</arguments></method>

This will be called when any of `Glyph.SelectionChanged` are posted. Default delay: 0.033 seconds.

#### <method>glyphDidStartChangeSelection<arguments>(self, info)</arguments></method>

This will be called when any of `Glyph.SelectionStarted` are posted. Default delay set to 0.00 seconds.

#### <method>glyphDidEndChangeSelection<arguments>(self, info)</arguments></method>

This will be called when any of `Glyph.SelectionEnded` are posted. Default delay set to 0.00 seconds.

#### <method>glyphDidChangeMeasurements<arguments>(self, info)</arguments></method>

This will be called when any of `Glyph.MeasurementAdded`, `Glyph.MeasurementsCleared`, `Glyph.MeasurementChanged` are posted. Default delay: 0.033 seconds.

### Glyph Editor

Remember to register your subscriber with `registerGlyphEditorSubscriber` to use these callbacks

#### <method>glyphEditorFontDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `fontDidChange` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorFontDidReloadGlyphs<arguments>(self, info)</arguments></method>

This does the same thing as `fontDidReloadGlyphs` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorFontDidChangeGlyphOrder<arguments>(self, info)</arguments></method>

This does the same thing as `fontDidChangeGlyphOrder` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorFontDidChangeGuidelines<arguments>(self, info)</arguments></method>

This does the same thing as `fontDidChangeGuidelines` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorFontInfoDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `fontInfoDidChange` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorFontInfoDidChangeValue<arguments>(self, info)</arguments></method>

This does the same thing as `fontInfoDidChangeValue` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorFontKerningDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `fontKerningDidChange` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorFontKerningDidChangePair<arguments>(self, info)</arguments></method>

This does the same thing as `fontKerningDidChangePair` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorFontKerningDidClear<arguments>(self, info)</arguments></method>

This does the same thing as `fontKerningDidClear` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorFontKerningDidUpdate<arguments>(self, info)</arguments></method>

This does the same thing as `fontKerningDidUpdate` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorFontGroupsDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `fontGroupsDidChange` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorFontGroupsDidChangeGroup<arguments>(self, info)</arguments></method>

This does the same thing as `fontGroupsDidChangeGroup` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorFontGroupsDidClear<arguments>(self, info)</arguments></method>

This does the same thing as `fontGroupsDidClear` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorFontGroupsDidUpdate<arguments>(self, info)</arguments></method>

This does the same thing as `fontGroupsDidUpdate` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorFontFeaturesDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `fontFeaturesDidChange` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorFontFeaturesDidChangeText<arguments>(self, info)</arguments></method>

This does the same thing as `fontFeaturesDidChangeText` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorFontLayersDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidChange` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorFontLayersDidChangeLayer<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidChangeLayer` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorFontLayersDidSetDefaultLayer<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidSetDefaultLayer` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorFontLayersDidChangeOrder<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidChangeOrder` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorFontLayersDidAddLayer<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidAddLayer` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorFontLayersDidRemoveLayer<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidRemoveLayer` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorFontLayersDidChangeLayerName<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidChangeLayerName` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorLayerDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChange` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorLayerDidChangeGlyphs<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChangeGlyphs` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorLayerDidChangeGlyph<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChangeGlyph` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorLayerDidAddGlyph<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidAddGlyph` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorLayerDidRemoveGlyph<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidRemoveGlyph` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorLayerDidChangeGlyphName<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChangeGlyphName` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorLayerDidChangeGlyphUnicodes<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChangeGlyphUnicodes` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorLayerDidChangeName<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChangeName` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorLayerDidChangeColor<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChangeColor` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorGlyphDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChange` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorGlyphDidChangeInfo<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeInfo` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorGlyphDidChangeMetrics<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeMetrics` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorGlyphDidChangeOutline<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeOutline` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorGlyphDidChangeContours<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeContours` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorGlyphDidChangeComponents<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeComponents` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorGlyphDidChangeAnchors<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeAnchors` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorGlyphDidChangeGuidelines<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeGuidelines` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorGlyphDidChangeImage<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeImage` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorGlyphDidChangeSelection<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeSelection` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorGlyphDidStartChangeSelection<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidStartChangeSelection` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorGlyphDidEndChangeSelection<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidEndChangeSelection` but is only called for the glyph in the glyph editor.

#### <method>glyphEditorGlyphDidChangeMeasurements<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeMeasurements` but is only called for the glyph in the glyph editor.

### Current Font

Remember to register your subscriber with `registerCurrentFontSubscriber` to use these callbacks

#### <method>currentFontDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `fontDidChange` but is only called for the current font.

#### <method>currentFontDidReloadGlyphs<arguments>(self, info)</arguments></method>

This does the same thing as `fontDidReloadGlyphs` but is only called for the current font.

#### <method>currentFontDidChangeGlyphOrder<arguments>(self, info)</arguments></method>

This does the same thing as `fontDidChangeGlyphOrder` but is only called for the current font.

#### <method>currentFontDidChangeGuidelines<arguments>(self, info)</arguments></method>

This does the same thing as `fontDidChangeGuidelines` but is only called for the current font.

#### <method>currentFontInfoDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `fontInfoDidChange` but is only called for the current font.

#### <method>currentFontInfoDidChangeValue<arguments>(self, info)</arguments></method>

This does the same thing as `fontInfoDidChangeValue` but is only called for the current font.

#### <method>currentFontKerningDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `fontKerningDidChange` but is only called for the current font.

#### <method>currentFontKerningDidChangePair<arguments>(self, info)</arguments></method>

This does the same thing as `fontKerningDidChangePair` but is only called for the current font.

#### <method>currentFontKerningDidClear<arguments>(self, info)</arguments></method>

This does the same thing as `fontKerningDidClear` but is only called for the current font.

#### <method>currentFontKerningDidUpdate<arguments>(self, info)</arguments></method>

This does the same thing as `fontKerningDidUpdate` but is only called for the current font.

#### <method>currentFontGroupsDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `fontGroupsDidChange` but is only called for the current font.

#### <method>currentFontGroupsDidChangeGroup<arguments>(self, info)</arguments></method>

This does the same thing as `fontGroupsDidChangeGroup` but is only called for the current font.

#### <method>currentFontGroupsDidClear<arguments>(self, info)</arguments></method>

This does the same thing as `fontGroupsDidClear` but is only called for the current font.

#### <method>currentFontGroupsDidUpdate<arguments>(self, info)</arguments></method>

This does the same thing as `fontGroupsDidUpdate` but is only called for the current font.

#### <method>currentFontFeaturesDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `fontFeaturesDidChange` but is only called for the current font.

#### <method>currentFontFeaturesDidChangeText<arguments>(self, info)</arguments></method>

This does the same thing as `fontFeaturesDidChangeText` but is only called for the current font.

#### <method>currentFontLayersDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidChange` but is only called for the current font.

#### <method>currentFontLayersDidChangeLayer<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidChangeLayer` but is only called for the current font.

#### <method>currentFontLayersDidSetDefaultLayer<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidSetDefaultLayer` but is only called for the current font.

#### <method>currentFontLayersDidChangeOrder<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidChangeOrder` but is only called for the current font.

#### <method>currentFontLayersDidAddLayer<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidAddLayer` but is only called for the current font.

#### <method>currentFontLayersDidRemoveLayer<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidRemoveLayer` but is only called for the current font.

#### <method>currentFontLayersDidChangeLayerName<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidChangeLayerName` but is only called for the current font.

#### <method>currentFontLayerDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChange` but is only called for the current font. This should be used sparingly as it can result in a high number of calls. Refer to the Subscriber examples for more information on when this should be used.

#### <method>currentFontLayerDidChangeGlyphs<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChangeGlyphs` but is only called for the current font. This should be used sparingly as it can result in a high number of calls. Refer to the Subscriber examples for more information on when this should be used.

#### <method>currentFontLayerDidChangeGlyph<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChangeGlyph` but is only called for the current font. This should be used sparingly as it can result in a high number of calls. Refer to the Subscriber examples for more information on when this should be used.

#### <method>currentFontLayerDidAddGlyph<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidAddGlyph` but is only called for the current font. This should be used sparingly as it can result in a high number of calls. Refer to the Subscriber examples for more information on when this should be used.

#### <method>currentFontLayerDidRemoveGlyph<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidRemoveGlyph` but is only called for the current font. This should be used sparingly as it can result in a high number of calls. Refer to the Subscriber examples for more information on when this should be used.

#### <method>currentFontLayerDidChangeGlyphName<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChangeGlyphName` but is only called for the current font. This should be used sparingly as it can result in a high number of calls. Refer to the Subscriber examples for more information on when this should be used.

#### <method>currentFontLayerDidChangeGlyphUnicodes<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChangeGlyphUnicodes` but is only called for the current font. This should be used sparingly as it can result in a high number of calls. Refer to the Subscriber examples for more information on when this should be used.

#### <method>currentFontLayerDidChangeName<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChangeName` but is only called for the current font. This should be used sparingly as it can result in a high number of calls. Refer to the Subscriber examples for more information on when this should be used.

#### <method>currentFontLayerDidChangeColor<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChangeColor` but is only called for the current font. This should be used sparingly as it can result in a high number of calls. Refer to the Subscriber examples for more information on when this should be used.

#### <method>currentFontGlyphDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChange` but is only called for the current font. This should be used sparingly as it can result in a high number of calls. Refer to the Subscriber examples for more information on when this should be used.

#### <method>currentFontGlyphDidChangeInfo<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeInfo` but is only called for the current font. This should be used sparingly as it can result in a high number of calls. Refer to the Subscriber examples for more information on when this should be used.

#### <method>currentFontGlyphDidChangeMetrics<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeMetrics` but is only called for the current font. This should be used sparingly as it can result in a high number of calls. Refer to the Subscriber examples for more information on when this should be used.

#### <method>currentFontGlyphDidChangeOutline<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeOutline` but is only called for the current font. This should be used sparingly as it can result in a high number of calls. Refer to the Subscriber examples for more information on when this should be used.

#### <method>currentFontGlyphDidChangeContours<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeContours` but is only called for the current font. This should be used sparingly as it can result in a high number of calls. Refer to the Subscriber examples for more information on when this should be used.

#### <method>currentFontGlyphDidChangeComponents<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeComponents` but is only called for the current font. This should be used sparingly as it can result in a high number of calls. Refer to the Subscriber examples for more information on when this should be used.

#### <method>currentFontGlyphDidChangeAnchors<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeAnchors` but is only called for the current font. This should be used sparingly as it can result in a high number of calls. Refer to the Subscriber examples for more information on when this should be used.

#### <method>currentFontGlyphDidChangeGuidelines<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeGuidelines` but is only called for the current font. This should be used sparingly as it can result in a high number of calls. Refer to the Subscriber examples for more information on when this should be used.

#### <method>currentFontGlyphDidChangeImage<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeImage` but is only called for the current font. This should be used sparingly as it can result in a high number of calls. Refer to the Subscriber examples for more information on when this should be used.

#### <method>currentFontGlyphDidChangeSelection<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeSelection` but is only called for the current font. This should be used sparingly as it can result in a high number of calls. Refer to the Subscriber examples for more information on when this should be used.

#### <method>currentFontGlyphDidStartChangeSelection<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidStartChangeSelection` but is only called for the current font. This should be used sparingly as it can result in a high number of calls. Refer to the Subscriber examples for more information on when this should be used.

#### <method>currentFontGlyphDidEndChangeSelection<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidEndChangeSelection` but is only called for the current font. This should be used sparingly as it can result in a high number of calls. Refer to the Subscriber examples for more information on when this should be used.

#### <method>currentFontGlyphDidChangeMeasurements<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeMeasurements` but is only called for the current font. This should be used sparingly as it can result in a high number of calls. Refer to the Subscriber examples for more information on when this should be used.

### Current Glyph

Remember to register your subscriber with `registerCurrentGlyphSubscriber` to use these callbacks

#### <method>currentGlyphFontDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `fontDidChange` but is only called for the current glyph.

#### <method>currentGlyphFontDidReloadGlyphs<arguments>(self, info)</arguments></method>

This does the same thing as `fontDidReloadGlyphs` but is only called for the current glyph.

#### <method>currentGlyphFontDidChangeGlyphOrder<arguments>(self, info)</arguments></method>

This does the same thing as `fontDidChangeGlyphOrder` but is only called for the current glyph.

#### <method>currentGlyphFontDidChangeGuidelines<arguments>(self, info)</arguments></method>

This does the same thing as `fontDidChangeGuidelines` but is only called for the current glyph.

#### <method>currentGlyphFontInfoDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `fontInfoDidChange` but is only called for the current glyph.

#### <method>currentGlyphFontInfoDidChangeValue<arguments>(self, info)</arguments></method>

This does the same thing as `fontInfoDidChangeValue` but is only called for the current glyph.

#### <method>currentGlyphFontKerningDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `fontKerningDidChange` but is only called for the current glyph.

#### <method>currentGlyphFontKerningDidChangePair<arguments>(self, info)</arguments></method>

This does the same thing as `fontKerningDidChangePair` but is only called for the current glyph.

#### <method>currentGlyphFontKerningDidClear<arguments>(self, info)</arguments></method>

This does the same thing as `fontKerningDidClear` but is only called for the current glyph.

#### <method>currentGlyphFontKerningDidUpdate<arguments>(self, info)</arguments></method>

This does the same thing as `fontKerningDidUpdate` but is only called for the current glyph.

#### <method>currentGlyphFontGroupsDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `fontGroupsDidChange` but is only called for the current glyph.

#### <method>currentGlyphFontGroupsDidChangeGroup<arguments>(self, info)</arguments></method>

This does the same thing as `fontGroupsDidChangeGroup` but is only called for the current glyph.

#### <method>currentGlyphFontGroupsDidClear<arguments>(self, info)</arguments></method>

This does the same thing as `fontGroupsDidClear` but is only called for the current glyph.

#### <method>currentGlyphFontGroupsDidUpdate<arguments>(self, info)</arguments></method>

This does the same thing as `fontGroupsDidUpdate` but is only called for the current glyph.

#### <method>currentGlyphFontFeaturesDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `fontFeaturesDidChange` but is only called for the current glyph.

#### <method>currentGlyphFontFeaturesDidChangeText<arguments>(self, info)</arguments></method>

This does the same thing as `fontFeaturesDidChangeText` but is only called for the current glyph.

#### <method>currentGlyphFontLayersDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidChange` but is only called for the current glyph.

#### <method>currentGlyphFontLayersDidChangeLayer<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidChangeLayer` but is only called for the current glyph.

#### <method>currentGlyphFontLayersDidSetDefaultLayer<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidSetDefaultLayer` but is only called for the current glyph.

#### <method>currentGlyphFontLayersDidChangeOrder<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidChangeOrder` but is only called for the current glyph.

#### <method>currentGlyphFontLayersDidAddLayer<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidAddLayer` but is only called for the current glyph.

#### <method>currentGlyphFontLayersDidRemoveLayer<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidRemoveLayer` but is only called for the current glyph.

#### <method>currentGlyphFontLayersDidChangeLayerName<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidChangeLayerName` but is only called for the current glyph.

#### <method>currentGlyphLayerDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChange` but is only called for the current glyph.

#### <method>currentGlyphLayerDidChangeGlyphs<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChangeGlyphs` but is only called for the current glyph.

#### <method>currentGlyphLayerDidChangeGlyph<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChangeGlyph` but is only called for the current glyph.

#### <method>currentGlyphLayerDidAddGlyph<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidAddGlyph` but is only called for the current glyph.

#### <method>currentGlyphLayerDidRemoveGlyph<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidRemoveGlyph` but is only called for the current glyph.

#### <method>currentGlyphLayerDidChangeGlyphName<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChangeGlyphName` but is only called for the current glyph.

#### <method>currentGlyphLayerDidChangeGlyphUnicodes<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChangeGlyphUnicodes` but is only called for the current glyph.

#### <method>currentGlyphLayerDidChangeName<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChangeName` but is only called for the current glyph.

#### <method>currentGlyphLayerDidChangeColor<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChangeColor` but is only called for the current glyph.

#### <method>currentGlyphDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChange` but is only called for the current glyph.

#### <method>currentGlyphDidChangeInfo<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeInfo` but is only called for the current glyph.

#### <method>currentGlyphDidChangeMetrics<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeMetrics` but is only called for the current glyph.

#### <method>currentGlyphDidChangeOutline<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeOutline` but is only called for the current glyph.

#### <method>currentGlyphDidChangeContours<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeContours` but is only called for the current glyph.

#### <method>currentGlyphDidChangeComponents<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeComponents` but is only called for the current glyph.

#### <method>currentGlyphDidChangeAnchors<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeAnchors` but is only called for the current glyph.

#### <method>currentGlyphDidChangeGuidelines<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeGuidelines` but is only called for the current glyph.

#### <method>currentGlyphDidChangeImage<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeImage` but is only called for the current glyph.

#### <method>currentGlyphDidChangeSelection<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeSelection` but is only called for the current glyph.

#### <method>currentGlyphDidStartChangeSelection<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidStartChangeSelection` but is only called for the current glyph.

#### <method>currentGlyphDidEndChangeSelection<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidEndChangeSelection` but is only called for the current glyph.

#### <method>currentGlyphDidChangeMeasurements<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeMeasurements` but is only called for the current glyph.

### Adjunct Font

Remember to set some fonts to be observed with `Subscriber.setAdjunctObjectsToObserve()` before using these callbacks

#### <method>adjunctFontDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `fontDidChange` but is only called for adjunct objects.

#### <method>adjunctFontDidReloadGlyphs<arguments>(self, info)</arguments></method>

This does the same thing as `fontDidReloadGlyphs` but is only called for adjunct objects.

#### <method>adjunctFontDidChangeGlyphOrder<arguments>(self, info)</arguments></method>

This does the same thing as `fontDidChangeGlyphOrder` but is only called for adjunct objects.

#### <method>adjunctFontDidChangeGuidelines<arguments>(self, info)</arguments></method>

This does the same thing as `fontDidChangeGuidelines` but is only called for adjunct objects.

#### <method>adjunctFontInfoDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `fontInfoDidChange` but is only called for adjunct objects.

#### <method>adjunctFontInfoDidChangeValue<arguments>(self, info)</arguments></method>

This does the same thing as `fontInfoDidChangeValue` but is only called for adjunct objects.

#### <method>adjunctFontKerningDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `fontKerningDidChange` but is only called for adjunct objects.

#### <method>adjunctFontKerningDidChangePair<arguments>(self, info)</arguments></method>

This does the same thing as `fontKerningDidChangePair` but is only called for adjunct objects.

#### <method>adjunctFontKerningDidClear<arguments>(self, info)</arguments></method>

This does the same thing as `fontKerningDidClear` but is only called for adjunct objects.

#### <method>adjunctFontKerningDidUpdate<arguments>(self, info)</arguments></method>

This does the same thing as `fontKerningDidUpdate` but is only called for adjunct objects.

#### <method>adjunctFontGroupsDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `fontGroupsDidChange` but is only called for adjunct objects.

#### <method>adjunctFontGroupsDidChangeGroup<arguments>(self, info)</arguments></method>

This does the same thing as `fontGroupsDidChangeGroup` but is only called for adjunct objects.

#### <method>adjunctFontGroupsDidClear<arguments>(self, info)</arguments></method>

This does the same thing as `fontGroupsDidClear` but is only called for adjunct objects.

#### <method>adjunctFontGroupsDidUpdate<arguments>(self, info)</arguments></method>

This does the same thing as `fontGroupsDidUpdate` but is only called for adjunct objects.

#### <method>adjunctFontFeaturesDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `fontFeaturesDidChange` but is only called for adjunct objects.

#### <method>adjunctFontFeaturesDidChangeText<arguments>(self, info)</arguments></method>

This does the same thing as `fontFeaturesDidChangeText` but is only called for adjunct objects.

#### <method>adjunctFontLayersDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidChange` but is only called for adjunct objects.

#### <method>adjunctFontLayersDidChangeLayer<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidChangeLayer` but is only called for adjunct objects.

#### <method>adjunctFontLayersDidSetDefaultLayer<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidSetDefaultLayer` but is only called for adjunct objects.

#### <method>adjunctFontLayersDidChangeOrder<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidChangeOrder` but is only called for adjunct objects.

#### <method>adjunctFontLayersDidAddLayer<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidAddLayer` but is only called for adjunct objects.

#### <method>adjunctFontLayersDidRemoveLayer<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidRemoveLayer` but is only called for adjunct objects.

#### <method>adjunctFontLayersDidChangeLayerName<arguments>(self, info)</arguments></method>

This does the same thing as `fontLayersDidChangeLayerName` but is only called for adjunct objects.

### Adjunct Layer

Remember to set some layers to be observed with `Subscriber.setAdjunctObjectsToObserve()` before using these callbacks

#### <method>adjunctLayerDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChange` but is only called for adjunct objects.

#### <method>adjunctLayerDidChangeGlyphs<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChangeGlyphs` but is only called for adjunct objects.

#### <method>adjunctLayerDidChangeGlyph<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChangeGlyph` but is only called for adjunct objects.

#### <method>adjunctLayerDidAddGlyph<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidAddGlyph` but is only called for adjunct objects.

#### <method>adjunctLayerDidRemoveGlyph<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidRemoveGlyph` but is only called for adjunct objects.

#### <method>adjunctLayerDidChangeGlyphName<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChangeGlyphName` but is only called for adjunct objects.

#### <method>adjunctLayerDidChangeGlyphUnicodes<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChangeGlyphUnicodes` but is only called for adjunct objects.

#### <method>adjunctLayerDidChangeName<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChangeName` but is only called for adjunct objects.

#### <method>adjunctLayerDidChangeColor<arguments>(self, info)</arguments></method>

This does the same thing as `layerDidChangeColor` but is only called for adjunct objects.

### Adjunct Glyph

Remember to set some glyphs to be observed with `Subscriber.setAdjunctObjectsToObserve()` before using these callbacks

#### <method>adjunctGlyphDidChange<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChange` but is only called for adjunct objects.

#### <method>adjunctGlyphDidChangeInfo<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeInfo` but is only called for adjunct objects.

#### <method>adjunctGlyphDidChangeMetrics<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeMetrics` but is only called for adjunct objects.

#### <method>adjunctGlyphDidChangeOutline<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeOutline` but is only called for adjunct objects.

#### <method>adjunctGlyphDidChangeContours<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeContours` but is only called for adjunct objects.

#### <method>adjunctGlyphDidChangeComponents<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeComponents` but is only called for adjunct objects.

#### <method>adjunctGlyphDidChangeAnchors<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeAnchors` but is only called for adjunct objects.

#### <method>adjunctGlyphDidChangeGuidelines<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeGuidelines` but is only called for adjunct objects.

#### <method>adjunctGlyphDidChangeImage<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeImage` but is only called for adjunct objects.

#### <method>adjunctGlyphDidChangeSelection<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeSelection` but is only called for adjunct objects.

#### <method>adjunctGlyphDidStartChangeSelection<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidStartChangeSelection` but is only called for adjunct objects.

#### <method>adjunctGlyphDidEndChangeSelection<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidEndChangeSelection` but is only called for adjunct objects.

#### <method>adjunctGlyphDidChangeMeasurements<arguments>(self, info)</arguments></method>

This does the same thing as `glyphDidChangeMeasurements` but is only called for adjunct objects.

</div>
