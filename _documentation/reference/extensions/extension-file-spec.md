---
layout: page
title: Extension File Specification
treeTitle: Extension File Spec
tags:
  - extensions
---

* Table of Contents
{:toc}

RoboFont extensions are [macOS packages] – folders which act like single files. They have a [standard folder structure](#extension-folder-structure) and the file extension `.roboFontExt`.

[macOS packages]: http://en.wikipedia.org/wiki/Package_(macOS)

> You can view the contents of a package by right-clicking it in Finder, and selecting *Show Package Contents*.
{: .tip }


Extension Folder Structure
--------------------------

*Version 5.0*

> Support for `*.pyc` files has been dropped with v4.0
{: .warning }

Extension packages must have the following internal structure:

> myExtension.robofontExtension
> ├── [info.plist](#infoplist)
> ├── [lib](#lib)
> │   └── \*.py
> ├── ([html](#html))
> │   ├── index.html
> │   └── (\*.html)
> ├── ([resources](#resources))
> │   └── (any type of file)
> ├── ([license](#license))
> └── ([requirements.txt](#requirements))
{: .asCode }

Files and folders in parentheses are optional.


info.plist
----------

The `info.plist` is a [XML property list file][plist] containing extension metadata.

> - [Technical specification of the Property List format as DTD][plist DTD]
{: .seealso }

[plist]: http://en.wikipedia.org/wiki/Property_list
[plist DTD]: http://www.apple.com/DTDs/PropertyList-1.0.dtd

<table>
<tr>
  <th width='45%'>key</th>
  <th>value</th>
</tr>
{% for data in site.data.extensionsSpec.extensionBundle %}
<tr>
  <td rowspan='3'>
    <code class='highlighter-rouge'>{{ data.key }}</code>
  </td>
  <td>{{ data.description | markdownify }}</td>
</tr>
<tr>
  <td><code>{{ data.value }}</code></td>
</tr>
<tr>
  <td class="{{ data.required | slugify }}">{{ data.required | markdownify }}</td>
</tr>
{% endfor %}
</table>

### Custom keys

The `info.plist` may include custom keys if needed.

Custom entries should use **reverse domain names** as keys to avoid namespace conflicts with other extensions.

Example: `com.myDomain.myExtension.myCustomKey`

> In RoboFont 1, extension metadata for Mechanic was stored in the `info.plist` of each extension under the custom key `com.robofontmechanic.mechanic`.
>
> **This custom key is no longer used in RoboFont 3 / Mechanic 2.**
>
> Mechanic 2 introduces a different system for publishing extensions, with centralized storage of extension metadata in the [Mechanic 2 Server].
>
> See {% internallink 'how-tos/extensions/publishing-extensions' %} for more information.
{: .note }

[Mechanic 2 Server]: http://github.com/robofont-mechanic/mechanic-2-server


lib
---

The `lib` folder contains all `.py` files needed by the extension.

When an extension is installed, its `lib` folder is added to the `sys.path` – so all the other files and folders in the extension can be imported as modules with Python. To avoid namespace conflicts with other extensions, it is recommended to use **reverse domain names without dots** to prefix extension modules.

Example: `comMyDomainMyExtensionMyModule`

> [RoboFont 3.3]({{site.baseurl}}/downloads/)
{: .version-badge }

> See the [Boilerplate Extension](http://github.com/roboDocs/rf-extension-boilerplate) for a working example.
{: .tip }


html
----

not required
{: .not-required}

If an html folder is declared in the `info.plist`, then it must contain a file named `index.html`. This file must be a plain html file.

> Use the *Help* link in the extension’s submenu to open the html help in RoboFont’s {% internallink "how-tos/mojo/help-window" %} (a simple [WebKit] browser).
{: .tip }

[WebKit]: http://www.webkit.org/
[Markdown]: http://en.wikipedia.org/wiki/Markdown


resources
---------

not required
{: .not-required}

The `resources` folder is a place to store any additional files needed for your extension. It is commonly used for assets (such as images for toolbar icons and cursors), or for additional compiled command-line tools.


license
-------

not required
{: .not-required}

The license file contains the full license text for the extension.

This file must be either in `.txt` or `.html` format.


requirements
------------

not required
{: .not-required}

The `requirements.txt` file contains a list of other extensions which also need to be installed for the current extension to work.

For example, an extension which requires [DrawBot] and [Batch] should include a `requirements.txt` file with the names of both extensions, one per line:

```text
Batch
DrawBot
```

[Batch]: http://github.com/typemytype/batchRoboFontExtension
[DrawBot]: http://github.com/typemytype/drawBotRoboFontExtension


Menu Item Description
---------------------

When installed, an extension can add entries to the *Extensions* menu. These entries are defined as a list of *menu item descriptions*.

Each menu item description is a dictionary with the following keys:

<table>
<tr>
  <th width='25%'>key</th>
  <th>value</th>
</tr>
{% for data in site.data.extensionsSpec.menuItemDescription %}
<tr>
  <td rowspan='3'>
    <code class='highlighter-rouge'>{{ data.key }}</code>
  </td>
  <td>{{ data.description | markdownify }}</td>
</tr>
<tr>
  <td>{{ data.value }}</td>
</tr>
<tr>
  <td class="{{ data.required | slugify }}">{{ data.required | markdownify }}</td>
</tr>
{% endfor %}
</table>

> - *Modifier flags* are macOS native input masks which can be imported from `AppKit`. See the Boilerplate Extension’s [build script] for an example.
> - If no modifiers are given, ⌃ Ctrl and ⌘ Cmd are assigned to the short key.
> - If a short key is already in use, it will not be added to the menu item.
{: .note}

[build script]: http://github.com/roboDocs/rf-extension-boilerplate/blob/master/build.py
