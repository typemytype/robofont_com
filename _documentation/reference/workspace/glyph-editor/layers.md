---
layout: page
title: Layers
tags:
  - layers
---

* Table of Contents
{:toc}


Description
-----------

Source fonts may contain multiple independent layers.

Layers can be used for many purposes:

- to separate different stages of a project
- to facilitate collaboration with other designers
- to store multiple sources of a typeface
- to store different layers of a color font
- *anything else you can think of!*


Edit layers
------------

You can edit the font layers using the {% internallink "/documentation/reference/workspace/layers-popover" text="layers popover" %}.

You can access the *layers popover* from the button sitting at the top right of the {% internallink 'workspace/glyph-editor' text='glyph editor' %} (where the name of current layer is also shown)

{% image reference/workspace/glyph-editor/layers-popover.png %}

Inside the glyph editor, you can use the shortkeys `Cmd ↓` and `Cmd ↑` to move to the previous / next layer.

You can also access the *layers popover* from the {% internallink "/documentation/reference/workspace/font-overview/#toolbar" text="Font Overview Toolbar" %}.

{% image reference/workspace/font-overview/layers-popover.png %}


Layers popup
------------

The *Layers* pop-up window offers a quick way to copy between layers of the same glyph. It can be opened by pressing `L` while in the Glyph Editor.

{% image reference/workspace/glyph-editor/glyph-editor_jump-to-layer.png %}

<table>
  <thead>
    <tr>
      <th width="25%">option</th>
      <th width="75%">action</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Copy</td>
      <td>Copy the contents of the current layer to one or more layers.</td>
    </tr>
    <tr>
      <td>Move</td>
      <td>Copy the contents of the current layer to one or more layers, and clear the contents of the current layer.</td>
    </tr>
    <tr>
      <td>Flip</td>
      <td>Switch the contents of the current layer with the contents of another layer, and vice-versa.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Copy Metrics</td>
      <td>Copy glyph width when copying between layers.</td>
    </tr>
  </tbody>
</table>


Adding layers
-------------

Layers are added with the *Add Layer sheet*, which can be opened by choosing *Create New Layer* from the Layers menu, or by clicking on the + button in the Layers Inspector.

{% image reference/workspace/glyph-editor/glyph-editor_add-layer.png %}

<table>
  <tr>
    <th width='35%'>action</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>Layer Name</td>
    <td>Provide a name for the layer (required).<br/>Layer names must be unique.</td>
  </tr>
  <tr>
    <td>Layer Color</td>
    <td>Choose a color for the layer. This color is used to display the layer when it is in the background.</td>
  </tr>
  <tr>
    <td>Add or Enter</td>
    <td>Create the new layer and switch to it.</td>
  </tr>
</table>


Removing layers
---------------

Layers can be deleted from the *layers popover*. Select a layer and click the - button.

- - -

> - UFO3 Specification > [layercontents.plist](http://unifiedfontobject.org/versions/ufo3/layercontents.plist) / [layerinfo.plist](http://unifiedfontobject.org/versions/ufo3/glyphs/layerinfo.plist)
> - {% internallink 'reference/api/fontParts/rlayer' text='FontParts API > RLayer' %}
{: .seealso }
