---
layout: page
title: Guidelines
tags:
  - designing
---

* Table of Contents
{:toc}

Definition
----------

Guidelines are moveable lines which can be used as visual aids during the design process. They are defined by a point and an angle, and are not part of the glyph’s contours.

{% image reference/workspace/glyph-editor/guidelines.png %}

Guidelines can be of two kinds:

font-level (global)
: displayed in all glyphs

glyph-level (local)
: displayed in one glyph only

Each guideline may also have a color attribute and a unique identifier.


Adding guidelines
-----------------

To create a new guideline, right-click on the Glyph View and select "Add Local Guideline" from the contextual menu.

{% image reference/workspace/glyph-editor/add_guideline.png %}

Or use the Click + add guidelines hot key (default is G) to create a new guideline.


Removing guidelines
-------------------

To remove a guideline, click and drag it outside the Glyph View. You'll see a `X` appearing near the cursor.

{% video reference/workspace/glyph-editor/remove_guideline.mp4 %}

Editing guides
--------------

Guidelines can be edited with the Guidelines Sheet or the [Guidelines Inspector][Guidelines].


Actions
-------

<table>
    <tr>
      <th width='35%'>action</th>
      <th width='65%'>description</th>
    </tr>
    <tr>
      <td>Click + add guidelines hot key (default is G)</td>
      <td>Add a guideline</td>
    </tr>
    <tr>
      <td>Click + ⌥ + add guidelines hot key (default is G)</td>
      <td>Add a global guideline</td>
    </tr>
    <tr>
      <td>Click + ⇧ + add guidelines hot key (default is G)</td>
      <td>Add a guideline following the italic angle (if set in the current font)</td>
    </tr>
    <tr>
      <td>Double-click on a guide</td>
      <td>Open the Guidelines Sheet</td>
    </tr>
    <tr>
      <td>Drag out of the view</td>
      <td>Delete the guideline</td>
    </tr>
    <tr>
      <td>⌥ + drag</td>
      <td>Rotate the guideline</td>
    </tr>
    <tr>
      <td>Mouse over</td>
      <td>Cursor displays guideline values</td>
    </tr>
    <tr>
      <td>Drag over point in glyph</td>
      <td>Snap the guideline to a point</td>
    </tr>
  </table>


Guidelines popover
------------------

All guideline properties can be edited with a Guidelines popover, which can be opened by double clicking on a guideline using the {% internallink 'tools/editing-tool' %}.

{% image reference/workspace/glyph-editor/guidelines_menu.png %}

<table>
  <thead>
    <tr>
      <th width='35%'>option</th>
      <th width='65%'>description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Name</td>
      <td>The name of the guideline. <em>(optional)</em></td>
    </tr>
    <tr>
      <td>Position</td>
      <td>The guideline’s center point as a <code>(x,y)</code> coordinate</td>
    </tr>
    <tr>
      <td>Angle</td>
      <td>The angle of the guideline</td>
    </tr>
    <tr>
      <td>Color</td>
      <td>The color of the guideline</td>
    </tr>
    <tr>
      <td>Is Global</td>
      <td>Make the guideline global (font-level) or local (only this glyph)</td>
    </tr>
    <tr>
      <td>Show Measurements</td>
      <td>Show measurements where the guideline interesects the glyph</td>
    </tr>
    <tr>
      <td>Locked</td>
      <td>Locks this guideline</td>
    </tr>
    <tr>
      <td>Magnetic</td>
      <td>Determine how fast a dragged selection should snap to the guideline</td>
    </tr>
  </tbody>
</table>

Contextual menu
--------------------

Some global guidelines properties can be edited from the Glyph View contextual menu (right-click on the Glyph View)

{% image reference/workspace/glyph-editor/glyph-editor_contextual-menu_globalGuideline.png %}

<table>
    <thead>
    <tr>
      <th width='35%'>option</th>
      <th width='65%'>description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Add Guideline</td>
      <td>Add a glyph guideline</td>
    </tr>
    <tr>
      <td>Remove Guidelines</td>
      <td>Remove all glyph guidelines</td>
    </tr>
    <tr>
      <td>Slice Glyph with Guideline</td>
      <td>Every contour intersecting the guideline will be sliced</td>
    </tr>
  </tbody>
</table>


Guidelines inspector
--------------------

The [Guidelines] section of the Inspector can also be used to edit the guideline’s attributes, including its color.

{% image reference/workspace/glyph-editor/inspector_guidelines.png %}

[Guidelines]: ../../inspector#guidelines


- - -

> - [UFO3 specification > Guideline](http://unifiedfontobject.org/versions/ufo3/glyphs/glif/#guideline)
> - {% internallink 'reference/api/fontParts/rguideline' text='FontParts > RGuideline' %}
{: .seealso }
