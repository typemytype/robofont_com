---
layout: page
title: Transform
tags:
---

* Table of Contents
{:toc}

The *Transform* mode offers a way to interactively transform the selected glyph objects or the whole glyph.

{% video reference/workspace/glyph-editor/transform.mp4 %}

Activating Transform mode
-------------------------

To enter Transform mode, select the {% internallink "editing-tool" %} and use the keyboard shortcut ⌘ + T, or choose *Glyph > Transform* from the main application menu. Triple-clicking a contour will also turn Transform mode on.

{% image reference/workspace/glyph-editor_transform.png %}

To exit Transform mode, double-click anywhere outside of the transformation box.


Transformations
---------------

When Transform mode is active, a frame with handles is displayed around the selected objects. Use the handles to scale, rotate or skew the selection.

### Translate

Translation is the default transformation, and is indicated by square corner points.

{% image reference/workspace/glyph-editor/transform-translate.png %}

<table>
  <tr>
    <th width='35%'>action</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>click + drag</td>
    <td>Translate the selection.</td>
  </tr>
  <tr>
    <td>click + drag corner or middle point</td>
    <td>Scale the selection.</td>
  </tr>
  <tr>
    <td>⇧ + click + drag</td>
    <td>Translate the selection on x, y or 45° axis.</td>
  </tr>
  <tr>
    <td>⇧ + click + drag corner or middle point</td>
    <td>Constrain translation to x, y or 45° axis.</td>
  </tr>
</table>

### Scale

Scale is activated when ⌘ (Command) is down, and is indicated by diamond-shaped corner points.

{% image reference/workspace/glyph-editor/transform-scale.png %}

<table>
  <tr>
    <th width='35%'>action</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>click + drag</td>
    <td>Start scaling the selection.</td>
  </tr>
  <tr>
    <td>⇧ + click + drag</td>
    <td>Constrain scale to x, y or 45° axis, or keep horizontal and vertical scale factors proportional.</td>
  </tr>
</table>

### Rotate

Rotation is activated when ⌥ (Alt) is down, and is indicated by round corner points.

{% image reference/workspace/glyph-editor/transform-rotate.png %}

<table>
  <tr>
    <th width='35%'>action</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>click + drag</td>
    <td>Click to define the center of rotation, and drag to rotate around it.</td>
  </tr>
  <tr>
    <td>⇧ + click + drag</td>
    <td>Constrain rotation to 0, 45° or 90°.</td>
  </tr>
</table>

### Skew

Skew is activated when ⌥ ⌘ (Alt + Command) are down, and is indicated by triangular corner points.

{% image reference/workspace/glyph-editor/transform-skew.png %}

<table>
  <tr>
    <th width='35%'>action</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>click + drag</td>
    <td>Click to define a reference point, and drag to skew around it.</td>
  </tr>
  <tr>
    <td>⇧ + click + drag</td>
    <td>Constrain skew angle to 0, 45° or 90°.</td>
  </tr>
</table>


Transform Inspector
-------------------

Transformations can also be applied using the [Transform Inspector](../../inspector#transform).

While lacking the interaction and the preview, this method provides several options which are not available when working in Transform mode, such as applying multiple transformations at once, or transforming multiple glyphs.

Holding down ⌥ while clicking the skew or rotate button will inverse the transformation.
{% image reference/workspace/glyph-editor/inspector_transform.png %}

> The transformation values in the Transform Inspector are updated whenever Transform mode is used. This makes it possible to apply the same transformation again on another glyph.
{: .tip }
