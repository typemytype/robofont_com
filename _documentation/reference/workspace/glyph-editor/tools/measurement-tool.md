---
layout: page
title: Measurement Tool
tags:
  - contours
---

* Table of Contents
{:toc}

{% image reference/workspace/toolbarToolMeasurement.png %}

The *Measurement* tool is an interactive tool to draw measurement lines across glyphs. Measurements include the distance, angle and horizontal / vertical projections between intersection points.

{% image reference/workspace/glyph-editor/tools/glyph-editor_measurement-line.png %}

To activate the tool, you can click on the ruler icon in the toolbar of the {% internallink 'workspace/glyph-editor' %} or select "Measurement Tool" in the {% internallink "reference/workspace/application-menu#glyph" text="Glyph application menu" %}.

> Measurements may also intersect with glyph and font metrics. See the settings *Measurements show glyph metrics* and *Measurements show font metrics* in the {% internallink 'workspace/preferences-window/glyph-view' %}.
{: .note }

> Use the [Display Options menu](../../#display-options) to toggle the measurements info box.
{: .note }


Actions
-------

{% video tutorials/drawingGlyphs_measurement-tool.mp4 %}

<table>
  <tr>
    <th width="35%">action</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>click</td>
    <td>Define the starting point of a measurement line.</td>
  </tr>
  <tr>
    <td>⌥ + click</td>
    <td>Enable multiple measurement lines.</td>
  </tr>
  <tr>
    <td>click (no drag)</td>
    <td>Remove all measurement lines.</td>
  </tr>
  <tr>
    <td>drag</td>
    <td>Set the end point of a measurement line.</td>
  </tr>
  <tr>
    <td>⇧ + drag</td>
    <td>Constrain the measurement line to x, y or 45° axes. When started on a segment, it will maintain the angle from the point on that segment.</td>
  </tr>
  <tr>
    <td>click + drag start/end point</td>
    <td>Modify the measurement line.</td>
  </tr>
</table>

> - {% internallink 'tutorials/drawing-glyphs' %}
> - {% internallink 'topics/drawing-environment' %}
{: .seealso }
