---
layout: page
title: Interactive tools
tree:
  - bezier-tool
  - editing-tool
  - slice-tool
  - measurement-tool
treeCanHide: true
---

<style>
#glyph-editor-tool .col { text-align: center; }
</style>

<div id="glyph-editor-tool" class='row'>
<div class='col' markdown=1>
{% image reference/workspace/toolbarToolsArrow.png %}
[Editing Tool]({% link _documentation/reference/workspace/glyph-editor/tools/editing-tool.md %})
</div>
<div class='col' markdown=1>
{% image reference/workspace/toolbarToolsPen.png %}
[Bezier Tool]({% link _documentation/reference/workspace/glyph-editor/tools/bezier-tool.md %})
</div>
<div class='col' markdown=1>
{% image reference/workspace/toolbarToolsSlice.png %}
[Slice Tool]({% link _documentation/reference/workspace/glyph-editor/tools/slice-tool.md %})
</div>
<div class='col' markdown=1>
{% image reference/workspace/toolbarToolMeasurement.png %}
[Measurement Tool]({% link _documentation/reference/workspace/glyph-editor/tools/measurement-tool.md %})
</div>
</div>


Glyph view events
-----------------

All interactive tools support the following mouse events:

<table>
  <tr>
    <th width="35%">event</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>space + click + drag</td>
    <td>Move the Glyph View inside the scroll window.</td>
  </tr>
  <tr>
    <td>⌘ + space + click</td>
    <td>Zoom in.</td>
  </tr>
  <tr>
    <td>⌘ + space + drag</td>
    <td>Zoom to marque rectangle.</td>
  </tr>
  <tr>
    <td>⌥ + ⌘ + space + click</td>
    <td>Zoom out.</td>
  </tr>
  <tr>
    <td>⌥ + mouse scroll</td>
    <td>Zoom in or out.</td>
  </tr>
  <tr>
    <td>drag an image into the Glyph View</td>
    <td>Create an Image object in the current glyph. Only one image per layer is allowed. Image formats <code>.png</code>, <code>.jpeg</code> and <code>.tiff</code> are supported.</td>
  </tr>
</table>


More interactive tools
----------------------

Many other interactive tools are available as {% internallink "topics/extensions" text='extensions' %}:

[Pixel Tool](http://github.com/typemytype/pixelToolRoboFontExtension)
: Draw with ‘pixels’ using rectangles, ovals or components.

[Shape Tool](http://github.com/typemytype/shapeToolRoboFontExtension)
: Draw primitive geometric shapes: rectangles and ovals.

[Scaling Edit Tool](http://github.com/klaavo/scalingEditTool)
: Editing Tool which scales off-curve points while moving on-curve points.

[Blue Zone Editor](http://github.com/andyclymer/BlueZoneEditor-roboFontExt)
: Edit blue zones visually inside the Glyph Editor.

[Bounding Tool](http://github.com/FontBureau/fbOpenTools/tree/master/BoundingTool)
: Editing Tool which displays the bounding box and divisions.

[Check Parallel Tool](http://github.com/jtanadi/CheckParallelTool)
: Check if lines connecting on-curve & off-curve points are parallel.

[Corner Tool](http://github.com/roboDocs/CornerTools)
: Round or cut contour corners by dragging to define the radius.
