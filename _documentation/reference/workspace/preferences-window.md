---
layout: page
title: Preferences Window
tags:
  - preferences
tree:
  - glyph-view
  - font-overview
  - space-center
  - short-keys
  - python
  - extensions
  - miscellaneous
treeCanHide: true
---

* Table of Contents
{:toc}

The *Preferences Window* is a place where all user defaults can be edited.

{% image reference/workspace/preferences.png %}


Preference Groups
-----------------

Preferences are grouped into sections and subsections:

{% tree page.url levels=1 %}


Applying changes
----------------

Click on the *Apply* button at the top right of the toolbar to apply the changes.

{% image reference/workspace/preferences_apply.png %}

Some changes may require a restart of RoboFont.

{% image reference/workspace/preferences_restart.png %}


Options
-------

Click on the gears icon at the bottom left to open a menu with options.

{% image reference/workspace/preferences_reset.png %}

<table>
  <tr>
    <th width='30%'>option</th>
    <th width='70%'>description</th>
  </tr>
  <tr>
    <td>Reset All</td>
    <td>Restore all Preferences to their default values.</td>
  </tr>
  <tr>
    <td>Export</td>
    <td>Export all Preferences to a <code>.roboFontSettings</code> file.</td>
  </tr>
  <tr>
    <td>Import</td>
    <td>Import Preferences from a <code>.roboFontSettings</code> file.</td>
  </tr>
</table>

- - -

> Preferences can also be changed using the {% internallink "workspace/preferences-editor" %}.
{: .seealso }
