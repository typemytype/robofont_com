---
layout: page
title: Add Glyphs sheet
tags:
  - character set
  - glyph names
---

* Table of Contents
{:toc}

The *Add Glyphs* sheet is a simple interface to add glyphs to the current font.

To open the sheet, choose *Font > Add Glyphs* from the {% internallink "workspace/application-menu" %} or use the shortcut keys ⌃ ⌘ G.

{% image how-tos/adding-and-removing-glyphs_add-glyphs-sheet.png %}

Use the text input area to fill in the names of the glyphs you would like to create, as a space-separated list.

The new glyphs can be added to the font as empty glyphs, or as [template glyphs](../#template-glyphs) if the option *Add As Template Glyphs* is selected.


Options
-------

<table>
  <thead>
    <tr>
      <th width="35%">option</th>
      <th width="65%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Import glyph names from…</td>
      <td>Import glyph names from <a href="../../../workspace/preferences-window/font-overview">saved character sets</a>.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Add unicode</td>
      <td>Try to set the unicode values for the given glyph names automatically.</td>
    </tr>
    <tr>
      <td>Sort Font</td>
      <td>Sort the glyphs in the font (alphabetically).</td>
    </tr>
    <tr>
      <td>Overwrite Glyphs</td>
      <td>Overwrite glyphs if they already exist in the font.</td>
    </tr>
    <tr>
      <td>Add As Template Glyphs</td>
      <td>Add glyphs as template glyphs only. If this option is unchecked, empty new glyphs will be created instead.</td>
    </tr>
  <tr>
      <td>Mark Color</td>
      <td>Click on the color swatch to open a color picker, and choose a mark color for the new glyphs.</td>
    </tr>
  </tbody>
</table>


Simple glyph construction
-------------------------

The Add Glyphs sheet supports a very basic glyph construction syntax to build new glyphs from components. Example:

```text
uni0430=a
aacute=a+acute
aringacute=a+ring+acute
```

Use `@` to align components using anchors:

```text
aacute=a+acute@top
```

> This example requires the base glyph `a` to have an anchor named `top`, and the accent glyph `acute` to have an anchor named `_top` (underscore prefix).
{: .note }

It’s also possible to assign an arbitrary unicode value to a glyph when creating it:

```text
aacute=a+acute@top|00E0
```

- - -

> The [Glyph Construction language] provides a more powerful syntax for building new glyphs from components. See {% internallink 'building-accented-glyphs-with-glyph-construction' %}.
{: .seealso }

[Glyph Construction language]: http://github.com/typemytype/GlyphConstruction#glyph-construction-language
