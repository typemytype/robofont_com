---
layout: page
title: Layers Popover
tags:
  - layers
  - UI
description: The features and applications of the Layers popover.
---

* Table of Contents
{:toc}

{% image reference/workspace/layers-popover.png %}

Actions
-------

The *layers popover* allows access to layer data. It is accessible from the {% internallink "/documentation/reference/workspace/font-overview" %} and the {% internallink "/documentation/reference/workspace/glyph-editor" %}.
Through the *layers popover*, users can:

<table>
  <tr>
    <th width="30%">action</th>
    <th width="70%">description</th>
  </tr>
  <tr>
    <td>drag and drop</td>
    <td>Change the layer order in the font.</td>
  </tr>
  <tr>
    <td>click and hold</td>
    <td>Rename a layer.</td>
  </tr>
  <tr>
    <td>right click</td>
    <td>Open a contextual menu with an option to make the current layer the default one.</td>
  </tr>
  <tr>
    <td>double click on color</td>
    <td>Open a color palette to choose the layer color.</td>
  </tr>
  <tr>
    <td>+ / -</td>
    <td>Add or remove a layer.</td>
  </tr>
  <tr>
    <td>display controls</td>
    <td>
      <p>Control how the layer is displayed in the Glyph View.</p>
      <ul>
        <li><em>if the selected layer is the active layer:</em><br/>
            display controls affect the display of the active layer.
        </li>
        <li><em>if the selected layer is not the active layer</em><br/>
            display controls affect the display of the layers in the background
        </li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>
      Global Layers Sync Settings
    </td>
    <td>
      Control the settings for glyph attributes (metrics, unicodes, mark color) syncing across layers
    </td>
  </tr>
</table>

> Layer names must contain ASCII characters only, and spaces, slashes and semicolons are not allowed.
{: .note }

> Users are restricted from setting another layer as the default if the font already contains a layer named `public.default`. RoboFont will not be able to save the UFO file.
{: .warning }


Global Layers Sync Settings
---------------------------

{% image reference/workspace/layers-popover-sync.png %}

The Global Layers Sync Settings popover allows to control which glyph attributes will be synced once edited. For example, consider the following layers structure for glyph `A`

```plaintext
A (foreground)         A (background)         A (sketches)      
  unicode: U+0041        unicode: None          unicode: None   
  color: red             color: blue            color: blue     
  width: 400             width: 450             width: 350      
```

if you edit the width of `A (sketches)` to `380` (with `metrics` as a synced attribute), it will result in:

```plaintext
synced attribute: metrics

BEFORE                  AFTER
A (foreground)      →   A (foreground)
  unicode: U+0041   →     unicode: U+0041
  color: red        →     color: red
  width: 400        →     width: 380
                    →
A (background)      →   A (background)
  unicode: None     →     unicode: U+0041
  color: blue       →     color: blue
  width: 450        →     width: 380
                    →
A (sketches)        →   A (sketches)
  unicode: None     →     unicode: U+0041
  color: violet     →     color: violet
  width: 350        →     width: 380

```

Be aware that template glyphs have a special behaviour, a so-called edge case. When a user jumps to a non-existing glyph in a layer, RoboFont creates a template glyph first. If any Global Layer Sync setting is on, the template glyph will inherit the synced attributes from the glyph in the default layer.

For example:
- Only the metrics are synced in Global Layers Sync Settings popover and foreground is the default layer
- User reaches S (foreground) in the glyph editor
- User jumps to S (background) in the glyph editor, but S (background) does not exist
- RoboFont will create a template glyph first (than a real glyph once editing starts to happen) and the template glyph will inherit the metrics from S (foreground) because it sits in the default layer


## Sync All Glyphs

{% image reference/workspace/syncAllModalDialog.png %}

The Sync All Glyphs button will align the selected attributes of each glyph belonging to a non-default layer, to the attributes of the glyph equally named belonging to the default layer (if existing). For example:

```plaintext
default layer: foreground
selected attributes: metrics, unicodes

BEFORE                  AFTER
A (foreground)      →   A (foreground)
  unicode: U+0041   →     unicode: U+0041
  color: red        →     color: red
  width: 400        →     width: 400
                    →
A (background)      →   A (background)
  unicode: None     →     unicode: U+0041
  color: blue       →     color: blue
  width: 450        →     width: 400

```



Display Options
---------------

Use the icon buttons to toggle different display attributes for layers:

<table>
  <tr>
    <th width="10%">icon</th>
    <th width="90%">layer attributes</th>
  </tr>
  <tr>
    <td><img src="../../../../images/reference/workspace/layer-display-options_visibility.png" /></td>
    <td>visibility</td>
  </tr>
  <tr>
    <td><img src="../../../../images/reference/workspace/layer-display-options_fill.png" /></td>
    <td>fill</td>
  </tr>
  <tr>
    <td><img src="../../../../images/reference/workspace/layer-display-options_stroke.png" /></td>
    <td>stroke</td>
  </tr>
  <tr>
    <td><img src="../../../../images/reference/workspace/layer-display-options_points.png" /></td>
    <td>points</td>
  </tr>
  <tr>
    <td><img src="../../../../images/reference/workspace/layer-display-options_positions.png" /></td>
    <td>point coordinates</td>
  </tr>
</table>

> - {% internallink "glyph-editor/layers" %}
{: .seealso }

Contextual Menu
---------------

Right-click on a layer to open a contextual menu with the option to make that layer the default one.

{% image reference/workspace/layers-popover-contextual-menu.png %}