---
layout: page
title: Output Window
tags:
  - scripting
  - UI
---

The *Output Window* catches all print statements and tracebacks of scripts which are not running in the {% internallink "scripting-window" %} – such as output from tools with dialogs or observers, RoboFont’s own warnings and error messages, etc.

{% image reference/workspace/output-window.png %}


Options
-------

<table>
  <tr>
    <th width="20%">option</th>
    <th width="80%">description</th>
  </tr>
  <tr>
    <td>Can hide</td>
    <td>
      <p>Enable this option to hide the window when RoboFont is not the active app (the Output Window acts like a floating window).</p>
      <p>When this option is disabled, the window will remain visible when RoboFont is not active.</p>
    </td>
  </tr>
  <tr>
    <td>Clear</td>
    <td>Clears all print statements and tracebacks from the Output Window.</td>
  </tr>
</table>

> - {% internallink "api/mojo/mojo-ui#mojo.UI.OutputWindow" text='mojo.UI.OutputWindow' %}
{: .seealso }
