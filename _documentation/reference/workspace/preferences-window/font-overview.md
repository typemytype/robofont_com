---
layout: page
title: Font Overview Preferences
treeTitle: Font Overview
tags:
  - character set
  - preferences
---

* Table of Contents
{:toc}

The *Font Overview* section contains settings related to the {% glossary character set %}s used to sort glyphs in the {% internallink "workspace/font-overview" %}, and the appearance of the glyph cells.

{% image reference/workspace/preferences-window/preferences_font-overview_character-set.png %}

## Character Sets

The *Character Set* tab shows a list of saved character sets (left), and a text area with a space separated list of glyph names in the current character set (right).

### Options

<table>
  <thead>
    <tr>
      <th width='40%'>option</th>
      <th width='60%'>description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Default charset new document</td>
      <td>The default character set for new fonts.</td>
    </tr>
    <tr>
      <td>+</td>
      <td>Create a new character set.</td>
    </tr>
    <tr>
      <td>-</td>
      <td>Delete the selected character set.</td>
    </tr>
    <tr>
      <td>Import from .enc file</td>
      <td>Import character set from an <code>.enc</code> file.</td>
    </tr>
    <tr>
      <td>Import from .ufo file</td>
      <td>Import character set from another <code>.ufo</code> font.</td>
    </tr>
    <tr>
      <td>Import from Current Font</td>
      <td>Import character set from the current font.</td>
    </tr>
    <tr>
      <td>Import from All Open Fonts</td>
      <td>Import the combined character set from all open fonts.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Deleting glyphs also removes glyph name from…</td>
      <td>Remove glyph names from kerning and groups when a glyph is deleted.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>New glyph width</td>
      <td>The default width for newly added glyphs.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>AGL/GNFUL path</td>
      <td>Optionally provide a <a href="http://github.com/adobe-type-tools/agl-aglfn">AGL</a> (Adobe Glyph List) or a <a href="http://github.com/LettError/glyphNameFormatter">GNFUL</a> (Glyph Name Formatted Unicode List) file to map glyph names to unicode values.</td>
    </tr>
  </tbody>
</table>

### Editing character sets

Use the +/- buttons to create or delete character sets.

To modify a character set, simply add, remove or modify glyph names from the list.

> - {% internallink "defining-character-sets" %}
{: .seealso }

## Appearance

The *Appearance* tab contains settings to choose light mode and dark mode colors and the template font used in the Font Overview’s glyph cells.

To edit a color, double click on a color cell to open a color picker.

{% image reference/workspace/preferences-window/preferences_font-overview_appearance.png %}

### Options

<table>
  <tr>
    <th width='40%'>option</th>
    <th width='60%'>description</th>
  </tr>
  <tr>
    <td>Template glyph preview font</td>
    <td>The font used to draw the template glyph cells. The font must be installed on your system.</td>
  </tr>
  <tr>
    <td>Template glyphs missing unicode character</td>
    <td>If a glyph does not have a unicode value, this character will be used as replacement.</td>
  </tr>
</table>
