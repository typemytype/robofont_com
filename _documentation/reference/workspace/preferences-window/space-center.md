---
layout: page
title: Space Center Preferences
treeTitle: Space Center
tags:
  - spacing
  - preferences
---

* Table of Contents
{:toc}

The *Space Center* section contains settings related to the {% internallink "workspace/space-center" %}.


Appearance
----------

The *Appearance* tab allows you to customize light mode and dark mode colors used in the Space Center.

{% image reference/workspace/preferences-window/preferences_space-center_appearance.png %}

To edit a color, double-click on a color cell to open a color picker.

### Options

<table>
  <tr>
    <th width='45%'>option</th>
    <th width='55%'>description</th>
  </tr>
  <tr>
    <td>Flexible input cell width</td>
    <td>Choose between flexible or fixed width columns in the Space Matrix.</td>
  </tr>
  <tr>
    <td>Show .notdef for non-existing glyphs</td>
    <td>Show a <code>.notdef</code> glyph for input glyphs which are not included in the font.</td>
  </tr>
  <tr>
    <td>Maximum glyph count for live updating</td>
    <td>Threshold value for live updating glyph previews in the Space Center.</td>
  </tr>
  <tr>
    <td>Keep input history</td>
    <td>Keep a list of previously used text input strings. Set the number of steps to remember.</td>
  </tr>
</table>


Input text
----------

The *Input text* tab contains a list of default text strings for use in the Space Center.

{% image reference/workspace/preferences-window/preferences_space-center_input-text.png %}

> Input strings may contain special syntax such as `/?` (current glyph) and `/!` (selected glyphs) – see {% internallink "space-center#text-input-field" text="Space Center > Text Input Field" %}.
{: .note }


Hot keys
--------

The *Hot keys* tab allows you to add and modify short keys for various Space Center functions.

{% image reference/workspace/preferences-window/preferences_space-center_hotkeys.png %}
