---
layout: page
title: Appearance (Dark Mode)
---

* Table of Contents
{:toc}

RoboFont supports dynamic appearance starting from RF4.4. This page offers some tips to improve your experience and the experience of your users.

### Preferences Window

By default RoboFont follows the system settings and updates the appearance instantly without the need to reboot the application. User can override the default behaviour in the {% internallink "reference/workspace/preferences-window/miscellaneous" text="Miscellaneous" %} section of the Preferences Window.

Every section of the {% internallink "reference/workspace/preferences-window" %} where color can be defined, provides two separate column for light and dark mode.

> - {% internallink "reference/workspace/preferences-window/glyph-view#appearance" %}
> - {% internallink "reference/workspace/preferences-window/space-center#appearance" %}
> - {% internallink "reference/workspace/preferences-window/font-overview#appearance" %}
> - {% internallink "reference/workspace/preferences-window/python" %}
{: .seealso}

### Preferences Editor

Every color key in the {% internallink "reference/workspace/preferences-editor" %} has a `.dark` variant for dark mode. Syntax highlighting colors are grouped under `PyDETokenColors` for light mode and `PyDETokenColors.dark` for dark mode.

### Scripting

`mojo.UI` provides the `appearanceColorKey()` function to verify which color key is at the moment in charge of a specific color. For example, if the application is in dark mode, `appearanceColorKey("PyDEBackgroundColor")` will return `"PyDEBackgroundColor.dark"`

`mojo.UI` provides the `inDarkMode()` function to check in which mode the application is. The function returns a boolean.

`mojo.subscriber` provides a `appearanceChanged()` callback that allows tools to react instantly to changes to the appearance settings.

```python
from mojo.subscriber import Subscriber, registerRoboFontSubscriber
from mojo.UI import inDarkMode

class Appearence(Subscriber):

    def roboFontAppearanceChanged(self):
        answer = 'yes' if inDarkMode() else 'no'
        print(f"Are we in dark mode? {answer}")


if __name__ == '__main__':
    registerRoboFontSubscriber(Appearence)
```
