---
layout: page
title: Upgrading code to RoboFont 3
treeTitle: Upgrading to RF3
tags:
  - extensions
draft: false
---

* Table of Contents
{:toc}

**Developers are kindly advised to upgrade their code for RoboFont 3.**

In practice, this means:

upgrading from the RoboFab API to the FontParts API
: see {% internallink "topics/robofab-fontparts" %}

upgrading from Python 2 to Python 3 syntax
: see [Writing Python 2-3 compatible code](http://python-future.org/compatible_idioms.html)


Running RF 1 code in RF 3
-------------------------

### RoboFab → FontParts

*Using the RoboFab API in RoboFont 3.*

What happens?
: A deprecation warning will be raised, with an example of the new syntax.

  > Warnings can be configured with the Preference setting `warningsLevel`. See {% internallink "workspace/preferences-editor" %}.
  {: .tip }

Does the script work?
: <span class='yes'>YES.</span>

What to do?
: Please update your code to the FontParts API.

Example
: ^
  ```python
  g = CurrentGlyph()
  g.move((100, 100))
  g.update()
  ```

  ```plaintext
  /Applications/RoboFontPy3.app/Contents/Resources/lib/python3.6/fontParts/base/deprecated.py:45: DeprecationWarning: 'RGlyph.move()': use RGlyph.moveBy()
  /Applications/RoboFontPy3.app/Contents/Resources/lib/python3.6/fontParts/base/deprecated.py:28: DeprecationWarning: 'RGlyph.update': use RGlyph.changed()
  ```

  Change to:

  ```python
  g = CurrentGlyph()
  g.moveBy((100, 100))
  g.changed()
  ```

### Python 2 → Python 3

*Using Python 2 syntax in RoboFont 3.*

What happens?
: A `SyntaxError` is raised.

Does the script work?
: <span class='no'>NO.</span>

What to do?
: Please update your code to Python 3.

Example
: ^
  ```python
  print 'hello world'
  ```

  ```plaintext
  Traceback (most recent call last):
    File "<untitled>", line 1
      print 'hello world'
                        ^
  SyntaxError: Missing parentheses in call to 'print'. Did you mean print('hello world')?
  ```

  Change to:

  ```python
  print('hello world')
  ```


Running RF 3 code in RF 1
-------------------------

### RoboFab ← FontParts

*Using the FontParts API in RoboFont 1.8.*

What happens?
: ^
  An error message is raised.

  If the [oneToThree extension] is installed, RoboFont will try to map the newer FontParts API to the older RoboFab API automatically. However, not all possible differences between the RoboFab and FontParts APIs are covered.

Does the script work?
: <span class='maybe'>MAYBE.</span>

Example
: ^
  ```python
  g = CurrentGlyph()
  g.moveBy((100, 100))
  g.changed()
  ```

  Without oneToThree:

  ```plaintext
  Traceback (most recent call last):
    File "<untitled>", line 2, in <module>
  AttributeError: 'RobofabWrapperGlyph' object has no attribute 'moveBy'
  ```

  With oneToThree installed, this particular code snippet would work fine in RoboFont 1.

  Here’s an example which oneToThree is not able to handle:

  ```python
  g = CurrentGlyph()
  g.rotateBy(30, origin=(100, 100))
  ```

  ```plaintext
  Traceback (most recent call last):
    File "<untitled>", line 2, in <module>
  TypeError: rotate() got an unexpected keyword argument 'origin'
  ```

What to do?
: ^
  As a first step, give the oneToThree extension a try.

  If your code includes syntax which is not covered by oneToThree, and you need to have your code working in both versions of RoboFont at the same time, you’ll need to use conditionals as a last resort. Simply ask RoboFont for its version, and run different bits of code for RF1 and RF3.

    ```python
  from mojo.roboFont import version

  # RF3
  if version >= "3.0.0":
      g.rotateBy(30, origin=(100, 100))

  # RF1
  else:
      g.rotateBy(30, offset=(100, 100))
  ```

### Python 2 ← Python 3

*Using Python 3 syntax in RoboFont 1.*

What happens?
: ^
  RoboFont 1 supports some of the newer Python 3 syntax automatically, thanks to the `__future__` module. However, not all possible differences between the Python 2 and Python 3 are supported.

Does the script work?
: <span class='maybe'>PROBABLY.</span>

Example
: ^
```python
s = chr(257)
```
: ^
```plaintext
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: chr() arg not in range(256)
```

What happens?
: ^
Python2 does not have integrated unicode support as Python3. Python2 strings only support range of 256 characters. In order to benefit from unicode functionalities, Python2 requires the use of the `unicode()` object

What to do?
: ^
  Make sure your Python 3 code is compatible with Python 2.

  See [Writing Python 2-3 compatible code](http://python-future.org/compatible_idioms.html) for reference.

[oneToThree extension]: http://github.com/typemytype/oneToThreeRoboFontExtension
