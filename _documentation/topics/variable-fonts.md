---
layout: page
title: Variable fonts
tags:
  - interpolation
  - designspace
level: intermediary
---

* Table of Contents
{:toc}


Variable fonts
--------------

*Variable fonts* are a special kind of OpenType font which can change their shape dynamically by parametrically ‘morphing’ between two or more *sources* contained in the font.

OpenType variable font technology is built on top of the TrueType GX model, expanding it to add support for cubic outlines (CFF2 table) and fully integrating it into other aspects of the OpenType format. The core technology was developed jointly by Microsoft, Google, Apple and Adobe, and released in September 2016.

> - [Introducing OpenType Variable Fonts](http://medium.com/variable-fonts/https-medium-com-tiro-introducing-opentype-variable-fonts-12ba6cd2369)
> - [What are OpenType variable fonts?](http://docs.microsoft.com/en-us/windows/desktop/DirectWrite/opentype-variable-fonts#what-are-opentype-variable-fonts)
{: .seealso }

### How variable fonts work

Variable fonts have a default or *neutral* source, and all the other sources are described as movements or *deltas* in relation to the neutral. Deltas from all sources add up to produce a glyph shape at a given location in the designspace.

This model, based on TrueType GX, is fundamentally different from the Multiple Master model, where individual sources are placed at the corners of the designspace, without a center or neutral.

UFO-based workflows to create variable fonts still use separate sources though. The calculation of deltas is handled by [fontTools.varLib] on font generation.

[fontTools.varLib]: https://fonttools.readthedocs.io/en/latest/varLib/

> - [Samsa, a web app to visualize how variable fonts work](http://www.axis-praxis.org/samsa/)
{: .seealso }

### Support for variable fonts

Variable fonts are supported by all major browsers. Variable font parameters can be controlled using the `font-variation-settings` CSS4 property.

> - [Variable fonts guide (MDN)](http://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Fonts/Variable_Fonts_Guide)
> - [Can I use variable fonts?](http://www.caniuse.com/#search=font-variation-settings)
> - [font-variation-settings (CSS4)](http://drafts.csswg.org/css-fonts-4/#font-variation-settings-def)
{: .seealso }

TrueType-flavored variable fonts are supported natively in macOS, and can be used programatically in [DrawBot].

Variable fonts are also supported in the main [Adobe Creative Cloud] apps (Photoshop, Illustrator, InDesign) and in [Sketch], and are coming soon to [Figma].

> - [Variable Fonts Are the Future of Web Type (Adobe)](https://create.adobe.com/2018/5/22/variable_fonts_are_t.html)
> - [Adobe InDesign 2020](https://theblog.adobe.com/adobe-indesign-2020/)
> - [Variable Fonts, improved OpenType support, … (Sketch)](https://blog.sketchapp.com/variable-fonts-improved-opentype-support-and-a-new-data-plugin-whats-new-in-sketch-e16f81bf8b75)
> - [Variable Font Support (Figma)](https://github.com/figma/community/issues/2)
{: .seealso }

[Adobe Creative Cloud]: http://www.adobe.com/creativecloud
[DrawBot]: http://drawbot.com/
[Sketch]: http://sketch.com/
[Figma]: http://www.figma.com/


Variable fonts vs. interpolation tools
--------------------------------------

While OpenType variable fonts are a fairly recent technology, interpolation is a well-known and widely used method for creating design variations .

> The *idea* of a typeface as a continuous variation space goes back further into pre-digital days.
>
> - [Serial type families](http://typeculture.com/academic-resource/articles-essays/serial-type-families/)
{: .seealso }

Below are some of the interpolation tools and technologies that have been available to type designers since before the advent of variable fonts.

### Multiple Master

Multiple Master was font format introduced by Adobe in the 1990s, and supported in several Adobe apps such as Illustrator, Photoshop and the Adobe Type Manager (ATM).

The interpolation model used by the Multiple Master format has some differences and limitations in relation to the TrueType GX model. For example, it requires sources to be placed at the extremes of the variation axes, without a neutral design; it also uses cubic bezier curves instead of quadratic.

Support for Multiple Master fonts was discontinued in XXXX, but the technology survived as an interpolation engine inside font editors (FontLab, Glyphs?).

> [Multiple Master – Wikipedia](https://en.wikipedia.org/wiki/Multiple_master_fonts)
{: .seealso }

### RoboFab interpolation & GlyphMath

Basic interpolation with Python was implemented in RoboFab (later FontParts), and is available in RoboFont since day one. There are many [example scripts] available, and several [extensions] which use interpolation to preview and create instances.

> - {% internallink "topics/interpolation" %}
{: .seealso }

[example scripts]: #
[extensions]: #

*GlyphMath* is a feature of the RoboFab/FontParts API which makes it possible to perform the four basic math operations – addition, subtraction, multiplication, division – on glyph objects. This can be used to calculate deltas and create complex interpolation systems.

> - {% internallink 'topics/glyphmath' %}
> - {% internallink 'tutorials/using-glyphmath' %}
{: .seealso }

### Superpolator & MutatorMath

[Superpolator] is a standalone application which (re)opened the door to *superimposed interpolation*, making it possible to create complex interpolation systems with an arbitrary amount of sources. Its underlying engine, [MutatorMath], was released under an open-source license in 2014 with support from Adobe. Both Superpolator and MutatorMath can be used to create interpolation design spaces.

> - [MutatorMath (archived)](https://web.archive.org/web/20181005042443/http://letterror.com/2014/09/19/mutatormath/)
{: .seealso }

### Skateboard

[Skateboard] is the next generation of Superpolator. Instead of a separate standalone app, it is tighly integrated into RoboFont as an extension. It is also designed from the ground up to support variable fonts.

Skateboard supports two different interpolation engines: `mutatorMath` and `varLib`. There are some differences between them:

mutatorMath
: The `mutatorMath` engine is a design tool which works with UFO sources. It is not very strict about the compatibility between sources: it requires only that contour order, contour direction and number of on-curve points match. It also supports [extrapolation](#), which is a very useful feature for exploring variations during the design stage.

varLib
: ^
  The `fontTools.varLib` engine is a production tool to compile binary sources into a variable font. It is much stricter in relation to the data it is fed – all sources must have the same glyphs, kerning pairs, groups, off-curve points, etc. And it does not support extrapolation, only interpolation.

  The [Batch extension](#) can solve most of these issues by inserting *repair mutators*.

> - [Skateboard and Superpolator: Differences](http://superpolator.com/pages/differences.html)
{: .seealso }

[Superpolator]: http://superpolator.com/pages/superpolator.html
[Skateboard]: http://superpolator.com/pages/skateboard
[MutatorMath]: http://github.com/LettError/MutatorMath


Examples of variable fonts
--------------------------

Have a look at some examples of variable fonts to get an idea of what’s possible to do with the format.

> - [Axis Praxis](http://axis-praxis.org/)
> - [Variable Fonts](http://v-fonts.com/)
{: .seealso }

How to create and test variable fonts
-------------------------------------

The RoboFont documentation contains several articles and tutorials related to variable fonts:

> - {% internallink "/tutorials/creating-variable-fonts" %}
> - {% internallink "/tutorials/testing-variable-fonts" %}
> - [The designSpace format](https://github.com/fonttools/fonttools/tree/main/Doc/source/designspaceLib)
> - {% internallink "/tutorials/creating-designspace-files" %}
> - {% internallink "/how-tos/creating-glyph-substitution-rules" %}
{: .seealso}