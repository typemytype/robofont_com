---
layout: page
title: The pre-history of RoboFont
treeTitle: Pre-History
---

{::options toc_levels='2' /}

* Table of Contents
{:toc}


RoboFog
-------

The idea of a programmable font editor goes back to RoboFog, a Python-powered version of {% glossary Fontographer %} produced by Petr van Blokland in the early 1990s.

{% image topics/Fontographer.png caption='Fontographer' %}

Petr had worked on the development of {% glossary Mac Ikarus %}, and had also attempted to create his own font editor – that proved to be a very difficult task. He realized that, instead of writing his own font editor from scratch, he could start from an existing one and make it programmable.

Thanks to work contacts at the {% glossary Font Bureau %}, Petr gained permission to use the old Fontographer codebase as a starting point. With Just van Rossum’s help, he managed to compile Fontographer together with a Python interpreter, and built an API so that the program became scriptable – RoboFog was born.

{% image topics/RoboFog.png caption='RoboFog splash screen' %}

RoboFog was very successful within its niche market. It included a small toolkit for creating custom UIs in pure Python. Users had a lot of fun with its features, and used it to build tools which were very useful for their workflows.

Altsys was acquired by Macromedia in 1995, and Fontographer development slowed down after version 4.1. Macromedia was later bought by Adobe. There were important font technology developments around that time, for example Unicode and the new {% glossary OpenType %} font technology. Fontographer quickly became obsolete, and its user base gradually moved to FontLab. 

<!-- The Fontographer codebase was licensed by FontLab in 2005, and the application was updated to support modern operating systems and font technologies. It was re-released as [Fontographer 5]. -->

[Fontographer]: http://en.wikipedia.org/wiki/Fontographer
[Fontographer 5]: http://www.fontlab.com/font-editor/fontographer


DrawBot
-------

DrawBot was originally developed by {% glossary Just van Rossum %} as an educational tool for teaching programming to designers, and has grown into a full-fledged platform for automating the creation of all kinds of graphic material.

> - {% internallink "topics/drawbot" %}
{: .seealso}

RoboFab
-------

FontLab was created in the early 1990s by Russian programmer Yuri Yarmola and distributed by the US-based company Pyrus. By the end of the decade, many type designers had moved from Fontographer to FontLab. Yuri was looking for a scripting language for FontLab. After meeting with Just van Rossum in 1997 and seeing a demonstration of RoboFog, he integrated a Python interpreter and added a low-level Python API to FontLab 4.

On top of that basic API, Just van Rossum, Erik van Blokland and Tal Leming developed [RoboFab], a {% glossary pythonic %} API to FontLab’s native objects. RoboFab was heavily inspired by RoboFog and their APIs were very similar. A simple toolkit for creating UIs in Python, {% glossary dialogKit %}, was also created. All together, this allowed designers to port their old RoboFog scripts to RoboFab.

{% image topics/RoboFab-object-map.png caption='RoboFab object map' %}

RoboFab also made it possible to write scripts to modify fonts outside of any font editor, using only Python, RoboFab and UFO – for example in {% internallink "tutorials/python/terminal" text='Terminal' %} or in a code editor. This ‘non-environment’ became known as *NoneLab*. There were many limitations though, as several specific tasks – overlap removal, font generation, hinting, etc. – still required a font editor.

The RoboFab package was distributed freely under an open-source license and worked in both Windows and Mac versions of FontLab. It had a pretty website with very complete documentation and a colourful font object map. RoboFab became popular among font makers and helped them create useful tools to get work done. The {% glossary RoboThon %} conferences were key to making RoboFab known to a larger group of type designers and font production people.

[RoboFab]: http://robofab.org/

FontTools / TTX
---------------

Another important milestone in the Python font world was Just van Rossum’s [FontTools/TTX] package, released in 1999 as an open-source project. FontTools is a library to manipulate binary fonts in Python, and TTX is a command-line tool to decompile OpenType fonts into XML and back.

{% image topics/FontToolsIconGreenCircle.png %}

FontTools proved to be extremely useful for mastering and debugging OpenType fonts. Development started to slow down around 2004. In 20XX, while working for Google, Behdad Esfahbod and contributors picked up development, added support for more OpenType tables, fixed many bugs, and added new tools to subset and merge fonts. The project also got a new logo and a much needed [documentation][FontTools docs] in 2020.

> - [FontTools/TTX — The Power of Python for OpenType](http://www.typotalks.com/videos/font-tools-ttx-the-power-of-python-for-opentype)
{: .seealso }

[FontTools/TTX]: http://github.com/fonttools/fonttools
[FontTools docs]: http://fonttools.readthedocs.io/


UFO
---

### Origins

What would later become UFO started in the mid 2000s when Just and Erik were collaborating on a font project while living in different cities, and needed a way to share glyph data during the project. They were using a version control system for their code, so it was natural to use it for the font data too. They came up with a text-based format to describe glyph data, one glyph per file, structured as XML. This format proved to be very useful, and they teamed up with Tal Leming to develop it further, adding support for meta info and metrics data, and improving the glyph format. A proper format specification was also made, and UFO 1 was published in 2004.

{% image topics/ufo-icon-file.png %}

### UFO-based workflows

Tal Leming’s [UFO Central] tool made it possible to import and export UFOs to/from FontLab. What started as a version control format gradually developed into a *data exchange format* – a way to transfer data back and forth between FontLab and UFO-based font applications like Metrics Machine (more about it below).

Over the years, UFO has also proved to be a good *long-term storage format* for font sources. Using a human-readable XML format ensures that data is accessible even after editing applications stop working.

### UFO updates

As more and more designers and tools started to use UFO, and with several new developments to the OpenType format, it became necessary to update and improve the format. [UFO2] was published in 2009 at the RoboThon conference, and Tal Leming was officially appointed editor of the UFO specification. The process continued with [UFO3], published 3 years later at RoboThon 2012, and goes on with [UFO4] \(currently under development).

[UFO Central]: http://github.com/typesupply/fontlab-scripts/blob/master/UFOCentral.py
[UFO2]: https://unifiedfontobject.org/versions/ufo2/
[UFO3]: https://unifiedfontobject.org/versions/ufo3/
[UFO4]: https://unifiedfontobject.org/versions/ufo4/

Open-source libraries
---------------------

With FontLab + RoboFab established as the central environment for drawing and generating fonts, in the mid 2000s the focus shifted towards developing specialized libraries and stand-alone applications. 

Independently or in collaboration, Just, Erik and Tal Leming developed a set of open-source code libraries for building font applications, establishing the foundations on which RoboFont stands until today.

### vanilla

The first of these libraries was [vanilla], a toolkit for creating native macOS interfaces in pure Python, inspired by the classic W library from RoboFog days. Vanilla provides Python wrappers for all the main macOS UI elements such as windows of different kinds, buttons, sliders, text input fields, list views, tabs, and lot more.

Vanilla made it possible to build native macOS applications entirely in Python, bypassing the need to use Apple’s [Interface Builder] or [Objective-C] directly.

[vanilla]: http://github.com/robotools/vanilla
[Interface Builder]: http://en.wikipedia.org/wiki/Interface_Builder
[Objective-C]: http://en.wikipedia.org/wiki/Objective-C

### defcon

Then came [defcon], a set of lightweight, fast, flexible UFO-based objects for use in font editing applications. Different from RoboFab, which was originally created around the scripting API of a particular application (FontLab), defcon was written from scratch with the purpose of building apps – its objects include built-in functionality such as observing notifications and caching data.

[defcon]: http://github.com/robotools/defcon

### defconAppKit

Along with defcon came [defconAppKit], a separate library containing reusable UI elements made specifically for font editing applications – things like a glyph cell, a font info sheet, a text preview area, etc. 

[defconAppKit]: http://github.com/robotools/defconAppKit

### mutatorMath

Based on Apple’s TrueType GX variable font technology, Erik van Blokland created [MutatorMath], a Python library for building complex interpolation systems, and the engine behind his Superpolator app (see [below](#superpolator)). The MutatorMath code was open-sourced in 2014 thanks to support and funding from Adobe.

[MutatorMath]: http://github.com/LettError/MutatorMath

### ufo2fdk

Another important milestone came in the form of [ufo2fdk], a Python bridge between UFO and Adobe’s {% glossary AFDKO %} written by Tal. <!-- This became possible after Adobe made the AFDKO available under an open-source license in 2004(?). --> ufo2fdk finally made it possible to generate OpenType fonts directly from UFOs in Nonelab<!-- (that is, without having to use FontLab) -->.

[ufo2fdk]: http://github.com/robotools/ufo2fdk

### Other libraries

Other libraries created in this period include [fontMath] to perform fast font math, [extractor] to convert binary fonts to UFO, [compositor] to set text with OpenType layout features, [aicbTools] to import vector data from Adobe Illustrator, etc.

> - {% internallink 'reference/embedded-libraries' %}
{: .seealso }

[fontMath]: http://github.com/robotools/fontMath
[extractor]: http://github.com/robotools/extractor
[compositor]: http://github.com/robotools/compositor
[aicbTools]: http://github.com/typesupply/aicbTools


Standalone apps
---------------

On top of these open-source libraries and using the UFO format for data interchange, several stand-alone font editing applications were built.

### Area 51

{% image topics/Area51.png %}

[Area 51] was a small application to inspect UFO fonts and test OpenType features. It started out as a small vanilla and defconAppKit demo, but ended up becoming something useful on its own. The app was later converted into the [Feature Preview] extension.

[Area 51]: http://web.archive.org/web/20130102061607/http://tools.typesupply.com/area51.html
[Feature Preview]: http://github.com/typemytype/featurePreviewRoboFontExtension

### Metrics Machine

{% image topics/MetricsMachine.png %}

[Metrics Machine] was created to make the kerning process suck less, and introduced a new, more efficient interface for kerning. It could write kerning into the UFO or export it as OpenType feature code. The app has recently been converted into an [extension][MetricsMachine extension] for RoboFont 3.

[Metrics Machine]: http://web.archive.org/web/20130102183731/http://tools.typesupply.com/metricsmachine.html
[MetricsMachine extension]: #

### Superpolator

{% image topics/superpolator.jpg %}

[Superpolator], released by Erik van Blokland in 2004, was created to make multidimensional font interpolation available for type designers. It allowed them to build large, complex typeface families using an arbitrary number of axes and sources, in a way that was much more flexible than the Multiple Master based interpolation available in FontLab. The underlying math model was released as the open-source library `mutatorMath` (see [above](#mutatorMath)).

With the introduction of OpenType variable fonts in 2016, Superpolator evolved into a separate tool, [Skateboard], which is an extension for RoboFont 3.

[Superpolator]: http://superpolator.com/pages/superpolator.html
[Skateboard]: http://superpolator.com/pages/skateboard.html

### Prepolator

{% image topics/Prepolator.png %}

[Prepolator] was created to make source fonts compatible for interpolation, making the tedious task of checking contour compatibility visual and quick. It was typically used next to Superpolator. With the release of RoboFont 3, Prepolator was converted into a [RoboFont extension][Prepolator extension].

[Prepolator]: http://web.archive.org/web/20130102184341/http://tools.typesupply.com/prepolator.html
[Prepolator extension]: #

### Kalliculator

<!-- {% image topics/kalliculator-app.png %} -->

Kalliculator was Frederik Berlaen’s first application, developed in 2005 as his graduation project at {% glossary TypeMedia %}. Kalliculator is an application to model calligraphic contrast around skeletons, with various parameters such pen width and height, rotation, and pressure. Kalliculator was never released commercially, but the techniques learned to build it were used to create other apps later, including RoboFont.

[Kalliculator]: http://kalliculator.com/

### UFOstretch

{% image topics/UFOstretch.png %}

[UFOstretch] is an application to interpolate and transform (scale, rotate, skew, translate) glyphs. It is useful for creating variations like small caps, superscript, condensed, oblique, etc.

[UFOstretch]: http://ufostretch.typemytype.com/

### roundingUFO

{% image topics/roundingUFO.png %}

[roundingUFO] is an application to apply contour effects to the corners of glyphs. It can be used to round off corners, but also to create ink traps or light traps automatically.

[roundingUFO]: http://roundingufo.typemytype.com/

### FontConstructor

{% image topics/FontConstructor.png %}

[FontConstructor] is an application to build glyphs intuitively out of modular parts. It was built as an educational tool to explore the relationships between shapes in a typeface.

[FontConstructor]: http://fontconstructor.com/


- - -

> - [Python in Typeface and Font Development – An interview with Just van Rossum](http://talkpython.fm/episodes/show/47/python-in-typeface-and-font-development)
> - [FontParts documentation > History](http://fontparts.robotools.dev/)
> - [Font Editor Tool History (notes by Adam Twardoch)](http://designwithfontforge.com/en-US/Font_Editor_History.html)
{: .seealso }
