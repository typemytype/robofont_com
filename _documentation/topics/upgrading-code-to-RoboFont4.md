---
layout: page
title: Upgrading code to RoboFont 4
tags:
  - scripting
  - mojo
  - merz
  - subscriber
---

* Table of Contents
{:toc}

**Robofont 4.0 is packed with new APIs and upgrades!** This article is the starting point for upgrading your tools and make the most out of this update. Each section points to several other resources inside or outside the RoboFont documentation.

> 4.x versions will keep supporting code from RoboFont 3.x with some limitations. So, the good news is that the new release won't break code but, some upgrades will significantly benefit the user experience.
{: .note }

## Merz

RoboFont 4.0 uses a completely new drawing back-end called Merz. It is based on Apple's Core Animation framework. It is faster than `mojo.drawingTools` and it is structured with an object-oriented approach, so it will work great with your tools. `mojo.drawingTools` became significantly slower since the BigSur update, so RoboFont will impose a resolution limit to `mojo.drawingTools` to avoid annoying rendering delays. If your tool draws anything on the screen, you should definitely start from here.

> + {% internallink "topics/merz" text="Introduction to Merz" %}
> + [Merz Playground Extension](https://github.com/typesupply/merzplayground)
> + [Merz documentation](https://typesupply.github.io/merz/)
> + [Apple's Core Animation](https://developer.apple.com/documentation/quartzcore?language=objc)
{: .seealso }

## Subscriber

RoboFont 4.0 proposes a new approach to observing events inside the application. The Subscriber module, built on top of `mojo.events`, will make it very easy to subscribe and – especially – unsubscribe to notifications. No phantom notifications lying around in the application anymore!

> + {% internallink "topics/subscriber" text="Introdution to Subscriber" %}
> + {% internallink "reference/api/mojo/mojo-subscriber" text="Subscriber reference" %}
> + {% internallink "topics/the-observer-pattern" text="The Observer Pattern" %}
{: .seealso }

## WindowController

The Subscriber module introduces a new base `WindowController` object which replaces DefconAppKit's `BaseWindow` class. It supports multiple inheritance in combination with Subscriber.

> + {% internallink "topics/window-controller" text="Introduction to WindowController" %}
> + {% internallink "reference/api/mojo/mojo-subscriber#WindowController" text="WindowController reference" %}
{: .seealso }

## ezui

RoboFont 4 embed [ezui](https://typesupply.github.io/ezui/overview.html) by [Tal Leming](https://typesupply.com)! A new layer built on top of vanilla that makes the most out of [Cocoa AutoLayout API](https://developer.apple.com/library/archive/documentation/UserExperience/Conceptual/AutolayoutPG/index.html). Using Tal's words:

> Ezui is a toolkit that makes it easy (EZ) to build user interfaces (UI). It’s built on top of and is fully interoperable with vanilla. If vanilla is “all the Lego pieces” then ezui is “a curated collection of Lego pieces that work together to build a specific class of things.”

> + [ezui reference](https://typesupply.github.io/ezui/)
> + [vanilla reference](https://vanilla.robotools.dev)
{: .seealso }

## RoboFont 3 vs. RoboFont 4

In the documentation you can find a comparison table with tools from RoboFont 3 with updated versions to RoboFont 4.

> {% internallink "how-tos/updating-scripts-from-RF3-to-RF4" %}
{: .seealso}

## Embedded packages

Several embedded packages have been updated. Here is the complete list:

+ fontTools: 4.24.4
+ fontmake: 2.4.0
+ booleanOperations: 0.9.0
+ fontMath: 0.6.1
+ fontPens: 0.2.5
