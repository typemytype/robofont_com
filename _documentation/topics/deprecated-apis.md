---
layout: page
title: Deprecated APIs
tags: 
  - FontParts
  - RoboFab
  - vanilla
  - mojo
---

* Table of Contents
{:toc}

This page collects all the deprecated APIs and their suggested replacement.


Robofab vs FontParts
--------------------

A ton of stuff changed while moving from `robofab` to `fontParts`. We have an entire page dedicated to that: {% internallink "/topics/robofab-fontparts" %}


DialogKit
---------

<table>
  <tr>
    <th>Deprecated</th>
    <th>Replacement</th>
  </tr>
  <tr>
    <td><code>dialogKit.Button</code></td>
    <td><code><a href="https://vanilla.robotools.dev/en/latest/objects/Button.html">vanilla.Button</a></code></td>
  </tr>
  <tr>
    <td><code>dialogKit.PopUpButton</code></td>
    <td><code><a href="https://vanilla.robotools.dev/en/latest/objects/PopUpButton.html">vanilla.PopUpButton</a></code></td>
  </tr>
  <tr>
    <td><code>dialogKit.List</code></td>
    <td><code><a href="https://vanilla.robotools.dev/en/latest/objects/List2.html">vanilla.List</a></code></td>
  </tr>
  <tr>
    <td><code>dialogKit.EditText</code></td>
    <td><code><a href="https://vanilla.robotools.dev/en/latest/objects/EditText.html">vanilla.EditText</a></code></td>
  </tr>
  <tr>
    <td><code>dialogKit.TextBox</code></td>
    <td><code><a href="https://vanilla.robotools.dev/en/latest/objects/TextBox.html">vanilla.TextBox</a></code></td>
  </tr>
  <tr>
    <td><code>dialogKit.CheckBox</code></td>
    <td><code><a href="https://vanilla.robotools.dev/en/latest/objects/CheckBox.html">vanilla.CheckBox</a></code></td>
  </tr>
  <tr>
    <td><code>dialogKit.HorizontalLine</code></td>
    <td><code><a href="https://vanilla.robotools.dev/en/latest/objects/HorizontalLine.html">vanilla.HorizontalLine</a></code></td>
  </tr>
  <tr>
    <td><code>dialogKit.VerticalLine</code></td>
    <td><code><a href="https://vanilla.robotools.dev/en/latest/objects/VerticalLine.html">vanilla.VerticalLine</a></code></td>
  </tr>
  <tr>
    <td><code>dialogKit.GlyphLineView</code></td>
    <td><code><a href="../../reference/api/mojo/mojo-ui/#mojo.UI.MultiLineView">mojo.UI.MultiLineView</a></code></td>
  </tr>
  <tr>
    <td><code>dialogKit.GlyphView</code></td>
    <td><code><a href="../../reference/api/mojo/mojo-glyphpreview/">mojo.glyphPreview</a></code></td>
  </tr>
</table>


RoboFont Misc
-------------

<table>
  <tr>
    <th>Deprecated</th>
    <th>Replacement</th>
  </tr>
  <tr>
    <td><code>GlyphViewDisplaySettings</code></td>
    <td><code><a href="../../reference/api/mojo/mojo-ui/#mojo.UI.setGlyphViewDisplaySettings">setGlyphViewDisplaySettings</a></code></td>
  </tr>
  <tr>
    <td><code>*showUI</code></td>
    <td><code>*showInterface</code></td>
  </tr>
  <tr>
    <td><code>mojo.events.BezierDrawingTool</code></td>
    <td><code><a href="../../reference/api/mojo/mojo-events/#mojo.events.DrawingTool">mojo.events.DrawingTool</a></code></td>
  </tr>
  <tr>
    <td><code>buildAdditionContectualMenuItems</code></td>
    <td><code><a href="../../reference/api/mojo/mojo-ui/#mojo.UI.buildAdditionContextualMenuItems">buildAdditionContextualMenuItems</a></code></td>
  </tr>
  <tr>
    <td><code>DecomposePointPen</code></td>
    <td><code><a href="../../documentation/reference/api/mojo/mojo-pens/#mojo.pens.DecomposePointPen">from mojo.pens import DecomposePointPen</a></code></td>
  </tr>
  <tr>
    <td><code>mojo.drawingTools.dashLine</code></td>
    <td><code><a href="../../reference/api/mojo/mojo-drawingtools/#mojo.drawingTools.lineDash">mojo.drawingTools.lineDash</a></code></td>
  </tr>
  <tr>
    <td><code>EditIntStepper</code></td>
    <td><code><a href="../../reference/api/mojo/mojo-ui/#mojo.UI.EditStepper">EditStepper</a></code></td>
  </tr>
  <tr>
    <td><code>SliderEditIntStepper</code></td>
    <td><code><a href="../../reference/api/mojo/mojo-ui/#mojo.UI.SliderEditStepper">SliderEditStepper</a></code></td>
  </tr>
  <tr>
    <td><code>AllFonts(*sortOptions)</code></td>
    <td><code>AllFonts(sortOptions=['familyName', 'styleName'])</code></td>
  </tr>
</table>


Smart Sets
----------

<table>
  <tr>
    <th>Deprecated</th>
    <th>Replacement</th>
  </tr>
  <tr>
    <td><code>SmartSet</code></td>
    <td><code><a href="../../reference/api/mojo/mojo-smartset/#mojo.smartSet.SmartSet">mojo.SmartSet.SmartSet</a></code></td>
  </tr>
  <tr>
    <td><code>addSmartSet</code></td>
    <td><code><a href="../../reference/api/mojo/mojo-smartset/#mojo.smartSet.addSmartSet">mojo.SmartSet.addSmartSet</a></code></td>
  </tr>
  <tr>
    <td><code>removeSmartSet</code></td>
    <td><code><a href="../../reference/api/mojo/mojo-smartset/#mojo.smartSet.removeSmartSet">mojo.SmartSet.removeSmartSet</a></code></td>
  </tr>
  <tr>
    <td><code>getSmartSets</code></td>
    <td><code><a href="../../reference/api/mojo/mojo-smartset/#mojo.smartSet.getSmartSets">mojo.SmartSet.getSmartSets</a></code></td>
  </tr>
  <tr>
    <td><code>setSmartSets</code></td>
    <td><code><a href="../../reference/api/mojo/mojo-smartset/#mojo.smartSet.setSmartSets">mojo.SmartSet.setSmartSets</a></code></td>
  </tr>
  <tr>
    <td><code>updateAllSmartSets</code></td>
    <td><code><a href="../../reference/api/mojo/mojo-smartset/#mojo.smartSet.updateAllSmartSets">mojo.SmartSet.updateAllSmartSets</a></code></td>
  </tr>
  <tr>
    <td><code>selectSmartSets</code></td>
    <td><code><a href="../../reference/api/mojo/mojo-smartset/#mojo.smartSet.selectSmartSets">mojo.SmartSet.selectSmartSets</a></code></td>
  </tr>
</table>


RoboFont FontParts Extension
---------------------

<table>
  <tr>
    <th>Deprecated</th>
    <th>Replacement</th>
  </tr>
  <tr>
    <td><code>font.generate(useMacRoman=True)</code></td>
    <td><code>useMacRoman</code> argument has been removed without replacement</td>
  </tr>
  <tr>
    <td><code>font.guides</code></td>
    <td><code>font.guidelines</code></td>
  </tr>
    <tr>
    <td><code>glyph or font.removeGuide()</code></td>
    <td><code>glyph or font.removeGuideline()</code></td>
  </tr>
  <tr>
    <td><code>glyph or font.clearGuides()</code></td>
    <td><code>glyph or font.clearGuidelines()</code></td>
  </tr>
  <tr>
    <td><code>glyph or font.addGuide()</code></td>
    <td><code>glyph or font.appendGuideline()</code></td>
  </tr>
  <tr>
    <td><code>point.type = offCurve</code></td>
    <td><code>point.type = offcurve</code></td>
  </tr>
  <tr>
    <td><code>point.type = onCurve</code></td>
    <td><code>point.type = oncurve</code></td>
  </tr>
  <tr>
    <td><code>Image.box</code></td>
    <td><code>Image.bounds</code></td>
  </tr>
  <tr>
    <td><code>Contour.selection</code></td>
    <td><code>Contour.selectedPoints</code></td>
  </tr>

  <tr>
    <td><code>Glyph.layerName</code></td>
    <td><code>Glyph.layer.name</code></td>
  </tr>

  <tr>
    <td><code>Glyph.getGlyph(glyphName)</code></td>
    <td><code>Glyph.font[glyphName]</code></td>
  </tr>

  <tr>
    <td><code>Glyph.selection</code></td>
    <td><code>Glyph.selectedPoints</code></td>
  </tr>

  <tr>
    <td><code>Layer.setDisplayOption(key, value)</code></td>
    <td><code>Layer.setDisplayOption({key: value})</code></td>
  </tr>

  <tr>
    <td><code>Font.showUI()</code></td>
    <td><code>Font.openInterface()</code></td>
  </tr>

  <tr>
    <td><code>Font.UIdocument()</code></td>
    <td><code>Font.shallowDocument()</code></td>
  </tr>

  <tr>
    <td><code>Font.box</code></td>
    <td><code>Font.bounds</code></td>
  </tr>

  <tr>
    <td><code>Font.setColorForLayer(layerName, (r, g, b, a))</code></td>
    <td><code>Font.getLayer(layerName).color = r, g, b, a</code></td>
  </tr>

  <tr>
    <td><code>Font.getColorForLayer(layerName)</code></td>
    <td><code>Font.getLayer(layerName).color</code></td>
  </tr>

  <tr>
    <td><code>Font.getLayer(layerName).setDisplayOption(option, value)</code></td>
    <td><code>Font.getLayer(layerName).setDisplayOption({key, value})</code></td>
  </tr>

  <tr>
    <td><code>Font.compileGlyph(...)</code></td>
    <td><code>glyphConstruction</code></td>
  </tr>

  <tr>
    <td><code>Font.templateSelection</code></td>
    <td><code>Font.templateSelectedGlyphNames</code></td>
  </tr>

  <tr>
    <td><code>Glyph.preferredSegmentType</code></td>
    <td>No replacement, RF4 supports both cubics and quadratics in the same glyph</td>
  </tr>

</table>


Defcon
------

<table>
  <tr>
    <th>Deprecated</th>
    <th>Replacement</th>
  </tr>
  <tr>
    <td><code>defcon.pens.reverseContourPointPen</code></td>
    <td><code><a href="https://fonttools.readthedocs.io/en/latest/pens/pointPen.html#fontTools.pens.pointPen.ReverseContourPointPen">from fontTools.pens.pointPen import ReverseContourPointPen</a></code></td>
  </tr>
</table>


ufo2fdk
-------

<table>
  <tr>
    <th>Deprecated</th>
    <th>Replacement</th>
  </tr>
  <tr>
    <td><code>ufo2fdk.pens.t2CharStringPen</code></td>
    <td><code><a href="https://fonttools.readthedocs.io/en/latest/pens/t2CharStringPen.html#fontTools.pens.t2CharStringPen.T2CharStringPen">from fontTools.pens.t2CharStringPen import T2CharStringPen</a></code></td>
  </tr>
</table>

