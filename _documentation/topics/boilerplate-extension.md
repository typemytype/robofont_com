---
layout: page
title: Boilerplate Extension
tags:
  - extensions
---

The [Boilerplate Extension] is a dummy RoboFont extension which serves as a template for creating new extensions. It is hosted on GitHub and configured as a [template repository] – so you can use it to create new repositories with the same directory structure and files, and a fresh commit history.

Different developers have {% internallink 'tutorials/packaging-extensions' text='different ways of organising extension sources' %}. The Boilerplate Extension demonstrates a simple scheme in which all files are stored in a single repository, and the extension is built from the source files using GitHub Action:

> rf-extension-boilerplate
> ├── source/
> │   ├── lib/
> │   ├── html/
> │   └── resources/
> ├── README.md
> ├── info.yaml
> ├── build.yaml
> ├── myExtension.mechanic
> └── myExtensionMechanicIcon.png
{: .asCode }

`source/`
: contains the ‘source files’ which are edited by the developer

`build.py`
: a script to generate the extension package from the source files

`README.md`
: a [markdown] file with information about the repository

`info.yaml`
: a [yaml] file with extension metadata

`build.yaml`
: a [yaml] with additional metadata for building the extension

`myExtension.yml`
: a [yaml] file with metadata for installation with Mechanic

`myExtensionMechanicIcon.png`
: an icon to identify the extension in the Mechanic extension index

[Boilerplate Extension]: http://github.com/roboDocs/rf-extension-boilerplate
[template repository]: http://help.github.com/en/articles/creating-a-repository-from-a-template
[yaml]: http://yaml.org/
[markdown]: http://en.wikipedia.org/wiki/Markdown
