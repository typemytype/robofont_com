---
layout: page
title: Tutorials
tree:
    - making-a-font
    - making-a-simple-font-family
    - making-a-complex-font-family
    - working-with-designspace-files
    - making-a-multilingual-font
    - making-italic-fonts
    - building-accented-glyphs
    - setting-font-infos
    - setting-font-names
    - drawing-glyphs
    - drawing-with-quadratic-curves
    - checking-contour-quality
    - spacing-intro
    - kerning
    - defining-character-sets
    - sorting-glyphs
    - using-production-names
    - using-mark-colors
    - marking-glyphs
    - working-with-large-fonts
    - generating-fonts
    - using-test-install
    - postscript-hinting
    - truetype-hinting
    - creating-designspace-files
    - creating-variable-fonts
    - testing-variable-fonts
    - python
    - dealing-with-errors
    - using-external-code-editors
    - using-robofont-from-command-line
    - using-git
    - making-proofs-with-drawbot
    - setting-up-a-startup-script
    - upgrading-from-py2-to-py3
    - get-font
    - get-glyph
    - scripts-font
    - scripts-glyph
    - scripts-interpolation
    - using-glyphmath
    - using-pens
    - creating-window-with-action-buttons
    - creating-font-library-browser
    - canvas
    - building-a-custom-key-observer
    - from-tool-to-extension
    - packaging-extensions
    - asking-for-help
    - submitting-bug-reports
    - editing-the-docs
    - writing-how-tos
---

*practical steps · learning-oriented · useful when studying*


<div class='row'>
<div class='col' markdown='1'>

### Making fonts

- {% internallink 'tutorials/making-a-font' %}
- {% internallink 'tutorials/making-a-simple-font-family' %}
- {% internallink 'tutorials/making-a-complex-font-family' %}
- {% internallink 'tutorials/working-with-designspace-files' %}
- {% internallink 'tutorials/making-italic-fonts' %}
- {% internallink 'tutorials/building-accented-glyphs' %}

### Setting font info

- {% internallink 'tutorials/setting-font-infos' %}
- {% internallink 'tutorials/setting-font-names' %} <span class='yellow'>✗</span>

### Drawing & spacing

- {% internallink 'tutorials/drawing-glyphs' %}
- {% internallink 'tutorials/drawing-with-quadratic-curves' %}
- {% internallink 'tutorials/checking-contour-quality' %}
- {% internallink 'tutorials/spacing-intro' %}
- {% internallink 'tutorials/kerning' %}

### Character set

- {% internallink 'tutorials/defining-character-sets' %}
- {% internallink 'tutorials/sorting-glyphs' %}
- {% internallink 'tutorials/using-production-names' %}
- {% internallink 'tutorials/using-mark-colors' %}
- {% internallink 'tutorials/marking-glyphs' %}
- {% internallink 'tutorials/working-with-large-fonts' %}

### Font generation

- {% internallink 'tutorials/generating-fonts' %}
- {% internallink 'tutorials/using-test-install' %}
- {% internallink 'tutorials/postscript-hinting' %}
- {% internallink 'tutorials/truetype-hinting' %}

### Variable fonts

- {% internallink 'tutorials/creating-designspace-files' %}
- {% internallink 'tutorials/creating-variable-fonts' %}
- {% internallink 'tutorials/testing-variable-fonts' %}

</div>
<div class='col' markdown='1'>

### Building Tools

- {% internallink 'tutorials/python' %}
- {% internallink 'tutorials/dealing-with-errors' %}
- {% internallink 'tutorials/using-external-code-editors' %}
- {% internallink 'tutorials/using-robofont-from-command-line' %}
- {% internallink 'tutorials/using-git' %}
- {% internallink 'tutorials/making-proofs-with-drawbot' %}
- {% internallink 'tutorials/setting-up-a-startup-script' %}
- {% internallink 'tutorials/upgrading-from-py2-to-py3' %}
- {% internallink 'tutorials/improving-code-efficiency' %}

### FontParts

- {% internallink 'tutorials/get-font' %}
- {% internallink 'tutorials/get-glyph' %}
- {% internallink 'tutorials/scripts-font' %}
- {% internallink 'tutorials/scripts-glyph' %}
- {% internallink 'tutorials/scripts-interpolation' %}
- {% internallink 'tutorials/using-glyphmath' %}
- {% internallink 'tutorials/using-pens' %}

### vanilla

- {% internallink 'tutorials/creating-window-with-action-buttons' %}
- {% internallink 'tutorials/creating-font-library-browser' %}

### mojo

- {% internallink 'tutorials/canvas' %}
- {% internallink 'tutorials/building-a-custom-key-observer' %}

### Extensions

- {% internallink 'tutorials/creating-extensions' %}
- {% internallink 'tutorials/from-tool-to-extension' %}
- {% internallink 'tutorials/packaging-extensions' %}

### Help & support

- {% internallink 'tutorials/asking-for-help' %}
- {% internallink 'tutorials/submitting-bug-reports' %}

### Documentation

- {% internallink 'tutorials/editing-the-docs' %}
- {% internallink 'tutorials/writing-how-tos' %}
</div>
</div>
