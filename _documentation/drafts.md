---
layout: page
title: Drafts
excludeSearch: true
excludeNavigation: true
---

{% assign drafItems = "" | split:"|" %}
{% for item in site.documentation %}
    {% if item.draft %}
        {% assign drafItems = drafItems | push: item %}
    {% endif %}
{% assign drafItems = drafItems | sort:"title" %}
{% endfor %}

A list of draft pages:

> read more how to jump in {% internallink "editing-the-docs" text='by editing the docs'%} and {% internallink "tutorials/writing-how-tos" text="how to write How To's" %}:
{: .note }

<ul>
{% for item in drafItems %}
<li>
    <a class="draft-source" href="{{ site.gitlabrepo }}{{ site.branch_name | default: site.default_branch_name }}/{{ item.path }}">edit</a>    
    <a href="{{ site.baseurl }}{{ item.url }}">{{ item.title }}</a>
</li>
{% endfor %}
</ul>

# To Do's

A list of pages with todo notes:

{% assign todoItems = "" | split:"|" %}
{% for item in site.documentation %}
    {% if item.content contains ": .todo" %}
        {% assign todoItems = todoItems | push: item %}
    {% endif %}
{% assign todoItems = todoItems | sort:"title" %}
{% endfor %}


<ul>
{% for item in todoItems %}
<li>
    <a class="draft-source" href="{{ site.gitlabrepo }}{{ site.branch_name | default: site.default_branch_name }}/{{ item.path }}">edit</a>    
    <a href="{{ site.baseurl }}{{ item.url }}">{{ item.title }}</a>
</li>
{% endfor %}
</ul>

# Comments's

A list of pages with comments notes:

{% assign commentsItems = "" | split:"|" %}
{% for item in site.documentation %}
    {% if item.content contains "% endcomment %" %}
        {% assign commentsItems = commentsItems | push: item %}
    {% endif %}
{% assign commentsItems = commentsItems | sort:"title" %}
{% endfor %}


<ul>
{% for item in commentsItems %}
<li>
    <a class="draft-source" href="{{ site.gitlabrepo }}{{ site.branch_name | default: site.default_branch_name }}/{{ item.path }}">edit</a>    
    <a href="{{ site.baseurl }}{{ item.url }}">{{ item.title }}</a>
</li>
{% endfor %}
</ul>


