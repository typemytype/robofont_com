---
layout: page
title: Synchronize Glyph Editors Across Fonts
tags:
  - scripting
  - subscriber
---

The following script leverages the power of {% internallink 'topics/subscriber' text="`Subscriber`" %} to synchronize multiple glyph editors across different fonts to the current glyph with a tiny amount of code. The script uses the `font.info.styleName` to avoid aligning also glyph editors from the same font.

> Did you know you can have multiple glyph editors opened from the same font? Just press alt when clicking on glyph from the Font Overview
{: .seealso}

{% showcode how-tos/subscriber/synchronizeGlyphEditorsAcrossFonts.py %}
