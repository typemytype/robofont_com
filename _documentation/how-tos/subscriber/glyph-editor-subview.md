---
layout: page
title: Glyph Editor subview
tags:
  - subscriber
---

This example shows how to draw or add controls directly into the {% internallink 'workspace/glyph-editor' %}.

{% image how-tos/subscriber/addGlyphEditorSubviewExample.png %}

{% showcode how-tos/subscriber/addGlyphEditorSubviewExample.py %}

> - [Adding text to CurrentGlyphView (RoboFont Forum)](http://forum.robofont.com/topic/543/adding-text-to-currentglyphview/9)
{: .seealso }
