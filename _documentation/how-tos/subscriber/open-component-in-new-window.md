---
layout: page
title: Open component in new window
tags:
  - subscriber
  - scripting
description: A script showing how to perform an action with a hot key using subscriber.
---

This example shows how to use a subscriber to perform an action when a key is pressed. If a component is selected in the {% internallink 'workspace/glyph-editor' %}, pressing `Shift C` will open a new glyph window with the selected component’s base glyph.

{% showcode how-tos/subscriber/openComponentInNewWindow.py %}
