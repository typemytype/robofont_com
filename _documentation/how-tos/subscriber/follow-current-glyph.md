---
layout: page
title: Follow the Current Glyph
tags:
  - subscriber
---

The following example shows how to follow the `CurrentGlyph()` across every UI component in the RoboFont application. Consider that the `Subscriber` class needs to be registered at the application level with `registerRoboFontSubscriber`, not with `registerCurrentGlyphSubscriber`.

{% showcode how-tos/subscriber/followCurrentGlyph.py %}
