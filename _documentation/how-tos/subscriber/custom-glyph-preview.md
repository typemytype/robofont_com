---
layout: page
title: Custom glyph preview
tags:
  - subscriber
---

{% image how-tos/subscriber/bubblesPreview.png %}

This example shows how to draw something in the canvas when previewing a glyph.

{% showcode how-tos/subscriber/bubblesPreview.py %}
