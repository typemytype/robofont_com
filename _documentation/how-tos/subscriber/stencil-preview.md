---
layout: page
title: Stencil preview
tags:
  - subscriber
  - merz
  - scripting
---

{% image how-tos/subscriber/stencilPreview_0.png %}

{% image how-tos/subscriber/stencilPreview_1.png %}

This example shows a simple stencil preview tool. The result is produced by subtracting the background layer from the foreground using {% internallink 'topics/boolean-glyphmath' %}.

{% showcode how-tos/subscriber/stencilPreview.py %}
