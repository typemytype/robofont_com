---
layout: page
title: Subscribing to one single font
tags:
  - subscriber
  - scripting
---

* Table of Contents
{:toc}


This example shows a floating window that captures all changes for one specific font – our *adjunct font*.

The tool uses the Subscriber [`setAdjunctObjectsToObserve`](https://robofont.com/documentation/reference/api/mojo/mojo-subscriber/?highlight=setAdjunctObjectsToObserve) to subscribe to the provided font's adjunct objects to receive callbacks for any changes in kerning, info, features or outlines.


{% showcode how-tos/subscriber/singleFontWithUI.py %}
