---
layout: page
title: Multiple subscribers listening to one window
tags:
  - subscriber
  - vanilla
  - merz
---

This example shows how to control multiple glyph editor subscribers using a single [vanilla](https://ts-vanilla.readthedocs.io/en/latest/) `Window`.

{% video how-tos/one-window-multiple-subscribers.mp4 %}

First of all, we need to create a custom subscriber event. It is handy to keep custom subscriber events in a separate module, so that the module can be executed only once, when an extension is started.

{% showcode how-tos/one-window-multiple-subscribers/events.py %}

> If you pack this code into an extension, you should set the events.py module as extension startup script
{: .tip }

The rest of the tool is organized in two objects:
+ a WindowController subclass (owning a vanilla window)
+ a Subscriber subclass (taking care of drawing in the glyph editors)

The Subscriber object draws a stroke around any outline in the glyph editors while the WindowController owns a slider that sets the stroke thickness. The drawing is performed using {% internallink "topics/merz" text="Merz" %}.

{% showcode how-tos/one-window-multiple-subscribers/tool.py %}

A few aspects worth noting:
+ You have to register and unregister the `Subscriber` tool inside the `started` and `destroy` events of the `WindowController` subclass. Here, you should set the `controller` attribute of the `Subscriber` subclass
+ Remember to fire `postEvent` inside the user interface callbacks, in this example the `sliderCallback`
+ You can access the modified value from the user interface inside the custom event callback in the subscriber subclass with `self.controller.thickness`

> + You can find a specific RoboFont extension boilerplate [here](https://github.com/roboDocs/single-windowcontroller-with-multiple-subscribers)
> + A more complex example can be found in the [Outliner](https://github.com/typemytype/outlinerRoboFontExtension) repository
{: .seealso}