---
layout: page
title: Installing RoboFont
---

1. Download the installer from the [Downloads](../../../downloads) page.

2. Double-click the `.dmg` file in Finder to open it. The following window will appear:

   {% image how-tos/install-robofont.png %}

3. Drag the RoboFont icon to the *Applications* folder to install it.

> Don’t forget to unmount the `.dmg` when you’re done – just click on the *Eject* arrow.
{: .note }
