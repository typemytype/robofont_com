---
layout: page
title: Scripting Smart Sets
tags:
  - mojo
  - smart sets
---

* Table of Contents
{:toc}

The scripts below demonstrate the {% internallink 'reference/api/mojo/mojo-smartset' %} API.


Inspecting Smart Sets
------------------

If you only need to inspect smart sets without altering the data, you can use `getSmartSets()`

```python
from mojo.smartSet import getSmartSets

# get default sets
defaultSets = getSmartSets()
print(defaultSets)

# get font sets
f = CurrentFont()
fontSets = getSmartSets(f)
print(fontSets)
```


Modifying Smart Sets
------------------

Instead, if you need to alter the smart sets, we strongly advice to use the `with smartSetsEditor()` context, to force an update of the RoboFont UI at the end of the process. The `with` context exposes a `list` that can be modified with standard methods like `.append()`, `.extend()` or `.remove()`

#### Append one smart set

```python
from mojo.smartSet import SmartSetsEditor, SmartSet

with SmartSetsEditor() as smartSets:
    smartSets.append(
        SmartSet(smartSetName="foo", glyphNames=["a", "b"]),
    )
```

#### Append several smart sets

```python
from mojo.smartSet import SmartSet, SmartSetsEditor

# a bunch of fresh new smart sets
items = [
    SmartSet(smartSetName="foo", glyphNames=["a", "b"]),
    SmartSet(smartSetName="bar", query="Width < 200")
]

with SmartSetsEditor() as smartSets:
    smartSets.extend(items)
```

#### Append smart set per mark color name

```python
from mojo.UI import getDefault
from mojo.smartSet import SmartSet, SmartSetsEditor
from lib.tools.misc import rgbaToString

# get all colors
colors = getDefault("markColors")

with SmartSetsEditor() as smartSets:
    group = None
    # look if there is already a Mark Color smart set group
    for smartset in smartSets:
        if smartset.name == "Mark Colors":
            group = smartset
            # remove all the group items
            group.removeGroups()
            break
    
    # if nothing is found, create a new group smart set
    if group is None:
        group = SmartSet()    
        group.name = "Mark Colors"
        smartSets.append(group)
    # loop over all colors and name and add a smart set query
    for rgba, name in colors:
        group.addGroupSmartSet(SmartSet(smartSetName=name, query=f"MarkColor == '{rgbaToString(rgba)}'"))
```

#### Clear all smart sets

> The following example will delete all existing smart sets, be aware!
{: .warning}

```python
from mojo.smartSet import SmartSetsEditor

with SmartSetsEditor() as smartSets:
    smartSets.clear()
```

#### Font-specific smart sets

If you want to create a font-specific smart set, remember to pass the font object to the `with smartSetsEditor()` context. The `SmartSet` object itself does not know if it belongs to a font or not. Check the example:

```python
from mojo.smartSet import SmartSetsEditor, SmartSet
from mojo.roboFont import CurrentFont

fontSmartSet = SmartSet(smartSetName="alternates",
                        glyphNames=["a.001", "b.001"])
myFont = CurrentFont()

with SmartSetsEditor(myFont) as smartSets:
    smartSets.append(fontSmartSet)
```


> - {% internallink 'topics/smartsets' %}
> - {% internallink 'workspace/font-overview/smart-sets-panel' %}
{: .seealso }


Remove Smart Sets
-----------------

If you want to remove a `SmartSet` by using its name, you can do it either globally:

```python
from mojo.smartSet import SmartSetsEditor

removeName = 'mySmartset'

# global sets!
with SmartSetsEditor() as smartSets:
    for smartset in smartSets:
        if smartset.name == removeName:
            smartSets.remove(smartset)
```

or locally:

```python
from mojo.smartSet import SmartSetsEditor
from mojo.roboFont import CurrentFont

removeName = 'mySmartset'

# font sets!
font = CurrentFont()
with SmartSetsEditor(font) as smartSets:
    for smartset in smartSets:
        if smartset.name == removeName:
            smartSets.remove(smartset)

```


Query-based Smart Sets
----------------------

Queries are expressed using Apple’s [Predicate Format String Syntax].

### Language tokens

These are the main building blocks of the predicate language:

| construct             | examples                                            |
| --------------------- | --------------------------------------------------- |
| literals              | `False` `True` `'a'` `42` `{'a', 'b'}`              |
| compound expressions  | `and` `or` `not`                                    |
| aggregate operations  | `any` `some` `all` `none` `in`                      |
| basic comparisons     | `=` `>=` `<=` `>` `<` `!=` `between`                |
| string comparisons    | `contains` `beginswith` `endswith` `like` `matches` |

> The `matches` comparison requires a regular expression. (see the example below)
{: .note }

### Glyph attributes and supported comparisons

These are the glyph attribute names, their data types and supported comparisons.

<table>
  <tr>
    <th width='30%'>type</th>
    <th width='30%'>attributes</th>
    <th width='40%'>comparisons</th>
  </tr>
  <tr>
    <td><code>str</code></td>
    <td>
      <code>Name</code><br/>
      <code>ComponentsNames</code><br/>
      <code>AnchorNames</code>
    </td>
    <td>
      <code>in</code>
      <code>contains</code>
      <code>beginswith</code>
      <code>endswith</code>
      <code>like</code>
      <code>matches</code>
    </td>
  </tr>
  <tr>
    <td><code>int</code> or <code>float</code></td>
    <td>
      <code>Width</code><br/>
      <code>LeftMargin</code><br/>
      <code>RightMargin</code><br/>
      <code>Unicode</code><br/>
      <code>Contours</code><br/>
      <code>Components</code><br/>
      <code>Anchors</code>
    </td>
    <td>
      <code><</code>
      <code>=</code>
      <code>></code>
      <code>!=</code>
      <code>between</code>
      <code>in</code>
    </td>
  </tr>
  <tr>
    <td><code>str</code></td>
    <td>
      <code>MarkColor</code>
    </td>
    <td>
      <code>=</code>
      <code>!=</code>
      <code>contains</code>
      <code>matches</code>
    </td>
  </tr>
  <tr>
    <td><code>bool</code></td>
    <td>
      <code>Empty</code><br/>
      <code>GlyphChanged</code><br/>
      <code>Template</code><br/>
      <code>SkipExport</code>
    </td>
    <td>
      <code>True</code> <code>False</code>
    </td>
  </tr>
  <tr>
    <td><code>str</code></td>
    <td>
      <code>Note</code>
    </td>
    <td>
      <code>contains</code>
    </td>
  </tr>
</table>

### Example queries

```text
"Name == 'a'"
"Name in {'a', 'b'}"
"Empty == 'True'"
"Name contains 'a' AND Width < 300"
"Name matches '[A-z]'"
"Width in {120, 240, 360}")
```

> - {% internallink 'workspace/font-overview/search-glyphs-panel' %}
{: .seealso }

[Predicate Format String Syntax]: http://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/Predicates/Articles/pSyntax.html#//apple_ref/doc/uid/TP40001795-CJBDBHCB
