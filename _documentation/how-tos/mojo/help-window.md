---
layout: page
title: Help Window
tags:
  - mojo
---

The Help window is a simple HTML browser for viewing documentation pages.

{% image how-tos/mojo/HelpWindow.png %}

```python
from mojo.UI import HelpWindow

HelpWindow("http://robofont.com")
```
