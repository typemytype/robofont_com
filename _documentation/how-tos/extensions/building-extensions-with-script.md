---
layout: page
title: Building extensions with a script
treeTitle: Building with a script
tags:
  - extensions
---

If you are more comfortable writing code than using UI-based tools like the {% internallink 'how-tos/extensions/building-extensions-with-extension-builder' text='Extension Builder' %}, you can also build your extension with a script using the `ExtensionBundle` object from {% internallink "reference/api/mojo/mojo-extensions#mojo.extensions.ExtensionBundle" text='mojo.extensions' %}.

Check the example code in the [robofontExtensionBundle](https://github.com/typemytype/roboFontExtensionBundle) repository on GitHub.