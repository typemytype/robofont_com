---
layout: page
title: Building extensions with Github Actions
treeTitle: Building with Github Actions
tags:
  - extensions
---

Since the `ExtensionBundle` class has been [open-sourced](https://github.com/typemytype/roboFontExtensionBundle), it is possible to build RoboFont Extensions outside RoboFont using the terminal. It becomes then quite easy to combine the new package with CI/CD tools like Github Actions. The RoboFont team prepared a [ready-to-use action](https://github.com/typemytype/roboFont-Extension-action) to be included in your repositories. You'll need to pack your data into `info.yaml` and `build.yaml` files and organize the data of your extension into a specific folder. Several extensions or templates already use Github Actions to deal with versioning and distribution. Check the links at the end of the article.

> - [Batch](https://github.com/typemytype/batchRoboFontExtension)
> - [Lasso Tool](https://github.com/roboDocs/LassoTool)
> - [Single WindowController ruling over several Subscribers](https://github.com/roboDocs/single-windowcontroller-with-multiple-subscribers)
> - [Extension Boilerplate](https://github.com/roboDocs/rf-extension-boilerplate)
{: .seealso }