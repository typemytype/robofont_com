---
layout: page
title: Generating and using custom GNFUL
tags:
  - unicode
  - character set
---

[GNUFL, Glyph Namer Formatted Unicode List](http://github.com/LettError/glyphNameFormatter) is a glyph name - unicode generator. You can specify your custom GNFUL in the Preferences > {% internallink "/reference/workspace/preferences-window/font-overview" text="Font Overview" %}.

{% image /reference/workspace/preferences-window/preferences_font-overview_character-set.png %}

The glyphNameFormatter module is {% internallink "/reference/embedded-libraries" text="embedded" %} in RoboFont, so you can access it with Python. The following example loads a custom GNFUL and generates a list with a custom separator.

```python
from glyphNameFormatter.exporters.exportFlatLists import generateFlat

gnfulPath = "path/to/your/gnful.txt"

generateFlat(gnfulPath, scriptSeparator=":")
```