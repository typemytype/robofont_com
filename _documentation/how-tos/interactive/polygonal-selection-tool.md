---
layout: page
title: Polygonal selection tool
---

This example shows a polygonal selection tool based on the [EditingTool].

{% showcode how-tos/interactive/polygonSelection.py %}

This snippet has been recently wrapped into the [Lasso Tool](https://github.com/roboDocs/LassoTool) extension.

[EditingTool]: ../../../reference/api/mojo/mojo-events/#mojo.events.EditingTool
