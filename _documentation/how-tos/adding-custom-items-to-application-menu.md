---
layout: page
title: Adding items to the application menu
tags:
  - scripting
  - UI
description: Learn to add custom items or scripts to the Mojo application menu using MenuBuilder.
---

* Table of Contents
{:toc}

## Create a menu from callbacks

It is possible to create menu items for functions in your code. You only need to pass a list of tuples (menu item string, function identifier) to the {% internallink "/reference/api/mojo/mojo-ui#mojo.UI.MenuBuilder" text="MenuBuilder" %} class constructor. Check the following example:

```python
from mojo.UI import MenuBuilder

def foo(sender):
    print(sender.title())


if __name__ == '__main__':
    menu = MenuBuilder(menu="main",
                       title="bar",
                       insert=5,
                       items=[("foo1", foo),
                              ("foo2", foo)]
                       )
```


## Create a menu from a folder with scripts

The {% internallink "/reference/api/mojo/mojo-ui#mojo.UI.MenuBuilder" text="MenuBuilder" %} class has the useful method `.addMenuFromPath()` for adding a folder of python scripts to the application menu

```python
from mojo.UI import MenuBuilder

folderPath = 'folderWithScripts'

if __name__ == '__main__':
    menu = MenuBuilder(menu="main", title="test")
    menu.addMenuFromPath(folderPath)
```

Both snippets can be set as {% internallink "/tutorials/setting-up-a-startup-script" text="startup scripts" %} from the application preferences menu

> {% internallink "/reference/api/mojo/mojo-ui#mojo.UI.MenuBuilder" text="mojo.ui MenuBuilder reference" %}
{: .seealso}