---
layout: page
title: Proof OpenType features
tags:
  - scripting
  - drawBot
  - proofing
---

This example shows a simple helper for visualising the OpenType features in a font, using an interface with checkboxes to quickly activate/deactivate the features.

{% image how-tos/drawbot/proofFeatures.png %}

The script works by getting a list of all features in the font, which is then used to create an interface with one checkbox per feature. A slider to adjust the text size is also included. Finally, the values from the interface are applied to the text.

{% showcode how-tos/drawbot/proofOpenTypeFeatures.py %}
