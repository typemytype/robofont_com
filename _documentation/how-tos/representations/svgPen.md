---
layout: page
title: Glyph SVG Syntax Representation
---

This example shows how to register a `defcon.Glyph` representation that outputs a svg syntax representation of the outline. The svg data is then sent to a `HTMLView` stored in a `WindowController`.

{% showcode how-tos/representations/svgPen.py %}

> - {% internallink "topics/subscriber" %}
> - {% internallink "topics/defcon-representation" %}
{: .seealso}