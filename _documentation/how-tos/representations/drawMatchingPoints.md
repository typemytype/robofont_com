---
layout: page
title: Draw Matching Points with a Reference Layer
tags:
  - subscriber
  - merz
---

This example shows how to draw a `merz` [symbol](https://typesupply.github.io/merz/objects/symbol.html) over each point in the current layer matching another point in a reference layer. The tool uses a set intersection to compare the coordinates between layers. A {% internallink "topics/defcon-representation" text="defcon representation factory" %} takes care of generating the sets avoiding redundant work thanks to its caching system.

{% image how-tos/representations/matchingPoints.png %}

{% showcode how-tos/representations/matchingPoints.py %}

> Merz reuses symbols that have the same settings. There is an internal cache system that stores images based on their settings. You can have 1000 symbols in different locations and it will only create 1 image for all of those as long as the image settings are the same.
{: .tip}

> - {% internallink "topics/subscriber" %}
> - {% internallink "topics/merz" %}
> - {% internallink "topics/defcon-representation" %}
{: .seealso}