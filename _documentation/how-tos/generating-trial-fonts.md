---
layout: page
title: Generating trial fonts
tags:
  - OpenType
  - scripting
---

* Table of Contents
{:toc}


Many designers and foundries like to offer *trial versions* of their fonts. Such fonts are often limited versions of the complete fonts, with a reduced character set and/or with a reduced set of features.

The {% glossary FontTools %} module includes a very powerful [subsetter] which can be used to generate trial fonts. It provides several options to remove data from a font – from removing glyphs to OpenType features, kerning, hinting, font names, etc.

[subsetter]: http://github.com/fonttools/fonttools/blob/master/Lib/fontTools/subset/__init__.py


Using the subsetter in a script
-------------------------------

RoboFont comes with FontTools as an {% internallink 'reference/embedded-libraries' text='embedded module' %}, so you can use it in any script.

The example script below does the following:

1. open a dialog to select an existing UFO font
2. generate a complete OpenType font from the UFO
3. close the font without saving any changes
3. generate a trial font by subsetting the complete font
4. remove the complete font

{% showcode how-tos/fontToolsSubsetterExample.py %}


Using the subsetter in Terminal
-------------------------------

If FontTools is installed in the system Python, the subsetter is also available in Terminal as the command line tool `ftpysubset`:

```text
pyftsubset myFolder/myFont.otf --text='ABCDXYZ abcdxyz'
```

See the included `--help` for more information about each option.

```text
pyftsubset --help
```
