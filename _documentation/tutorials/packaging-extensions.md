---
layout: page
title: Packaging extensions
tags:
  - extensions
  - version control
---

{::options toc_levels='2' /}

* Table of Contents
{:toc}

With the release of the open-source [RoboFont Extension Bundle](https://github.com/typemytype/roboFontExtensionBundle) module, packaging and distributing extensions has never been easier. This article will present the preferred method to package extensions and two older methods still supported.

## GitHub Actions

GitHub Actions is a GitHub service that allows to easily build continuous integration (CI) and continuous deployment (CD) pipelines. In other words, through these actions we can run code on the GitHub servers following git events. It is possible to automatically handle releases and validate extensions. To achieve this result we need to follow a specific file structure:

> http://github.com/user/myExtension
> ├─ .github/
> │  └─ workflows/
> │     └─ validate_build.yml
> ├─ info.yaml
> ├─ build.yaml
> └─ source/
>    ├─ lib/
>    │  └─ tool.py
>    ├─ html/
>    │  └─ index.html
>    └─ resources/
>       └─ image.png
{: .asCode }

A few notes:
- The `info.yaml` file replicates the `info.plist` content.
- The `build.yaml` stores the location to the source subfolders (lib, html and resources) and an optional `path` if [it should be different from the extension name](https://github.com/typemytype/roboFont-Extension-action?tab=readme-ov-file#naming)
- The `validate_build.yml` file contains the github action instructions.

Check the examples at the end of this section for some use cases.

Consider that following this pattern, it will not be necessary to track the bundle in the repository history. The `validate_build.yml` action will take care of building and releasing a new extension any time a commit contains a different version number in the `info.yaml` file (assuming the `autotagging` is set to [true](https://github.com/typemytype/roboFont-Extension-action?tab=readme-ov-file#autotagging)).


### Advantages & disadvantages

<table>
<tr>
  <th width='50%'>advantages</th>
  <th width='50%'>disadvantages</th>
</tr>
<tr>
  <td>
    <ul>
      <li>build and release process is completely automated</li>
      <li>no duplication between bundle and source code</li>
      <li>no manual editing of info.plist</li>
    </ul>
  </td>
  <td>
    <ul>
      <li>none, really</li>
    </ul>
  </td>
</tr>
</table>


### Examples

- [Extension Boilerplate](https://github.com/roboDocs/rf-extension-boilerplate)
- [Single WindowController ruling over...](https://github.com/roboDocs/single-windowcontroller-with-multiple-subscribers)
- [Batch](http://github.com/typemytype/batchRoboFontExtension)
- [Outliner](http://github.com/typemytype/outlinerRoboFontExtension)
- [CornerTools](http://github.com/roboDocs/CornerTools)


## The extension is the source (legacy)

There is no separation between the extension source and the built extension: the code is written as an extension according to the {% internallink 'reference/extensions/extension-file-spec' %}.

> http://github.com/user/myExtension
> ├── myExtension.roboFontExt
> │   ├── lib/
> │   ├── html/
> │   ├── info.plist
> │   └── license
> ├── README.md
> └── license.txt
{: .asCode }

### Workflow

1. Install the extension. The files will be copied into the plugins folder:

    ```text
    /Users/username/Library/Application Support/RoboFont/plugins
    ```

2. If you need to make changes to the code, you can do it directly in the installed extension – so you can test the changes right away.

3. Once you’ve finished making changes, you can copy the modified files back into the source folder, overwriting the older files.

4. Follow the usual process to update the extension: add and commit your changes, and push to the remote server.

### Advantages & disadvantages

<table>
<tr>
  <th width='50%'>advantages</th>
  <th width='50%'>disadvantages</th>
</tr>
<tr>
  <td>
    <ul>
      <li>no build process</li>
    </ul>
  </td>
  <td>
    <ul>
      <li>working inside the plugins folder</li>
      <li>updating files manually</li>
    </ul>
  </td>
</tr>
</table>


## Source and extension in the same repository (legacy)

This scheme is common for extensions which are built with a script. The repository contains both the source files and the built extension.

**The source files are used for development, the extension for distribution.**

> http://github.com/user/myExtension
> ├── source/
> │   ├── code/
> │   └── documentation/
> ├── build/myExtension.roboFontExt
> │   ├── lib/
> │   ├── html/
> │   ├── info.plist
> │   └── license
> ├── README.md
> ├── build.py
> └── license.txt
{: .asCode }

### Workflow

1. The code is developed as unpackaged source code by calling scripts directly.

2. When you are done making changes, run a build script to update the extension.

3. The generated extension can be installed for testing.

4. Follow the usual process to update the extension: add and commit your changes, and push to the remote server.

### Advantages & disadvantages

<table>
<tr>
  <th width='50%'>advantages</th>
  <th width='50%'>disadvantages</th>
</tr>
<tr>
  <td>
    <ul>
      <li>source folder can be anywhere</li>
      <li>not necessary to work inside the plugins folder like <a href='#workflow'>Pattern 1</a></li>
    </ul>
  </td>
  <td>
    <ul>
      <li>duplication of data in the same repository (source / extension)</li>
    </ul>
  </td>
</tr>
</table>

[Pattern 2]: #2-source-and-extension-in-the-same-repository

> Before the release of the open-source [RoboFont Extension Bundle](https://github.com/typemytype/roboFontExtensionBundle) package, this article presented a different set of patterns. We decided to remove entirely pattern #3 "Source and extension in separate repositories" and pattern #4 "Multiple extensions in a single repository" as they are totally discouraged.
{: .warning}