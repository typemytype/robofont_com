---
layout: page
title: Introduction to Python
tree:
  - introduction
  - uses
  - terminal
  - language
  - principles
  - numbers-arithmetics
  - variables
  - strings
  - lists
  - tuples
  - sets
  - collections-loops
  - comparisons
  - conditionals
  - boolean-logic
  - dictionaries
  - builtin-functions
  - functions
  - objects
  - modules
  - standard-modules
  - external-modules
  - custom-modules
  - resources
treeCanHide: true
---

*A basic introduction into the Python programming language for designers.*

{% tree page.url levels=3 %}
