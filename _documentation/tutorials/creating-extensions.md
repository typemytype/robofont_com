---
layout: page
title: Creating extensions
tags:
  - extensions
---

* Table of Contents
{:toc}


Creating your own extensions
----------------------------

*RoboFont is built on the principle that designers should adapt their tools to their ideas – not the other way around.*

RoboFont enables custom tool development by offering extensive documentation of its APIs, as well as examples and step-by-step guides.

If you would like to create your own tools and extensions, have a look at the chapter documentation dedicated to scripting. Give it a try! And if you have any questions along the way, don’t be shy to ask them on the [forum][Forum > Extensions].


Finding a developer
-------------------

While Python is easy and fun to learn, becoming fluent does require some time and effort. If you are under pressure to find a quick solution to a problem, it might make more sense to hire a specialist who can do the work for you.

The Extension Store includes a list of [Certified Developers] you can get in touch with.

You can also use the [Extensions][Forum > Extensions] section of the forum to discuss your extension ideas with other RoboFont users.

[Certified Developers]: http://extensionstore.robofont.com/certified-developers/
[Forum > Extensions]: http://forum.robofont.com/category/13/extensions
