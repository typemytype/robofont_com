---
layout: page
title: Modules
---

* Table of Contents
{:toc}


Introduction
------------

A module is an *organizational unit* of Python code. Each module has its own namespace and contains arbitrary Python objects. Code from a module can be loaded into a script using the `import` statement.


Many modules to choose from
---------------------------

{% internallink 'tutorials/python/standard-modules' %}
: Python comes with many modules included and ready to use in your scripts. These standard modules cover a wide range of tools, including file system access, numerical and mathematical functions, unicode text processing, dealing with dates and calendars, reading and writing several types of data formats, serving web pages, generating random numbers… and a lot more.

{% internallink 'tutorials/python/external-modules' %}
: Python also allows users to install and use third-party modules from multiple sources. The {% glossary Python Package Index (PyPI) %} provides an extensive list of libraries developed by the Python community, and many more can be found on code sharing platforms like GitHub. Third-party packages can be installed using a package manager like {% glossary pip %}, or manually using setup scripts or `.pth` files.

{% internallink 'tutorials/python/custom-modules' %}
: If you can’t find an existing module which suits your needs, Python allows you to create your own, and use it alongside all your other modules. That’s how many useful tools start their life: someone scratches his / her own itch, and ends up creating something that can benefit lots of other people too.


Importing modules
-----------------

Before you can use a module, you need to *import* it into your script. This is done with the `import` command:

```python
import myModule
```

After this line, the module becomes available in your script.

If you wish to see what’s inside the module, use the built-in function `dir()`:

```python
print(dir(myModule))
```

> This example assumes that the module is installed. It also works if `myModule` lives next to the current script.
>
> If you wish to use an external or custom module and haven’t installed it yet, follow the steps described in [Installing modules].
{: .note }

[Installing modules]: ../external-modules/#installing-modules


Reloading a module
------------------

Imported modules are compiled and cached, so they load faster when imported again.

While working on our own modules, we are constantly making changes to their contents. But because the module is cached, these changes are **not** imported into our scripts right away – unless we explicitly tell Python to *reload* the module:

```python
from importlib import reload
import myModule  # import the module
reload(myModule) # reload the module
```

> In Python 2, `reload` is a built-in function and does not have to be imported.
{: .note }


Accessing the contents of a module
----------------------------------

Python offers different ways to import the contents of a module into a script.

### Import module, access objects using dot notation

We can simply import the module, and access its contents using the dot notation (just like we do with [object attributes and methods]):

```python
import myModule
myModule.myFunction()
```

[object attributes and methods]: ../objects/#object-attributes-and-methods

The namespaces of the module and of the script in which it is imported are kept separated in this approach. This avoids *namespace collisions* in case your main script contains objects with the same name as objects in the module:

```python
import myModule

def myFunction():
    pass

# module function
myModule.myFunction()

# local function
myFunction()
```

### Import everything from a module

The syntax below allows us to import the names of *all*\* objects contained in a module into the namespace of the current script:

```python
from myModule import *
```

> \* With the exception of names starting with an underscore, which are private.
{: .note }

> This approach can save some typing during development and in interactive sessions, but it is generally discouraged when releasing code – because it pollutes the main namespace, and has a negative impact on code readability.
{: .warning }

### Selective import

Rather than importing everything from a module, we can be more selective and import only the objects we need in the current script:

```python
from myModule import myFunction, myOtherFunction, someVariable
```

### Import objects under a different name

To avoid namespace collisions with objects in the current script, it’s also possible to import module objects under a different name using the `as` keyword:

```python
from myModule import myFunction as myModuleFunction

def myFunction():
    pass

# module function
myModuleFunction()

# local function
myFunction()
```

> In all variations of `from … import …` only the objects in the module are imported – the module itself is not.
>
> ```python
> from myModule import myFunction
> print(myFunction)
> print(myModule)
> ```
>
> ```plaintext
> <function myFunction at 0x11081da70>
> NameError: name 'myModule' is not defined
> ```
{: .note }

- - -

> - [Python Glossary > Module](http://docs.python.org/3.9/glossary.html#term-module)
> - [The import system](http://docs.python.org/3.9/reference/import.html#importsystem)
{: .seealso }
