---
layout: page
title: External modules
---

* Table of Contents
{:toc}


The Python ecosystem goes far beyond the Standard Library, with many third-party modules available on the [Python Package Index (PyPI)][PyPI] and elsewhere on the web.

Because these modules are not part of the standard Python distribution, they need to be installed on your system first.

> RoboFont comes with several external modules embedded and ready to use – see {% internallink 'reference/embedded-libraries' %} for the complete list.
{: .note }

[PyPI]: http://pypi.org/


Installing modules
------------------

There are different ways to install a module. We recommend using {% glossary pip %}.

### Installing modules with pip

Installing packages using `pip` in Terminal will make them available for the system Python. Using the {% internallink 'workspace/package-installer' %} will make packages available only inside RoboFont.

`pip` is included in the latest distributions of Python. Make sure it’s up-to-date:

```text
pip install -U pip
```

Packages from the [Python Package Index (PyPI)](#) can be installed by name:

```text
pip install SomePackage          # latest version
pip install SomePackage==1.0.4   # specific version
pip install 'SomePackage>=1.0.4' # minimum version
```

It’s also possible to install a package from a git repository:

```text
pip install https://github.com/user/repository/archive/branch.zip
```

> - [pip User Guide](https://pip.pypa.io/en/stable/user_guide/)
{: .seealso }

### Installing modules with setup scripts

Some packages include a setup script which copies all files to their appropriate locations. Such a package can be installed by running its `setup.py` script in Terminal:

```text
python /someFolder/myPackage/setup.py
```

### Installing modules with .pth files

Modules can be installed manually using `.pth` files – simple text files containing the path to the root folder where the module is located. For example:

```text
/someFolder/myPackage/Lib/
```

> The easiest way to get the full path for a file or folder – without having to type it – is by dragging it from Finder into a code editor or Terminal.
{: .tip }

`.pth` files must be saved in the `site-packages` folder of the desired version of Python:

```plaintext
/Library/Python/3.9/site-packages/myModule.pth
```

This method installs packages by creating a *reference* to the folder which contains the module.

### Appending modules to sys.path

```python
import sys
sys.path.append('/someFolder/myPackage/Lib/')
```


Testing a module
----------------

To see if a module is installed, just try to import it in the desired environment:

```python
import myModule
```

If no error is raised, you’re good to go.

Some useful external modules
----------------------------

[markdown](http://pythonhosted.org/Markdown/)
: Generating html from {% glossary markdown %} sources. *Embedded in RoboFont since version 3.3.*

[grapefruit](http://github.com/xav/Grapefruit)
: Working with colors in different color modes, creating gradients, etc.
