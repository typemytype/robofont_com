---
layout: page
title: Numbers and arithmetics
---

* Table of Contents
{:toc}

On this page we’ll look at some objects in Python which are used to represent numbers.


Integers and floating point numbers
-----------------------------------

The two main ones are: `int` (integers) and `float` (floating point numbers):

```python
>>> A = 12 # this is an integer
>>> type(A)
```

```plaintext
<type 'int'>
```

```python
>>> B = 12.5 # this is a float
>>> print(type(B))
```

```plaintext
<type 'float'>
```

The only difference in terms of notation is the use of a period for floating point numbers. A number is a `float` even if there is no digit after the period:

```python
>>> C = 12. # this is still a float
>>> type(C)
```

```plaintext
<type 'float'>
```


Arithmetic Operations
---------------------

### Addition

Adding numbers is easy and intuitive: simply use the `+` (plus) operator :

```python
>>> 1 + 1
```

```plaintext
2
```

```python
>>> 1 + 1 + 10
```

```plaintext
12
```

Integers can be added to integers, floats can be added to floats, and integers can be added to floats (and vice-versa):

```python
>>> 12 + 13 # result is an integer
```

```plaintext
25
```

```python
>>> 12 + 0.5 # result is a float
```

```plaintext
12.5
```

```python
>>> 0.5 + 12.5 # result is a float
```

```plaintext
13.0
```

- If both numbers are integers, the result is also an integer.
- If at least one of the numbers is a float, the result is a float.

### Subtraction

Subtracting numbers follows the same logic, just use the `-` (minus) operator:

```python
>>> 12 - 8
```

```plaintext
4
```

The minus sign is also used to indicate negative numbers:

```python
>>> a = -100
>>> a + 25
```

```plaintext
-75
```

### Multiplication

In Python, the multiplication operator is the `*` character, rather than `×` (multiplication sign) as usual in maths. But it works exactly the same way:

```python
>>> 12 * 8
```

```plaintext
96
```

```python
>>> 12 * -25
```

```plaintext
-300
```

### Division

Division in Python uses the `/` (forward slash) as its operator:

```python
>> 11 / 2
```

```plaintext
5.5
```

Division by zero is mathematically not possible and will always raise an error:

```python
>> 1 / 0
```

```plaintext
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ZeroDivisionError: division by zero
```

Integer division can be performed with the special operator `//`:

```python
>>> 11 // 2
```

```plaintext
5
```

Finally, use the `%` operator to get the rest of a division (modulo):

```python
>>> 11 % 2
```

```plaintext
1
```

### Exponentiation

To elevate a number to the power of another number, the operator `**` is used:

```python
2 ** 8
10 ** 2
2 ** 0.5
```

### Combining operations

Arithmetic operations can be combined into larger statements and calculations:

```python
7 + 10 - 100 * 3 / 200 ** 4
```

Python executes division and multiplication first, and addition and subtraction afterwards. If you want to add or subtract first, you must put these operations between parentheses – Python will execute operations between parentheses first:

```python
>>> 9 * 9 + 2
```

```plaintext
83
```

```python
>>> 9 * (9 + 2)
```

```plaintext
99
```

### Increment/decrement operators

Sometimes when writing code it is necessary to increment or decrement a value (to add or subtract a value from an integer):

```python
a = 10
# ... something happens here...
a = a + 2
```

In cases like this, it is possible to write the same line using `+=` to increase the value:

```python
a += 2 # same as: a = a + 2
```

...and `-=` to decrease it:

```python
b -= 1 # same as: b = b - 1
```

There is also a matching `*=` operator to multiply a value by itself:

```python
c *= 10 # same as: c = c * 10
```


More math
---------

Python can do many other kinds of mathematical calculations, of course. Many of these mathematical functions live in the [math] module. For example, trigonometric functions such as *sine*, *cosine*, *tangent*; constants such as *pi*, etc.

```python
>>> import math
>>> math.pi
```

```plaintext
3.14159265359
```

> - [Numeric Types](http://docs.python.org/3.9/library/stdtypes.html#numeric-types-int-float-complex)
{: .seealso }

[math]: http://docs.python.org/3.9/library/math.html
