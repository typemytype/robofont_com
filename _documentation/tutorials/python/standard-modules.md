---
layout: page
title: Standard modules
---

* Table of Contents
{:toc}

Python comes with *batteries included* – it has (almost) everything you need out-of-the-box. Distributions of Python include modules from the [Python Standard Library], which provide a wide range of tools for common problems in everyday programming – for example file system access, numerical and mathematical functions, unicode text processing, dealing with dates and calendars, reading / writing multiple data formats, serving web pages, generating random numbers, and a lot more.

This page shows selected examples of useful functions available in various standard modules. Follow the links to the official documentation of each module for more information.


os
--

The `os` module contains functions that deal with the file system.

```python
import os
```

os.listdir
: ^
  Lists the contents of a folder:

  ```python
  >>> os.listdir('/myFolder')
  ```

os.walk

: ^
  Recursively lists the contents of a folder and all its subfolders:

  ```python
  aFolder = '/myFolder'
  for path, folders, files in os.walk(aFolder):
      print(path)
      print(folders)
      print(files)
      print('-'*30)
  ```

os.getcwd
: ^
  Returns the folder where the current script is running (*current working directory*):

  ```python
  >>> os.getcwd()
  ```

os.mkdir
: ^
  Creates a new folder:

  ```python
  newFolder = "/Users/myUsername/Desktop/test"
  os.mkdir(newFolder)
  ```

os.makedirs
: ^
  Creates a folder and all parent folders in a given path:

  ```python
  newFolder2 = "/Users/myUsername/Desktop/test2/aSubfolder"
  os.makedirs(newFolder2)
  ```

os.rename
: ^
  Renames a folder or file:

  ```python
  oldFile = "/Users/myUsername/Desktop/image.png"
  newFile = "/Users/myUsername/Desktop/imageNew.png"
  os.rename(oldFile, newFile)
  ```

os.remove
: ^
  Removes a file:

  ```python
  filePath = "/Users/myUsername/Desktop/test3/image.png"
  os.remove(filePath)
  ```

os.rmdir
: ^
  Removes an empty folder:

  ```python
  os.rmdir(newFolderName)
  ```

  > To remove a folder and everything inside it, use `shutil.rmtree` (see [below](#rmtree)).
  {: .note }

> - [os — Miscellaneous operating system interfaces](http://docs.python.org/3.9/library/os.html)
{: .seealso }


shutil
------

The `shutil` module offers functions to copy and remove collections of files.

```python
import shutil
```

copytree
: ^
  Copies a folder and all its contents to a given path, creating missing parent folders.

  ```python
  srcFolder = "/Users/myUsername/Desktop/test1"
  dstFolder = "/Users/myUsername/Desktop/backup/test1"
  shutil.copytree(srcFolder, dstFolder)
  ```

rmtree
: ^
  Delete a folder and all its contents.

  ```python
  shutil.rmtree(srcFolder)
  ```

> - [shutil — High-level file operations](http://docs.python.org/3.9/library/shutil.html)
{: .seealso }


os.path
-------

The `os.path` module contains functions for manipulating file and folder names.

os.path.exists
: ^
  Checks if a file or folder exists, returns a bool:

  ```python
  >>> os.path.exists('/myFile.png')
  ```

os.path.basename
: ^
  Returns the file name for a given path, without the parent folder:

  ```python
  >>> filePath = "/Users/myUsername/Desktop/test3/image.png"
  >>> os.path.basename(filePath)
  ```

  ```plaintext
  image.png
  ```

os.path.dirname
: ^
  Returns the parent folder for a given file path:

  ```python
  os.path.dirname(filePath)
  ```

  ```plaintext
  /Users/myUsername/Desktop/test3
  ```

os.path.split
: ^
  Splits a given file path into parent folder and file name:

  ```python
  folder, fileName = os.path.split(filePath)
  ```

os.path.splitext
: ^
  Splits a file path into root and extension:

  ```python
  >>> os.path.splitext(filePath)
  ```

  ```plaintext
  ('/Users/myUsername/Desktop/test3/image', '.png')
  ```

> - [os.path — Common pathname manipulations](http://docs.python.org/3.9/library/os.path)
{: .seealso }


glob
----

The `glob` module finds all file or folder names matching a specified pattern. 

The example script below returns a list of all UFO files in a given folder:

```python
>>> import glob
>>> glob.glob('/aFolder/*.ufo')
```

> - [glob — Unix style pathname pattern expansion](http://docs.python.org/3.9/library/glob.html)
{: .seealso }


random
------

The `random` module provides functions to generate pseudo-random numbers, make random choices, etc.

```python
from random import random, randint, choice # ... etc.
```

random
: ^
  Returns a random `float` between `0.0` and `1.0`:

  ```python
  >>> for i in range(5):
  ...     random()
  ```

  ```plaintext
  0.1440220412035288
  0.6030806336013335
  0.23406747728785426
  0.12986516342850718
  0.5555918432876255
```

randint
: ^
  Returns a random `int` between two given integers:

  ```python
  >>> for i in range(5):
  ...     randint(10, 70)
  ```

  ```plaintext
  17
  59
  27
  40
  34
  ```

uniform
: ^
  Returns a random `float` between two given numbers:

  ```python
  >>> for i in range(5):
  ...     uniform(0.2, 12)
  ```
  
  ```plaintext
  9.377996146219418
  11.67729261822652
  9.499059365948389
  5.330554497768862
  8.164767529218107
  ```

choice
: ^
  Returns one randomly selected item from a given list.

  ```python
  >>> fruits = ['apple', 'orange', 'banana', 'lemon', 'strawberry']
  >>> for i in range(5):
  ...    choice(fruits)
  ```

  ```plaintext
  lemon
  banana
  strawberry
  orange
  orange
  ```

shuffle
: ^
  Shuffles the items of a given list in place.

  ```python
  for i in range(5):
      shuffle(fruits)
      print(fruits
  ```

  ```plaintext
  ['apple', 'banana', 'orange', 'lemon', 'strawberry']
  ['strawberry', 'lemon', 'banana', 'apple', 'orange']
  ['apple', 'lemon', 'strawberry', 'orange', 'banana']
  ['apple', 'banana', 'orange', 'lemon', 'strawberry']
  ['lemon', 'banana', 'apple', 'orange', 'strawberry']
  ```

seed
: ^
  Initializes the random number generator.

  ```python
  from random import random, seed
  seed(3)
  print(random())
  ```

  The result of this script will *always* be:

  ```plaintext
  0.32383276483316237
  ```

  > This makes it possible to reproduce random values generated with a script!
  {: .tip }

> - [random — Generate pseudo-random numbers](http://docs.python.org/3.9/library/random.html)
{: .seealso }


math
----

The `math` module provides several mathematical functions and constants.

```python
from math import *
```

sqrt
: ^
  Returns the square root of a number.

  ```python
  >>> sqrt(81)
  ```

  ```plaintext
  9.0
  ```

floor
: ^
  Returns the largest integer which is less than or equal to a given number.

  ```python
  >>> floor(4.80)
  ```

  ```plaintext
  4
  ```

ceil
: ^
  Returns the smallest integer which is greater than or equal to a given number.

  ```python
  >>> ceil(4.80)
  ```

  ```plaintext
  5
  ```

pi
: ^
  The mathematical constant π (pi).

  ```python
  >>> from math import pi
  >>> pi
  ```

  ```plaintext
  3.141592653589793
  ```

sin, cos, tan, etc.
: ^
  Return the *sine*, *cosine*, *tangent* etc. for a given angle.

  The DrawBot script below shows an example of *vector math*, using `cos` and `sin` to calculate new coordinates from an origin point, angle and distance.

  ```python
  from math import sin, cos

  x1, y1 = 100, 100
  distance = 800
  angle = 60

  x2 = x1 + distance * sin(radians(angle))
  y2 = y1 + distance * cos(radians(angle))

  stroke(1, 0, 0)
  line((x1, y1), (x2, y2))
  ```

  Here’s another example of vector math, using the trigonometric functions `hypot` (*hypothenuse*) and `atan2` (*arc tangent*) to calculate the distance and angle between two points.

  ```python
  from math import hypot, atan2

  x1, y1 = 100, 100
  x2, y2 = 500, 480

  distance = hypot(x2 - x1, y2 - y1)
  angle = degrees(atan2(y2 - y1, x2 - x1))
  ```

  > - use `radians()` to convert angles in degrees to radians
  > - use `degrees()` to convert angles in radians to degrees
  {: .note }

> - [math — Mathematical functions](http://docs.python.org/3.9/library/math.html)
{: .seealso }


datetime
--------

The `datetime` module supplies classes for manipulating dates and times.

```python
>>> import datetime
>>> now = datetime.datetime.now()
>>> now
```

```plaintext
2020-03-19 09:19:24.797088
```

Date objects support a `strftime(format)` method to create a string representation of time and date using *format codes*. For example:

```python
>>> now.strftime("%x %X")
>>> now.strftime("%d %B %Y %H:%M %A")
```

```plaintext
03/19/2020 09:19:24
19 March 2020 09:19 Thursday 079
```

See [strftime() and strptime() Behavior] for the full set of format codes available.

[strftime() and strptime() Behavior]: http://docs.python.org/3.9/library/datetime.html#strftime-strptime-behavior

> - [datetime — Basic date and time types](http://docs.python.org/3.9/library/datetime.html)
{: .seealso }


colorsys
--------

The `colorsys` module contains functions to convert color values between different color systems (RGB, YIQ, HLS, HSV).

The DrawBot script below uses `colorsys` to produce a range of hues.

```python
from colorsys import hsv_to_rgb

size(600, 120)

steps = 15
w = width() / steps
x = y = 0

for i in range(steps):
    hue = i * 1.0 / steps
    R, G, B = hsv_to_rgb(hue, 1.0, 1.0)
    fill(R, G, B)
    rect(x, y, w, height())
    x += w
```

{% image tutorials/python/colorsys-hue.png %}

> - [colorsys — Conversions between color systems](http://docs.python.org/3.9/library/colorsys.html)
{: .seealso }


zipfile
-------

The `zipfile` module provides tools to create, read, write, append, and list a [ZIP] file.

[ZIP]: http://en.wikipedia.org/wiki/Zip_(file_format)

Create a zip file from a given folder
: ^
  ```python
  import os
  from zipfile import ZipFile
  
  folder = 'someFolder'
  zipPath = os.path.join(os.path.dirname(folder), 'myZipFile.zip')
  
  with ZipFile(zipPath, 'w') as myZip:
      for root, dirs, files in os.walk(folder):
          for fileName in files:
              filePath = os.path.join(root, fileName)
              myZip.write(filePath, os.path.relpath(filePath, folder))
  ```

Expand the contents of a zip file into a folder
: ^
  ```python
  from zipfile import ZipFile 
  
  zipPath = 'myZipFile.zip'
  dstPath = '/someFolder'
  
  with ZipFile(zipPath, 'r') as myZip: 
      myZip.extractall(dstPath)
  ```

> - [zipfile — Working with ZIP archives](http://docs.python.org/3.9/library/zipfile.html)
{: .seealso }


subprocess
----------

The `subprocess` module allows you to run new processes, for example to execute command-line tools from a script.

The script below converts a VFB font to UFO by calling a locally installed [vfb2ufo](http://blog.fontlab.com/font-utility/vfb2ufo/).

```python
import subprocess

cmds = ['/usr/local/bin/vfb2ufo', '/aFolder/myFont.vfb']

p = subprocess.Popen(cmds, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)

output, errors = p.communicate()

print(output)
```

> - [subprocess — Subprocess management](http://docs.python.org/3.9/library/subprocess.html)
{: .seealso }


xml.etree.ElementTree
---------------------

The `xml.etree.ElementTree` module implements a simple API for parsing and creating [XML] data.

[XML]: https://en.wikipedia.org/wiki/XML

Parse an XML file
: ^
  ```python
  import xml.etree.ElementTree as ET
  xmlPath = '/aFolder/myFont.ufo/glyphs/a.glif'
  tree = ET.parse(xmlPath)
  ```

Get root tag and attributes
: ^
  ```python
  root = tree.getroot()
  print(root.tag, root.attrib)
  ```
  
  ```plaintext
  glyph {'name': 'a', 'format': '2'}
  ```

Get all children of an element
: ^
  ```python
  for child in root:
      print(child.tag, child.attrib)
  ```
  
  ```plaintext
  advance {'width': '475'}
  unicode {'hex': '0061'}
  outline {}
  lib {}
  ```

Find all children with a given tag
: ^
  ```python
  for elem in root.findall('outline'):
      print(elem.tag)
  ```

Iterate recursively over all sub-trees
: ^
  ```python
  for elem in root.iter('point'):
      print(elem.tag)
  ```

Find element and set a value for an attribute
: ^
  ```python
  root.find('advance').attrib['width'] = 700
  ```

Save changes to XML file
: ^
  ```python
  tree.write(xmlPath)
  ```

> - [xml.etree.ElementTree — The ElementTree XML API](http://docs.python.org/3.9/library/xml.etree.elementtree.html)
{: .seealso }


string
------

The `string` module contains some useful lists of characters:

```python
>>> import string
>>> string.ascii_lowercase
```

```plaintext
abcdefghijklmnopqrstuvwxyz
```

```python
>>> string.ascii_uppercase
```

```plaintext
ABCDEFGHIJKLMNOPQRSTUVWXYZ
```

```python
>>> string.digits
```

```plaintext
0123456789
```

```python
>>> string.punctuation
```

```plaintext
!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~
```

> - [string — Common string operations](http://docs.python.org/3.9/library/string.html)
{: .seealso }


json
----

The `json` module provides tools for reading and writing [JSON](http://json.org/) files.

json.loads
: ^
  Loads data from a JSON file into a dictionary.

  ```python
  import json

  jsonPath = 'somedata.json'

  with open(jsonPath, 'r', encoding='utf-8') as f:
      data = json.loads(f.read())

  print(data)
  ```

json.dumps
: ^
  Encodes a Python object hierarchy into JSON format.

  The example below also saves a `.json` file at a given path.

  ```python
  fontinfo = {
      'familyName' : 'My Font',
      'versionMajor' : 1,
      'versionMinor' : 3,
      'openTypeNameDesignerURL' : 'http://mywebsite.com/',
  }

  jsonPath = 'fontinfo.json'

  with open(jsonPath, 'w', encoding='utf-8') as f:
      data = json.dumps(fontinfo)
      f.write(data)
  ```

> - [json — JSON encoder and decoder](http://docs.python.org/3.9/library/json.html)
{: .seealso }


plistlib
--------

The `plistlib` module provides an interface for reading and writing the [property list] files used by macOS.

> Property lists are also used by the {% glossary UFO %} format to store various kinds of metadata, for example [fontinfo.plist], [kerning.plist], [groups.plist], etc.
{: .note }

[property list]: http://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/PropertyLists/Introduction/Introduction.html
[fontinfo.plist]: http://unifiedfontobject.org/versions/ufo3/fontinfo.plist/
[kerning.plist]: http://unifiedfontobject.org/versions/ufo3/kerning.plist/
[groups.plist]: http://unifiedfontobject.org/versions/ufo3/groups.plist/

plistlib.load
: ^
  Loads data from a `.plist` file into a dictionary.

  ```python
  import plistlib

  plistPath = 'somedata.plist'

  with open(plistPath, 'rb') as f:
      data = plistlib.load(f)

  print(data)
  ```

plistlib.dump
: ^
  Write an object to a `.plist` file.

  ```python
  fontinfo = {
    'familyName' : 'My Font',
    'versionMajor' : 1,
    'versionMinor' : 3,
    'openTypeNameDesignerURL' : 'http://mywebsite.com/',
  }

  plistPath = 'fontinfo.plist'

  with open(plistPath, 'wb') as f:
      plistlib.dump(fontinfo, f)
  ```

> - [plistlib — Generate and parse macOS .plist files](http://docs.python.org/3.9/library/plistlib.html)
{: .seealso }


difflib
-------

The `difflib` module provides classes and functions for comparing sequences. It can be used to show the differences between two versions of a text-based file – such as two `.glif` files, as in the example below.

```python
import difflib

glifPath1 = 'myFont1.ufo/glyphs/a.glif'
with open(glifPath1, 'r') as f:
    glifSrc1 = f.readlines()

glifPath2 = 'myFont2.ufo/glyphs/a.glif'
with open(glifPath2, 'r') as f:
    glifSrc2 = f.readlines()

uniDiff = difflib.unified_diff(glifSrc1, glifSrc2)
print(''.join(list(uniDiff)))
```

```plaintext
--- 
+++ 
@@ -1,18 +1,12 @@
 <?xml version="1.0" encoding="UTF-8"?>
 <glyph name="a" format="2">
-  <advance width="500"/>
+  <advance width="514.9054171954316"/>
   <outline>
     <contour>
       <point x="152.70780456852776" y="609.2342163705584" type="line"/>
       <point x="65.2954473350253" y="245.02359612944178" type="line"/>
-      <point x="334.9054171954316" y="151.91108819796978" type="line"/>
+      <point x="434.9054171954316" y="151.91108819796978" type="line"/>
       <point x="423.27093908629433" y="603.8090894670054" type="line"/>
     </contour>
   </outline>
-  <lib>
-    <dict>
-      <key>public.markColor</key>
-      <string>0,1,0,1</string>
-    </dict>
-  </lib>
 </glyph>
```

Several diff modes are available, including an HTML diff with colors.

```python
D = difflib.HtmlDiff()
html = D.make_file(glifSrc1, glifSrc2)
print(html)
```

{% image tutorials/python/difflib-html.png %}

> - [difflib — Helpers for computing deltas](http://docs.python.org/3.9/library/difflib.html)
{: .seealso }

You can use difflib to compute the differences between GLIF files. For example:

```python
import difflib
import vanilla
from mojo.UI import CodeEditor

glyph = CurrentGlyph()

before = glyph.dumpToGLIF()

for contour in glyph:
    contour.autoStartSegment()

after = glyph.dumpToGLIF()

D = difflib.Differ()
diff = D.compare(before.split('\n'), after.split('\n'))
diffText = '\n'.join(diff)

w = vanilla.Window((800, 400), minSize=(300, 400))
w.e = CodeEditor((0, 0, 0, 0), diffText, readOnly=True, lexer="xml")
w.open()
```

- - -


More standard modules
---------------------

The [Python Standard Library] includes many other modules – see the documentation for more information about each module.

- [itertools](http://docs.python.org/3/library/itertools.html)
- [csv](http://docs.python.org/3/library/csv.html)
- [doctest](http://docs.python.org/3/library/doctest.html)
- [unittest](http://docs.python.org/3/library/unittest.html)
- [ftplib](http://docs.python.org/3/library/ftplib.html)

> - [Brief Tour of the Standard Library]
{: .seealso }

[Brief Tour of the Standard Library]: http://docs.python.org/3.9/tutorial/stdlib.html
[Python Standard Library]: http://docs.python.org/3.9/library/index.html
