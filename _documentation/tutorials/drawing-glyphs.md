---
layout: page
title: Drawing glyphs
tags:
  - beginner
  - contours
  - designing
---

* Table of Contents
{:toc}

*This page gives an overview of the main methods for drawing and editing contours in RoboFont.*

Drawing contours
----------------

Use the {% internallink 'workspace/glyph-editor/tools/bezier-tool' %} to draw contours.

{% video tutorials/drawingGlyphs_bezier-tool.mp4 %}

Editing contours
----------------

Once you have placed contours with the Bezier Tool, you can use the {% internallink 'workspace/glyph-editor/tools/editing-tool' %} to move points and fix handles.

{% video tutorials/drawingGlyphs_edit-tool.mp4 %}

Use the [Scaling Edit Tool] to edit contours and maintain curve tension by scaling the handles – this is specially useful when modifying the weight of a glyph.

{% video tutorials/drawingGlyphs_scaling-edit-tool.mp4 %}

[Scaling Edit Tool]: https://github.com/klaavo/scalingEditTool


Copying and pasting
-------------------

You can copy-paste with keyboard shortcuts or the application menu (Edit > Copy, Edit > Paste). The pasted item will be placed in the same location as the source.

{% video tutorials/drawingGlyphs_copy-paste.mp4 %}

Cutting and slicing
-------------------

Use the {% internallink 'workspace/glyph-editor/tools/slice-tool' %} to add points or divide contours.

{% video tutorials/drawingGlyphs_slice.mp4 %}

Measuring
---------

Use the {% internallink 'workspace/glyph-editor/tools/measurement-tool' %} to measure distances between contours. You can place multiple measurements while pressing the ⌥ alt modifier.

{% video tutorials/drawingGlyphs_measurement-tool.mp4 %}

{% internallink 'workspace/glyph-editor/guidelines' %} can also show measurements. Just create a guideline and double click on the guideline to show the guideline attributes modal.

{% video tutorials/drawingGlyphs_measurement-tool-guideline.mp4 %}

Checking contour quality
------------------------

We have an entire article dealing with {% internallink 'checking-contour-quality' %}. "Check" 😅 it out!

Removing (or not removing) overlaps
-----------------------------------

To remove overlaps in the selected contours, click on the *Remove Overlap* button in the toolbar. If no contour is selected, all contours are used.

{% video tutorials/drawingGlyphs_remove-overlaps.mp4 %}

Adding overlaps
---------------

You can add overlaps by breaking the contour and adding the needed segments. Copy the original outline to the background as a trace.

{% video tutorials/drawingGlyphs_add-overlaps.mp4 %}

Or, you could use the [Add Overlaps](https://github.com/asaumierdemers/AddOverlap) extension.

Aligning
--------

Use the {% internallink  'glyph-editor/tools/editing-tool' %}’s cross-hair cursor to align points while moving a selection.

Turn on point coordinates in the *Display Options* menu, and use the keyboard to nudge contours to matching x/y position.

Use guidelines to snap the current selection into position.

The [Pop Up Tools] extension provides a dialog to align selected shapes in the current glyph, similar to align functions vector drawing apps.

[Pop Up Tools]: https://github.com/typesupply/popuptools

{% video tutorials/drawingGlyphs_align.mp4 %}


Transforming
------------

Use the {% internallink "/reference/workspace/glyph-editor/transform" text="Transform mode" %} to transform the selected glyph objects interactively. You can scale:

{% video tutorials/drawingGlyphs_transform.mp4 %}

or rotate (hold the ⌥ alt key):

{% video tutorials/drawingGlyphs_transform-rotate.mp4 %}

Use the transform Inspector for more options such as applying combining transformations, transforming several glyphs at once, etc.

The [Pop Up Tools] extension also offers options for transforming the current glyph.


Path operations
---------------

RoboFont supports boolean operations (addition, difference, intersection, xor) on outlines through the [booleanOperations](https://github.com/typemytype/booleanOperations) module. You can apply the boolean operations in the glyph editor using the [Pop Up Tools](https://github.com/typesupply/popuptools) extension (as in the video), or the [Path Operator](https://github.com/typemytype/pathOperatorRoboFontExtension) extension

{% video tutorials/drawingGlyphs_boolean-operations.mp4 %}

Drawing geometric shapes
------------------------

Turn on the grid in the {% internallink "/reference/workspace/glyph-editor/display-options" text="Display Options menu" %}. Set the grid size in the {% internallink 'preferences-window/glyph-view' %}.

- [Shape Tool](https://github.com/typemytype/shapeToolRoboFontExtension): draw ovals and rectangles
- [Pixel Tool](https://github.com/typemytype/pixelToolRoboFontExtension): draw with vector 'pixels' (also controls grid)
- [Symmetric Round Shape Tool](https://github.com/LettError/symmetricalRoundShapeDrawingTool): draw symmetrical round shapes

Digitising drawings
-------------------

To use an existing drawing as a base for your contours, import an {% internallink 'glyph-editor/image' text='Image' %} into the background of a glyph.

Use the [Tracer](https://github.com/typemytype/tracerRoboFontExtension) extension to trace images automatically.


Expanding strokes
-----------------

In RoboFont you can easily a stroke to a skeleton by using the [Outliner](https://github.com/typemytype/outlinerRoboFontExtension) extension

{% video tutorials/drawingGlyphs_outliner.mp4 %}
