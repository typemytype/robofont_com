---
layout: page
title: Introduction to spacing
tags:
  - spacing
---

* Table of Contents
{:toc}

*This page describes a general method for spacing glyphs.*


The basics
----------

Spacing is the process of adjusting the margins of each glyph, so that the white spaces *between* the glyphs and the white spaces *inside* the glyphs are visually balanced.

{% image how-tos/spacing-intro_outer-inner-shapes.png caption='the white inside and between glyphs' %}

The ultimate goal of spacing is to produce a pleasant text image for reading.

Spacing and drawing are usually done in parallel – the black shapes define the white, and the other way around.

In RoboFont, spacing is visualized and edited in the {% internallink 'workspace/space-center' %}.


Spacing vs. Kerning
-------------------

Spacing comes before {% glossary kerning %}. Kerning should be done later in the design process, once the spacing has been established. Kerning should *not* be used to fix problems caused by bad spacing.

> - {% internallink 'topics/kerning' %}
{: .seealso }

### Modularity in spacing

Black shapes are created by repeating certain elements (stems, bowls, arches, etc). The patterns of white shapes are formed around shapes with similar edges.

{% image how-tos/spacing-intro_groups-right.png caption='glyph with a similar right side' %}

{% image how-tos/spacing-intro_groups-left.png caption='glyph with a similar left side' %}


Spacing the lowercase
---------------------

### Define values for straight sides

Start by defining sidebearings for shapes with straight edges – this group is typically represented by the *n*.

Type in a string of *n*s, then adjust the values for left / right margins to create the base rhythm of your lowercase.

{% image how-tos/spacing-intro_nnn_.png %}

> The *Cut Off* display mode helps to visualize the space between and inside glyphs. You can edit the size of the Cut Off section using cmd ⌘ + drag
{: .tip }

### Define values for round sides

Based on the straight sides, you can now define the round sides using the *o*.

{% image how-tos/spacing-intro_o.png %}

### Transfer margins to other glyphs

After the margins for straight and round sides have been defined, you can move on and transfer these values to all other glyphs with a matching edge shape.

For example: take the left margin from  *n* and copy it to the left margin of all the other glyphs which have a straight left side (*m*, *h*, *l*, *b*, *p*, etc).

Here’s a visualisation of these relationships as a table:

<table>
  <tr>
    <td width='20%'></td>
    <th width='40%'>left margin</th>
    <th width='40%'>right margin</th>
  </tr>
  <tr>
    <th>straight</th>
    <td>n h m u l i j k b p r</td>
    <td>n h m u l i j</td>
  </tr>
  <tr>
    <th>round</th>
    <td>o c e d q</td>
    <td>o b p</td>
  </tr>
  <tr>
    <th>angled</th>
    <td>v w y x</td>
    <td>v w y x k</td>
  </tr>
</table>

Use the *pre* / *after* input fields to create the reference pattern for spacing.

{% image how-tos/spacing-intro_straights-lc.png caption='lowercase glyphs with straight sides' %}

You can type in expressions like `=n` into the input fields, and they will be converted into the actual numerical value equal to `n`.

> Depending on the design (for example a serif or italic type), you may need to turn on the *beam* to measure margins from the stems and not from the edge of the shape.
{: .tip }

The same is done for characters with round sides…

{% image how-tos/spacing-intro_rounds-lc.png %}

…and characters with angled sides:

{% image how-tos/spacing-intro_angled-lc.png %}

And finally, the remaining lowercase glyphs are adjusted visually to fit the rhythm of the other glyphs.

{% image how-tos/spacing-intro_lc-all.png %}


Spacing the uppercase
---------------------

The same process is used to define the margins for the uppercase glyphs.

<table>
  <tr>
    <td width='20%'></td>
    <th width='40%'>left margin</th>
    <th width='40%'>right margin</th>
  </tr>
  <tr>
    <th>straight</th>
    <td>H E F L I B D P R M N K</td>
    <td>H I J</td>
  </tr>
  <tr>
    <th>round</th>
    <td>O C G Q</td>
    <td>O D Q</td>
  </tr>
  <tr>
    <th>angled</th>
    <td>V W Y A X</td>
    <td>V W Y A X K</td>
  </tr>
</table>

We start by defining the margins for `H` (straight sides) and `O` (round sides)…

{% image how-tos/spacing-intro_uc.png %}

…then proceed to copy those values to all other glyphs with matching edge shapes, and finally take care of the remaining glyphs.

{% image how-tos/spacing-intro_uc-2.png %}

{% image how-tos/spacing-intro_uc-3.png %}


Other groups of glyphs
----------------------

When spacing numbers, we can use `0` as the base glyph.

{% image how-tos/spacing-intro_figures.png %}

- - -

> - Letters Of Credit – Walter Tracy
> - Counterpunch - Fred Smeijers
> - [Approaches to applying spacing methods…](http://www.fermello.org/FernandoMello_essay.pdf) – Fernando Mello
> - [Spacing a font, Part 1](http://www.societyoffonts.com/2018/09/19/spacing-a-font-part-1/) & [2](https://www.societyoffonts.com/2018/09/26/spacing-a-font-part-2/) – Society Of Fonts
{: .seealso }
