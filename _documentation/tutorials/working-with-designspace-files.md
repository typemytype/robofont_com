---
layout: page
title: Working with designspace files (and multiple ufos)
tags:
  - designspace
  - interpolation
  - extensions
---

* Table of Contents
{:toc}

RoboFont is a document based application built with the purpose of editing a single ufo file. But contemporary type design is mostly concerned with multi-source projects, so designers often need to work with many ufo documents at once tied in a designspace file. Here follows a list of extensions that make this kind of experience smoother. All these extensions are available through [Mechanic](https://robofontmechanic.com).

## DesignSpaceEditor

A design space document is nothing more that a XML document following a defined structure. You might even type it yourself in a code editor or generate it with Python code, but let's be honest, a good interface makes the editing way easier and less error-prone. The extension was completely rewritten for [v5](https://fonttools.readthedocs.io/en/latest/_images/v5_class_diagram.png) of the design space document specification (it supports discrete axes!).

> [GitHub repository](https://github.com/LettError/designSpaceRoboFontExtension)
{: .seealso }

## Batch

Batch answers the question «How do I generate a variable font in RoboFont?». Seamlessly integrated into the File application menu, it can generate variable fonts from design space documents, static fonts from opened ufos, webfonts and more. You can even activate the "Threaded" option to let it work in the background while you improve some spacing. The extension was completely rewritten for [v5](https://fonttools.readthedocs.io/en/latest/_images/v5_class_diagram.png) of the design space document specification (it supports discrete axes!).

> [GitHub repository](https://github.com/typemytype/batchRoboFontExtension/)
{: .seealso }

## Prepolator

Interpolation most important requirement is outlines compatibility. And that counts for variable fonts too. The best tool on the market to find and fix all the compatibility issues in your designspace is with no doubt Prepolator. You can get it on the RoboFont Extension Store and it's worth every penny.

> [Extension Store](https://extensionstore.robofont.com/extensions/prepolator/)
{: .seealso }

## Skateboard

Skateboard allows designers to see the effects of drawing on a design space immediately, right where it happens, in the RoboFont glyph editor. Check the tool's documentation for extra info and a complete list of features. The extension is available through the RoboFont Extension Store.

> - [Official documentation](https://superpolator.com/skateboard.html)
> - [Extension Store](https://extensionstore.robofont.com/extensions/skateboard/)
{: .seealso }

## EditThatNextMaster

You are deep into chiseling a beautiful serif of a no-better-identified glyph in a 8-sources type project when you realize that you need to make a very small edit in all occurrences of that glyph. This would normally mean a lot of clicks. Switch to another font, look for the glyph, open the glyph, do the edit. And then repeat 7 more times for all the other ufos. EditThatNextMaster makes switching between ufo documents as easy as a shortcut on your keyboard. And it even remembers windows locations!

> [GitHub repository](https://github.com/LettError/editThatNextMaster)
{: .seealso }

## Ground Control

This extension gives you a single place where you can look at all the opened ufos and typeset some text. It's basically a collection of space centers melted into a single window.

> [GitHub repository](https://github.com/roboDocs/GroundControl)
{: .seealso }
