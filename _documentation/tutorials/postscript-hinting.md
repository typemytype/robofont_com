---
layout: page
title: PostScript hinting
tags:
  - hinting
---

* Table of Contents
{:toc}

Hinting is the process to adjust the display of font outlines to fit the pixel matrix. At low screen resolutions, hinting is critical for producing accessible text. Postscript hinting parameters help the rasterizer to choose whether to suppress or not the rendering of subtle details, like rounds overshoots.

{% image tutorials/postscript-hinting-overshoots.png caption="Blue Zones displayed in the glyph editor" %}

Postscript fonts allow the setup of a few parameters that can improve the performance of the rasterizer:

+ The "**zones**" – magnetic horizontal bands
  + BlueValues
  + OtherValues
  + FamilyBlues
  + FamilyOtherBlues
+ BlueFuzz – sets tolerance in the zones
+ BlueScale – sets the threshold for rendering an overshoot
+ BlueShift – defines the minimum overshoot distance that can become visible
+ Vertical and horizontal stems

> The term "stems" does not follow the canonical typographical meaning. In hinting terminology, a stem could also be a round section. For example, a lowercase 'o' has two vertical stems and two horizontal stems.
{: .tip}

If you want to know more about setting these parameters for your fonts, this talk by Miguel Sousa at RoboThon is highly advised

> - [Postscript Hints][video] ([pdf]) – presentation by Miguel Sousa at RoboThon 2012
{: .seealso }

[video]: http://vimeo.com/38364880
[pdf]: http://typekit.files.wordpress.com/2012/04/postscript_hints_robothon_2012.pdf

> Consider that hints are discarded on Apple devices, so you'll need to check the outcome of the postscript hinting parameters on a Windows or Linux machine.
{: .tip }

These postscript hinting parameters can be set in the Postscript pane of the {% internallink "/reference/workspace/font-overview/font-info-sheet/postscript#hinting" text="font info sheet" %}.

{% image /reference/workspace/font-overview/font-info-sheet/font-info_postscript_hinting.png %}

If you need to read or write these parameters through the RoboFont API, you can refer to the [UFO3 specs](https://unifiedfontobject.org/versions/ufo3/fontinfo.plist/#postscript-specific-data) naming.

```python
myFont = CurrentFont()
myFont.info.postscriptBlueValues
myFont.info.postscriptOtherBlues
myFont.info.postscriptFamilyBlues
myFont.info.postscriptFamilyOtherBlues
myFont.info.postscriptStemSnapH
myFont.info.postscriptStemSnapV
myFont.info.postscriptBlueFuzz
myFont.info.postscriptBlueShift
myFont.info.postscriptBlueScale
myFont.info.postscriptForceBold
```

