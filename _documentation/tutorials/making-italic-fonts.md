---
layout: page
title: Making italic fonts
tags:
  - italics
slideShows:
  extensions:
    - image: tutorials/making-italic-fonts/1-nodisplay.png
      caption: |
        Display Italic Angle is inactive
    - image: tutorials/making-italic-fonts/5-alldisplay.png
      caption: |
        Display Italic Angle is active
  autoPlay: true
  height: 550
---

* Table of Contents
{:toc}

*This tutorial explains how to use RoboFont’s italic slant feature to draw italic and slanted typefaces.*


In current font formats, glyph shapes live inside rectangular boxes with vertical and horizontal edges. This is a problem when drawing italic or slanted typefaces, because the angle of the stems and the edges of the glyph box don’t align.

To improve this, modern font editors like RoboFont can display slanted sidebearings in order to make drawing italic typefaces more convenient.

This article describes how to work with Italic Slant in RoboFont, some of the issues which appear, and how to solve them.


Activating italic slant
-----------------------

Two settings are required in order to display slanted sidebearings in RoboFont:

1. In your font file, the font info must contain the correct italic angle of the font. This setting can be found in the {% internallink "workspace/font-overview/font-info-sheet" %} under *General > Dimensions*:

    {% image how-tos/working-with-italic-slant_fontinfo.png %}

    > - The default italic angle in a font is `0`, which corresponds to the 12 o’clock position.
    > - The italic angle’s value is counter-clockwise. So, if your font leans to the right (clockwise), the angle will be negative.
    {: .note }

2. In the {% internallink "workspace/preferences" %}, under *Glyphs > Display*, the option *Display Italic Angle* must be turned on:

    {% image how-tos/working-with-italic-slant_italic-preferences.png %}

    The *Slant Point Coordinates* option displays coordinates using the slanting value set in the {% internallink "reference/workspace/font-overview/font-info-sheet/general" text="Font Info > General Font Info" %}. The *Offset Point Coordinates* option displays the coordinates value applying the *Italic Slant Offset* set in the {% internallink "reference/workspace/font-overview/font-info-sheet/robofont" text="Font Info > RoboFont" %}.

    {% include slideshows items=page.slideShows.extensions %}

When these two conditions are met, the sidebearings of each glyph will appear slanted in the Font Window and in the Glyph Window.

As you’ll see, it’s a lot easier to draw slanted/italic fonts this way!


Issues with Italic Slant
------------------------

It’s important to remember that the slanted sibearings are just a display helper offered by RoboFont during the design process – the final OpenType font can only have rectangular boxes.

When Italic Slant is activated, the sidebearings in your font will be slanted from the baseline. Suppose that you have placed your italic shapes in the center of the slanted box, so that left and right sidebearings are balanced. Everything will appear to be fine:

{% image how-tos/working-with-italic-slant_offset-1.png caption="The glyph shape appears centered between slanted margins…" %}

However, when the font is generated, the glyph shape will not be centered in its rectangular box.

The problem becomes visible if you turn off the *Display Italic Angle* option in the Preferences. You can see how the shapes are not really centered between the vertical edges of each glyph box. This is what happens when you generate an OpenType font and use it in any application.

{% image how-tos/working-with-italic-slant_offset-2.png caption="…but it’s actually not centered in the ‘real’ glyph box." %}


Setting the Italic Slant Offset
-------------------------------

The solution to this problem is to set the *Italic Slant Offset*. This value is used to shift the glyphs horizontally when using the *Italic Slant* preview, so the glyphs are actually centered between their margins in the generated fonts.

**The Italic Slant Offset should be set before you start drawing**. If you have started drawing before setting this value, see how to add the offset to your font in the next section (below).

The Italic Slant Offset can be calculated from two other values in your font: the *italic angle* and the *x-height*. Here is the calculation in code:

{% showcode tutorials/calculateItalicSlantOffset.py %}

After you have calculated the Italic Slant Offset, you’ll need to store it in the font. Open the {% internallink "workspace/font-overview/font-info-sheet" %}, and find the appropriate setting under *RoboFont > Generate*:

{% image how-tos/working-with-italic-slant_fontinfo-offset.png %}

The Italic Slant Offset is stored in a custom entry in the `font.lib`. You can get and set this value with Python:

```python
f = CurrentFont()
print(f.lib["com.typemytype.robofont.italicSlantOffset"])
```

```plaintext
>>> 0
```

```python
f.lib["com.typemytype.robofont.italicSlantOffset"] = -40
print(f.lib["com.typemytype.robofont.italicSlantOffset"])
```

```plaintext
>>> -40
```


Applying the Italic Slant Offset after drawing
----------------------------------------------

If you started drawing your font with Italic Slant and *without* setting an Italic Slant Offset, all glyphs in your generated font will be off-center. To fix that after some drawing is done, you’ll need to:

1. calculate and set the Italic Slant Offset
2. move all glyphs to the correct centered position

The script below takes care of both steps for you:

{% showcode tutorials/fixItalicSlantOffset.py %}

> The script takes care of the foreground layer only – other layers in the font are not touched.
{: .note }

----

Based on original contribution by [Lukas Schneider](http://www.revolvertype.com).
