---
layout: page
title: Upgrading from Python 2 to Python 3
tags:
  - scripting
---

* Table of Contents
{:toc}

Among the {% internallink "robofont-3-changes" text="changes introduced by RoboFont 3" %} is the switch from Python 2 to Python 3.


Installing Python 3 on your system
----------------------------------

> If all your coding work is done inside RoboFont 3 and DrawBot, then **you don’t need to install anything else!** Both apps come with Python 3 embedded.
>
> *This section is intended only for developers who also wish to use Python 3 outside of RoboFont 3.*
{: .warning }

MacOS comes with Python 2.7 out-of-the-box. If you wish to use Python 3 outside of RoboFont, you’ll need to install it on your system first.

To install Python 3, go to [Python.org](http://python.org/), click on the *Downloads* menu, and use the button to download the installer for the latest version of Python 3.

{% image how-tos/upgrading-from-py2-to-py3_python-org.png %}

Once the download is finished, double-click the installer, and follow the instructions until the installation is completed.

{% image how-tos/upgrading-from-py2-to-py3_py3-installer.png %}

### Running Python 3 in Terminal

To run Python 3 in Terminal, use the `python3` command instead of just `python`:

```text
user:~ user$ python3
```

```plaintext
Python 3.6.4 (v3.6.4:d48ecebad5, Dec 18 2017, 21:07:28)
[GCC 4.2.1 (Apple Inc. build 5666) (dot 3)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

<!-- Welcome to Python 3…! -->

> If your work relies on external libraries, you’ll need to install them again for Python 3. See [Installing Python 3 packages](#installing-python-3-packages) (below).
{: .note }

> While it is possible to make Python 3 the default Python, that’s probably not a good idea: it may break some applications that depend on Python 2.
>
> For more info, see [How to set Python's default version to 3.3 on OS X?](http://stackoverflow.com/questions/18425379/how-to-set-pythons-default-version-to-3-3-on-os-x).
{: .warning }

### Running Python 3 in SublimeText

1. Go to Sublime Text to: Tools → Build System → New Build System 
and put the next lines in the editor:

```json
{
    "cmd": ["python3", "-i", "-u", "$file"],
    "file_regex": "^[ ]File \"(...?)\", line ([0-9]*)",
    "selector": "source.python"
}
```

Then save it with a meaningful name like: python3.sublime-build

2. Create a new .py file, save it on disk, and write the following code
```python
import sys
print(sys.version)
```

3. Go to Tools → Build system → and check python3 (or whatever name you assigned to the build system)
test it with:

4. Press: Cmd + B. You should read in the console something similar to

```plaintext
3.9.5 (default, May  4 2021, 03:36:27)
```

Adapted from this [gist](https://gist.github.com/zaemiel/4fbd8b5125fda7a140be)

Installing Python 3 packages
----------------------------

### System Python

To make external packages available to the system-level Python 3, put your modules/packages or `.pth` files here:

```text
/Library/Python/3.6/site-packages/
```

> This folder is not created automatically by the Python 3 installer. If it does not yet exist on your system, you’ll need to create it manually.
{: .note }

> A `.pth` file can contain a series of paths to be added to `sys.path`, one per line. You’ll need to create this file manually with a code editor.
{: .tip }


### RoboFont

RoboFont 3 adds `/Library/Python/3.6/site-packages` to `sys.path` on start-up, so all external packages installed for the system-level Python 3 will also be available in RoboFont 3.

If you wish to install external packages **for RoboFont 3 only**, put them in this folder instead:

```text
/Library/Application Support/RoboFont/external_packages/
```

Packages installed via this folder will *not* be available to the system-level Python 3.

> The `external_packages` folder also supports `.pth` files.
{: .tip }

> - {% internallink 'how-tos/overriding-embedded-libraries' %}
{: .seealso }
