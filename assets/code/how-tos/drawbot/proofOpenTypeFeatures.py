# get the current font
f = CurrentFont()
fontName = '%s-%s' % (f.info.familyName, f.info.styleName)
fontName = fontName.replace(' ', '')
print(fontName)

# install the font locally
# f.testInstall()

# check if the font is installed
if fontName not in installedFonts():
    print('font %s not installed' % fontName)

# get OpenType features in font
features = listOpenTypeFeatures(fontName=fontName)

# prepare variables UI
variables  = [{'name': "txtSize", 'ui': "Slider", 'args': {'value': 36, 'minValue': 16, 'maxValue': 64}}]
variables += [{'name': fea, 'ui': "CheckBox", 'args': {'value': value}} for fea, value in features.items()]

# create variables UI
V = Variable(variables, globals())

# create text object
T = FormattedString()
T.font(fontName)
T.fontSize(txtSize)

# get variables UI selection
selectedFeatures = {fea: globals()[fea] for fea in features}

# set text features
T.openTypeFeatures(**selectedFeatures)

# set text
T.append('''Placeat nobis nam doloribus facere nobis non. Libero qui molestiae incidunt omnis illo et sunt ullam. Expedita eaque nesciunt mollitia esse quia facere saepe modi. Dolor provident in dolorum. Dolor ea eum dolorum voluptas placeat sint. Ut odio tempore sunt accusamus excepturi amet voluptatem. Omnis ut debitis id qui omnis minima. Officia optio tempore doloribus unde nisi. Porro est expedita quos. Sit aperiam deleniti libero est saepe eum sit pariatur. Exercitationem ut sint ut et non. Eos neque ullam iusto qui excepturi dicta labore. Voluptas et aut et eius consectetur eum eum. Ex consequatur consequatur sed amet molestias magnam delectus nam. Adipisci dignissimos enim nobis blanditiis dolore et dolorum. Blanditiis dolorem voluptatem quasi aliquam impedit porro facere dolorum. Ut fuga earum incidunt praesentium recusandae nam optio est. Soluta velit dolore a rerum aperiam aspernatur occaecati sit. Vitae aperiam quia quidem id. Quia ea reiciendis ut impedit nulla vel qui enim. Id dicta occaecati accusamus consequatur. Quia in distinctio enim nobis.''')

# make sample
newPage('A4')
margin = 60
w = width() - margin * 2
h = height() - margin * 2
hyphenation(True)
textBox(T, (margin, margin, w, h))