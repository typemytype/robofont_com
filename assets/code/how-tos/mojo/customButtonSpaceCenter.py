import vanilla
from mojo.UI import CurrentSpaceCenter

def buttonCallback(sender):
    '''A function that does something when the button is clicked.'''
    print('button clicked!')


if __name__ == '__main__':
    # access the current space center
    w = CurrentSpaceCenter()

    # resize the top bar to create space for the new button
    w.top.setPosSize((0, 0, -100, 40))

    # add the button with the function above as callback
    w.myButton = vanilla.Button((-100, 10, -10, 22),
                                "click me",
                                callback=buttonCallback)
