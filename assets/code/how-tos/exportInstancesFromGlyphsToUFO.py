# MenuTitle: export instances to UFO
# -*- coding: utf-8 -*-

"""
Export all instances from the current Glyphs.app font to UFO.
It saves the UFO file in the same folder as the source files.

"""

import os
import subprocess

ufoExporter = Glyphs.objectWithClassName_("GlyphsFileFormatUFO")
font = Glyphs.font
fontFilePath = font.filepath

for instance in Glyphs.font.instances:
    if not instance.active:
        continue
    instanceFont = instance.interpolatedFont
    ufoExporter.setFontMaster_(instanceFont.masters[0])
    folder = os.path.dirname(font.filepath)
    ufoFilePath = os.path.join(folder, '%s-%s.ufo' % (font.familyName, instance.name))
    dest = NSURL.fileURLWithPath_(ufoFilePath)
    ufoExporter.writeUfo_toURL_error_(instanceFont, dest, None)

subprocess.Popen(['open', folder])