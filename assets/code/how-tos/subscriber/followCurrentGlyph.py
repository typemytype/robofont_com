from mojo.subscriber import Subscriber, registerRoboFontSubscriber

class Follower(Subscriber):

    def roboFontDidSwitchCurrentGlyph(self, info):
        print(info["glyph"])


if __name__ == '__main__':
    registerRoboFontSubscriber(Follower)
