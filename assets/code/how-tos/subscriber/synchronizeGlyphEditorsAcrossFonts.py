from mojo.subscriber import registerGlyphEditorSubscriber, Subscriber, disableSubscriberEvents
from mojo.UI import AllGlyphWindows


class GlyphEditorSubscriber(Subscriber):

    debug = True

    def glyphEditorDidSetGlyph(self, info):
        glyph = info["glyph"]
        glyphEditors = AllGlyphWindows()
        with disableSubscriberEvents():
            for eachEditor in glyphEditors:
                editorFont = eachEditor.getGlyph().font
                if editorFont.info.styleName != glyph.font.info.styleName and glyph.name in editorFont:
                    eachEditor.setGlyphByName(glyph.name)


if __name__ == '__main__':
    registerGlyphEditorSubscriber(GlyphEditorSubscriber)
