from vanilla import PopUpButton
from mojo.UI import CurrentGlyphWindow
from mojo.subscriber import Subscriber, registerRoboFontSubscriber


class CustomGlyphWindowDisplayMenu(Subscriber):

    debug = True

    title = 'hello world'
    items = {'A': True, 'B': True, 'C': False}
    width = 85

    @property
    def window(self):
        return CurrentGlyphWindow()

    @property
    def bar(self):
        if not self.window:
            return
        return self.window.getGlyphStatusBar()

    def glyphEditorDidOpen(self, info):
        if not self.bar:
            return

        if hasattr(self.bar, "menuButton"):
            del self.bar.menuButton

        menuItems = list(self.items.keys())
        menuItems.insert(0, self.title)
        self.bar.menuButton = PopUpButton((120, 0, self.width, 16),
                                          menuItems,
                                          sizeStyle="small",
                                          callback=self.menuButtonCallback)
        self.bar.menuButton.getNSPopUpButton().setPullsDown_(True)
        self.bar.menuButton.getNSPopUpButton().setBordered_(False)

        for i, menuItem in enumerate(self.bar.menuButton.getNSPopUpButton().itemArray()[1:]):
            value = list(self.items.values())[i-1]
            menuItem.setState_(value)

    def menuButtonCallback(self, sender):
        item = sender.getNSPopUpButton().selectedItem()
        item.setState_(not item.state())
        selection = []
        for i, menuItem in enumerate(self.bar.menuButton.getNSPopUpButton().itemArray()[1:]):
            if menuItem.state():
                selection.append(i)
        print(selection)


if __name__ == '__main__':
    registerRoboFontSubscriber(CustomGlyphWindowDisplayMenu)
