import os

# a folder containing UFO fonts
folder = '/myFolder'

# the output UFO version
outputVersion = 2

# iterate over all files in the folder
for f in os.listdir(folder):

    # ignore all files which are not UFOs
    if not os.path.splitext(f)[-1] == '.ufo':
        continue

    # open the UFO (without the UI)
    inputPath = os.path.join(folder, f)
    font = OpenFont(inputPath, showInterface=False)

    # save UFO version under a different name
    outputPath = inputPath.replace('.ufo', '_UFO%s.ufo' % outputVersion)
    font.save(path=outputPath, formatVersion=outputVersion)
    font.close()