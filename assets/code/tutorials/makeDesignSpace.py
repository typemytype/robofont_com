import os
from fontTools.designspaceLib import DesignSpaceDocument

root = os.getcwd()
doc = DesignSpaceDocument()

familyName = "MutatorMathTest"

#------
# axes
#------

doc.addAxisDescriptor(
    maximum=1000,
    minimum=0,
    default=0,
    name="width",
    tag="wdth",
)


doc.addAxisDescriptor(
    maximum = 1000,
    minimum = 0,
    default = 0,
    name = "weight",
    tag = "wght",
)

#---------
# masters
#---------

doc.addSourceDescriptor(
    path="MutatorSansLightCondensed.ufo",
    name="master.MutatorMathTest.LightCondensed.0",
    familyName=familyName,
    styleName="LightCondensed",
    location=dict(weight=0, width=0),
    copyLib=True,
    copyInfo=True,
    copyGroups=True,
    copyFeatures=True,
)

doc.addSourceDescriptor(
    path="MutatorSansBoldCondensed.ufo",
    name="master.MutatorMathTest.BoldCondensed.1",
    familyName=familyName,
    styleName="BoldCondensed",
    location=dict(weight=1000, width=0),
)

doc.addSourceDescriptor(
    path="MutatorSansLightWide.ufo",
    name="master.MutatorMathTest.LightWide.2",
    familyName=familyName,
    styleName="LightWide",
    location=dict(weight=0, width=1000),
)

doc.addSourceDescriptor(
    path="MutatorSansBoldWide.ufo",
    name="master.MutatorMathTest.BoldWide.3",
    familyName=familyName,
    styleName="BoldWide",
    location=dict(weight=1000, width=1000),
)

#-----------
# instances
#-----------

doc.addInstanceDescriptor(
    name='instance_LightCondensed',
    familyName=familyName,
    styleName="Medium",
    path=os.path.join(root, "instances", "MutatorSansTest-Medium.ufo"),
    location=dict(weight=500, width=327),
    kerning=True,
    info=True,
)

#-------
# rules
#-------

doc.addRuleDescriptor(
    name='fold_I_serifs',
    conditionSets = [[{'name': "width", 'minimum': 0, 'maximum': 328 }]],
    subs = [("I", "I.narrow"),],
)

doc.addRuleDescriptor(
    name='fold_S_terminals',
    conditionSets = [[
        {'name': "width",  'minimum': 0, 'maximum': 1000 },
        {'name': "weight", 'minimum': 0, 'maximum': 500  },
    ]],
    subs = [("S", "S.closed")]
)

#--------
# saving
#--------

path = os.path.join(root, "MutatorSans_test_.designspace")
doc.write(path)
