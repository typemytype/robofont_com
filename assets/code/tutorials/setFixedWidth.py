# get the current font
font = CurrentFont()

# iterate over all glyphs in the font
for glyph in font:

    # set glyph width
    glyph.width = 400