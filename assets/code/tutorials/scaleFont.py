font = CurrentFont()

newUpm = 100
oldUpm = font.info.unitsPerEm
factor = newUpm / oldUpm

for layer in font.layers:

    for glyph in layer:

        for contour in glyph:
            contour.scaleBy(factor)

        for anchor in glyph.anchors:
            anchor.scaleBy(factor)

        for guideline in glyph.guidelines:
            guideline.scaleBy(factor)

        glyph.width *= factor

font.kerning.scaleBy(factor)

for guideline in font.guidelines:
    guideline.scaleBy(factor)

for attr in ['unitsPerEm', 'descender', 'xHeight', 'capHeight', 'ascender']:
    oldValue = getattr(font.info, attr)
    newValue = oldValue * factor
    setattr(font.info, attr, newValue)

font.changed()