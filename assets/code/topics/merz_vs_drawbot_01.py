myGlyph = CurrentGlyph()

newPage(myGlyph.width, myGlyph.font.info.unitsPerEm)

translate(0, -myGlyph.font.info.descender)
drawGlyph(myGlyph)

stroke(1, 0, 0)
line((0, 0), (myGlyph.width, 0))