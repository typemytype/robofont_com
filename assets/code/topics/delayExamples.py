import ezui
from mojo.subscriber import Subscriber


class YourTool(Subscriber, ezui.WindowController):

    debug = True

    def build(self):
        content = """
        Observing stuff
        """
        self.w = ezui.EZWindow(
            title="AutoSaver",
            content=content,
            controller=self
        )

    def started(self):
        self.w.open()

    ## -- CALLBACK DELAYS EXAMPLES -- ##

    currentGlyphDidChangeDelay = 2   # seconds
    def currentGlyphDidChange(self, info):
        print('currentGlyphDidChange', info)

    fontLayersDidRemoveLayerDelay = 2   # seconds
    def fontLayersDidRemoveLayer(self, info):
        print('fontLayersDidRemoveLayer', info)

    spaceCenterDidChangeSelectionDelay = 2   # seconds
    def spaceCenterDidChangeSelection(self, info):
        print('spaceCenterDidChangeSelection', info)


if __name__ == '__main__':
    YourTool(currentGlyph=True)
