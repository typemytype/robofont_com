from mojo.subscriber import Subscriber, registerGlyphEditorSubscriber

YELLOW = (1, 1, 0, 0.4)
RED = (1, 0, 0, 0.4)

class Neighbours(Subscriber):

    debug = True

    def build(self):
        glyphEditor = self.getGlyphEditor()
        self.container = glyphEditor.extensionContainer(
            identifier="com.roboFont.NeighboursDemo.foreground",
            location="foreground",
            clear=True)
        self.leftPathLayer = self.container.appendPathSublayer(
            fillColor=YELLOW,
            name="leftNeighbour")
        self.rightPathLayer = self.container.appendPathSublayer(
            fillColor=RED,
            name="rightNeighbour")

    def destroy(self):
        self.container.clearSublayers()

    def glyphEditorDidSetGlyph(self, info):
        glyph = info["glyph"]
        if glyph is None:
            return
        self._updateNeighbours(glyph)

    def glyphEditorGlyphDidChange(self, info):
        glyph = info["glyph"]
        if glyph is None:
            return
        self._updateNeighbours(glyph)

    def _updateNeighbours(self, glyph):
        glyphPath = glyph.getRepresentation("merz.CGPath")
        self.leftPathLayer.setPath(glyphPath)
        self.leftPathLayer.setPosition((glyph.width, 0))
        self.rightPathLayer.setPath(glyphPath)
        self.rightPathLayer.setPosition((-glyph.width, 0))


if __name__ == '__main__':
    registerGlyphEditorSubscriber(Neighbours)
