from mojo.subscriber import WindowController, Subscriber
from mojo.roboFont import NewFont
import vanilla

class SillyTool(Subscriber, WindowController):

    def build(self):
        self.w = vanilla.Window((400, 300), "Silly Tool")
        self.font = NewFont()
        self.w.open()

    def destroy(self):
        self.font.close()


if __name__ == '__main__':
    SillyTool()