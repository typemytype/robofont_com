import ezui

class GlyphVisualizer:

    def __init__(self):

        myGlyph = CurrentGlyph()

        content = """
        * MerzView    @merzView
        """
        descriptionData = dict(
            merzView=dict(
                backgroundColor=(1, 1, 1, 1),
                delegate=self
            ),
        )

        self.w = ezui.EZWindow(
            title="Glyph Visualizer",
            content=content,
            size=(myGlyph.width, myGlyph.font.info.unitsPerEm),
            descriptionData=descriptionData,
            controller=self
        )

        merzView = self.w.getItem("merzView")
        container = merzView.getMerzContainer()

        # a layer for the glyph and the baseline
        groupLayer = container.appendBaseSublayer(
            size=(myGlyph.width, myGlyph.font.info.unitsPerEm),
            backgroundColor=(1, 1, 1, 1)
        )

        glyphLayer = groupLayer.appendPathSublayer(
            position=(0, -myGlyph.font.info.descender)
        )
        glyphPath = myGlyph.getRepresentation("merz.CGPath")
        glyphLayer.setPath(glyphPath)

        groupLayer.appendLineSublayer(
            startPoint=(0, -myGlyph.font.info.descender),
            endPoint=(myGlyph.width, -myGlyph.font.info.descender),
            strokeWidth=1,
            strokeColor=(1, 0, 0, 1)
        )

        self.w.open()


if __name__ == '__main__':
    GlyphVisualizer()
