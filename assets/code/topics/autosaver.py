import ezui
from mojo.subscriber import Subscriber
from datetime import datetime


class AutoSaver(Subscriber, ezui.WindowController):

    debug = True

    def build(self):
        content = """
        Autosaving... 💽
        """
        self.w = ezui.EZWindow(
            title="AutoSaver",
            content=content,
            controller=self
        )

    def started(self):
        self.w.open()

    currentGlyphDidChangeDelay = 30

    def currentGlyphDidChange(self, info):
        glyph = info['glyph']
        glyph.font.save()
        now = datetime.now()
        print(f'{now:%Y-%m-%d %H:%M:%S} – AUTOSAVED!')


if __name__ == '__main__':
    AutoSaver(currentGlyph=True)
