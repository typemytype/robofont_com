from mojo.subscriber import WindowController
from mojo.roboFont import NewFont
import vanilla

class SillyTool(WindowController):

    def build(self):
        self.w = vanilla.Window((400, 300), "Silly Tool")
        self.font = NewFont()
        self.w.open()

    def windowWillClose(self, sender):
        self.font.close()


if __name__ == '__main__':
    SillyTool()