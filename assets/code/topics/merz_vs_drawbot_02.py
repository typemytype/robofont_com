myGlyph = CurrentGlyph()

# a layer for the glyph and the baseline
groupLayer = container.appendBaseSublayer(
    size=(myGlyph.width, myGlyph.font.info.unitsPerEm)
)

glyphLayer = groupLayer.appendPathSublayer()
glyphPath = myGlyph.getRepresentation("merz.CGPath")
glyphLayer.setPath(glyphPath)

lineLayer = groupLayer.appendLineSublayer(
    startPoint=(0, 0),
    endPoint=(myGlyph.width, 0),
    strokeWidth=1,
    strokeColor=(1, 0, 0, 1)
)