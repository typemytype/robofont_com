import ezui
from mojo.subscriber import Subscriber, registerCurrentFontSubscriber


class BlurringLens(Subscriber, ezui.WindowController):

    debug = True

    def build(self):
        content = """
        * MerzView    @merzView
        """
        descriptionData = dict(
            merzView=dict(
                backgroundColor=(1, 1, 1, 1),
                delegate=self
            ),
        )

        self.w = ezui.EZWindow(
            title="Blurred Glass",
            content=content,
            size=(500, 200),
            minSize=(500, 200),
            descriptionData=descriptionData,
            controller=self
        )

    def started(self):
        self.w.open()

    def currentFontDidSetFont(self, info):
        font = info['font']

        # chooses the first three non-empty glyphs
        # according to glyph order
        glyphs = []
        for name in font.glyphOrder:
            glyph = font[name]
            if not len(glyph):
                continue
            glyphs.append(glyph)
            if len(glyphs) == 3:
                break

        self.font = font
        self.glyphs = glyphs
        self.setAdjunctObjectsToObserve(glyphs)
        self.adjunctGlyphDidChangeContours(None)

    def adjunctGlyphDidChangeContours(self, info):
        print("adjunct!")
        font = self.font
        glyphs = self.glyphs
        container = self.w.getItem("merzView").getMerzContainer()
        container.clearSublayers()
        if font is None:
            print("NO FONT!")
            return

        pointSize = 150
        scale = pointSize / font.info.unitsPerEm
        offset = 25 * (1.0 / scale)
        container.addSublayerScaleTransformation(scale)

        contents = [
            (glyphs[0], "left", offset),
            (glyphs[1], "center", 0),
            (glyphs[2], "right", -offset)
        ]

        for glyph, xPosition, offset in contents:
            xMin, yMin, xMax, yMax = glyph.bounds
            width = xMax - xMin
            height = yMax - yMin
            glyphContainer = container.appendBaseSublayer(
                position=(
                    dict(
                        point=xPosition,
                        offset=offset
                    ),
                    "center"
                ),
                size=(width, height)
            )
            glyphLayer = glyphContainer.appendPathSublayer()
            glyphLayer.appendFilter(
                dict(
                    name="glyphLayerFilter",
                    filterType="gaussianBlur",
                    radius=5)
            )
            glyphPath = glyph.getRepresentation("merz.CGPath")
            glyphLayer.setPath(glyphPath)


if __name__ == '__main__':
    registerCurrentFontSubscriber(BlurringLens)
