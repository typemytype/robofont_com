import ezui
from mojo.subscriber import Subscriber
from datetime import datetime

class MouseFollower(Subscriber, ezui.WindowController):

    debug = True

    def build(self):
        content = """
        Check the output window!
        """
        self.w = ezui.EZWindow(
            title="Mouse Follower",
            content=content,
            controller=self
        )

    def started(self):
        self.w.open()

    def currentGlyphDidChange(self, info):
        glyph = info['glyph']
        now = datetime.now()
        print(f'{now:%Y-%m-%d %H:%M:%S %f} – {glyph} did change!')


if __name__ == '__main__':
    MouseFollower(currentGlyph=True)
