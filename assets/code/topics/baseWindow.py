from mojo.subscriber import WindowController, Subscriber
import vanilla

class MyTool(Subscriber, WindowController):

    debug = True

    def build(self):
        self.w = vanilla.Window((600, 400), "My Window")
        self.w.button = vanilla.Button(
            (10, -30, 120, 20),
            "Show a message",
            callback=self.buttonCallback)
        self.w.open()

    def buttonCallback(self, sender):
        self.showMessage(messageText="WindowController",
                         informativeText="This window subclasses WindowController")


if __name__ == '__main__':
    MyTool()
