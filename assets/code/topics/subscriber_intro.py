import ezui
from mojo.subscriber import Subscriber, WindowController

BASE_2_ACCENTED = {
    'a': ('agrave', 'aacute', 'acircumflex', 'atilde'),
}

class MarginsListener(Subscriber, WindowController):
    debug = True

    def build(self):
        content = """
        I am listening!
        """
        self.w = ezui.EZWindow(
            title="Margins Listener",
            content=content,
            controller=self
        )

    def started(self):
        self.w.open()

    def currentGlyphMetricsDidChange(self, info):
        glyph = info['glyph']
        if glyph.name in BASE_2_ACCENTED:
            for eachGlyphName in BASE_2_ACCENTED[glyph.name]:
                glyph.font[eachGlyphName].leftMargin = glyph.leftMargin
                glyph.font[eachGlyphName].rightMargin = glyph.rightMargin


if __name__ == '__main__':
    MarginsListener(currentGlyph=True)
