---
layout: page
title: RoboHackathon DS5, December 2023
excludeNavigation: true
---

{% image events/robohackathon-ds5.mp4 %}

Get ready for another exciting RoboHackathon event, right before we wrap up the year! Mark your calendars for December 13th, from 7 to 10 pm CET.

With the release of designspace format v5, RoboFont has undergone some impressive changes behind the scenes, impacting both the core application and the extensions ecosystem. Now, it's time to unveil these updates to our community.

**Here's the program:**
* Erik van Blokland and Frederik Berlaen: DesignspaceEditor 2.0, UFOOperator, and Batch
* Connor Davenport: LERPing: Dungeons, Dragons, and Designspaces

And that's not all! Following the presentations, dive into a hands-on scripting lab led by Frederik Berlaen.

**[Zoom link](https://us06web.zoom.us/j/89775223485)**

See you on December 13th!

## Video

{% include embed vimeo=894275376 %}

> - [DesignspaceExtension 2.0](https://github.com/LettError/designSpaceRoboFontExtension)
> - [DSE2.0 Scripting Examples](https://github.com/LettError/designSpaceRoboFontExtension/tree/master/Scripting%20examples)
> - [Batch](https://github.com/typemytype/batchRoboFontExtension)
> - [UFOOperator](https://github.com/LettError/ufoProcessor/blob/master/Lib/ufoProcessor/ufoOperator.py)
{: .seealso}