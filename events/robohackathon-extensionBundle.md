---
layout: page
title: RoboHackathon Extension Bundle, March 2024
excludeNavigation: true
---

{% image events/robohackathon-extensionBundle.mp4 %}

After some exciting work on the RoboFont extensions ecosystem, we are ready to share a very important update with the community: the RoboFont ExtensionBundle goes open-source! In other words, this update will allow extension developers to harness the power of CI/CD routines to handle releases for their tools. Mark your calendars for March 18th, 2024, from 7 to 9 pm CET.

**Here's the program:**

* Frederik Berlaen: the reasons behind the update
* Roberto Arista: the new ExtensionBundle. What’s different and how to use it

Following the presentation, the RoboFont team will demo how to port your extensions to the new, suggested setup.

**[Zoom link](https://us06web.zoom.us/j/82026378729)**

See you on March 18th!

## Video

{% include embed vimeo=924763861 %}


> - [Extension Bundle](https://github.com/typemytype/extensionBundle)
> - [Extension Github Action](https://github.com/typemytype/roboFont-Extension-action)
{: .seealso}