---
excludeNavigation: true
excludeSearch: true
---

<html>
 <head>
     <meta charset="utf-8"/>
     <meta http-equiv="refresh" content="1;url=https://static.typemytype.com/robofont/RoboFont.dmg"/>
     <link rel="canonical" href="https://static.typemytype.com/robofont/RoboFont.dmg"/>
     <script type="text/javascript">
             window.location.href = "https://static.typemytype.com/robofont/RoboFont.dmg"
     </script>
     <title>Page Redirection</title>
 </head>
 <body>
 If you are not redirected automatically, follow <a href='https://static.typemytype.com/robofont/RoboFont.dmg'>this link</a>.
 </body>
 </html>
