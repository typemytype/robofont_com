import json
# use pip install pysftp
import pysftp
import os

try:
    # this module is ignored in git...
    from ftpSettings import *
except:
    ftpLogin = ftpPassword = None

if ftpLogin and ftpPassword:
    print("\textracting downloads...")

    ftpHost = "142.93.117.226"
    ftpPath = "/var/www/static.typemytype.com/robofont/versionHistory"

    with pysftp.Connection(host=ftpHost, username=ftpLogin, password=ftpPassword) as sftp:
        sftp.chdir(ftpPath)
        try:
            files = sftp.listdir()
        except Exception:
            files = []


    data = dict(release=[], beta=[])

    for fileName in reversed(sorted(files)):
        if fileName.endswith(".dmg"):
            parsed = fileName.replace(".dmg", "").replace("_beta", "")
            _, version, build = parsed.split("_")
            year = build[0:2]
            month = build[2:4]
            day = build[4:6]
            hour = build[6:8]
            minute = build[8:10]
            if version.endswith("b"):
                dest = data["beta"]
            else:
                dest = data["release"]
            dest.append(dict(
                build=build,
                date="20%s-%s-%s %s:%s" % (year, month, day, hour, minute),
                version=version,
                url = "https://static.typemytype.com/robofont/versionHistory/%s" % fileName,
            ))

    dest = os.path.join(os.path.dirname(__file__), "..", "_data", "downloads.json")
    with open(dest, "w") as f:
        f.write(json.dumps(data, sort_keys=True, indent=2))
    print("...done.\n")
