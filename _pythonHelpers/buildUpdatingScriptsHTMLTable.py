#!/usr/bin/env python3

# -- Constants -- #
header = """<table>
  <thead>
    <tr>
      <th width="70%">{tablename}</th>
      <th width="15%">RoboFont 3</th>
      <th width="15%">RoboFont 4</th>
    </tr>
  </thead>
  <tbody>"""

headerMechanic = """<table>
  <thead>
    <tr>
      <th>{tablename}</th>
      <th>developer</th>
      <th>observers</th>
      <th>drawingTools</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>"""

footer = """</tbody>
</table>
"""

# -- Objects, Functions, Procedures -- #
def formatRow(scriptName, beforeURL, afterURL):
    """
    <tr>
        <td>synchronizeSpaceCenters.py</td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/mojo/synchronizeSpaceCenters.py">↗</a></td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/a098bd67eb6076c57ded45599888554180c54616/assets/code/building-tools/mojo/synchronizeSpaceCenters.py">↗</a></td>
    </tr>
    """
    return f'    <tr>\n        <td>{scriptName}</td>\n        <td><a href="{beforeURL}">OLD</a></td>\n        <td><a href="{afterURL}">NEW</a></td>\n    </tr>'

def formatMechanicRow(name, url, developer, developerURL, observers, drawingTools, updated):
    """
    <tr>
        <td>RoboSomething</td>
        <td><a href="https://www.fredastaire.com/">Fred Astaire</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    """
    return f'    <tr>\n        <td><a href="{url}">{name}</a></td>\n        <td><a href="{developerURL}">{developer}</a></td>\n        <td>{"yes" if observers else "no"}</td>\n        <td>{"yes" if drawingTools else "no"}</td>\n        <td>{"yes" if updated else "no"}</td>\n    </tr>'


# -- Variables -- #
scripts = {
    "synchronizeSpaceCenters.py":
        ("https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/mojo/synchronizeSpaceCenters.py",
         "https://gitlab.com/typemytype/robofont_com/-/blob/a098bd67eb6076c57ded45599888554180c54616/assets/code/building-tools/mojo/synchronizeSpaceCenters.py"),

    "listLayersTool.py":
        ("https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/observers/listLayersTool.py",
         "https://gitlab.com/typemytype/robofont_com/-/blob/a098bd67eb6076c57ded45599888554180c54616/assets/code/building-tools/observers/listLayersTool.py"),

    "multiFontGlyphPreview.py":
        ("https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/observers/multiFontGlyphPreview.py",
         "https://gitlab.com/typemytype/robofont_com/-/blob/a098bd67eb6076c57ded45599888554180c54616/assets/code/building-tools/observers/multiFontGlyphPreview.py"),

    "openComponentInNewWindow.py":
        ("https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/observers/openComponentInNewWindow.py",
         "https://gitlab.com/typemytype/robofont_com/-/blob/a098bd67eb6076c57ded45599888554180c54616/assets/code/building-tools/observers/openComponentInNewWindow.py"),

    "pointDistance.py":
        ("https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/observers/pointDistance.py",
         "https://gitlab.com/typemytype/robofont_com/-/blob/a098bd67eb6076c57ded45599888554180c54616/assets/code/building-tools/observers/pointDistance.py"),

    "stencilPreview.py":
        ("https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/observers/stencilPreview.py",
         "https://gitlab.com/typemytype/robofont_com/-/blob/a098bd67eb6076c57ded45599888554180c54616/assets/code/building-tools/observers/stencilPreview.py"),

    "simpleFontWindow.py":
        ("https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/how-tos/simpleFontWindow.py",
         "https://gitlab.com/typemytype/robofont_com/-/blob/a098bd67eb6076c57ded45599888554180c54616/assets/code/how-tos/simpleFontWindow.py"),

    "drawReferenceGlyph.py":
        ("https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/observers/drawReferenceGlyph.py",
         "https://gitlab.com/typemytype/robofont_com/-/blob/a098bd67eb6076c57ded45599888554180c54616/assets/code/building-tools/observers/drawReferenceGlyph.py"),

    "addToolbarItem.py":
        ("https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/mojo/addToolbarItem.py",
         "https://gitlab.com/typemytype/robofont_com/-/blob/a098bd67eb6076c57ded45599888554180c54616/assets/code/building-tools/mojo/addToolbarItem.py"),

    "earMarkGlyph.py":
        ("https://github.com/okay-type/Markymark-roboFontExt/blob/67f208797579d940304b1d49670fcee7320a27e8/MarkyMark.roboFontExt/lib/mark-in-glyphview.py",
         "https://gist.github.com/okay-type/9111826ae6571d56905538c98d5de369"),

    "GlyphViewMetricsUI.py":
        ("https://github.com/okay-type/GlyphviewMetricsHUD-robofontExt/blob/d8c691909785fad5a61f09ecf0fc39a28e5a8a42/GlyphviewMetricsUI.roboFontExt/lib/glyphview-metrics-ui.py",
         "https://gist.github.com/okay-type/5b82a346996015f26e0e4f4d1a3cec85"),

    "showBody.py":
        ("https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/observers/showBody.py",
         "https://gitlab.com/typemytype/robofont_com/-/blob/7e5356a0607eff8c04201768e86994a3490b550e/assets/code/building-tools/observers/showBody.py"),

    "addGlyphEditorSubviewExample.py":
        ("https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/observers/addGlyphEditorSubviewExample.py",
         "https://gitlab.com/typemytype/robofont_com/-/blob/6e120385698eca5e1798e43f3a08e8ea1f717015/assets/code/building-tools/observers/addGlyphEditorSubviewExample.py"),

    "customToolActivator.py":
        ("https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/interactive/customToolActivator.py",
         "https://gitlab.com/typemytype/robofont_com/-/blob/6e120385698eca5e1798e43f3a08e8ea1f717015/assets/code/building-tools/interactive/customToolActivator.py"),

    "customGlyphWindowDisplayMenu.py":
        ("https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/observers/customGlyphWindowDisplayMenu.py",
         "https://gitlab.com/typemytype/robofont_com/-/blob/6e120385698eca5e1798e43f3a08e8ea1f717015/assets/code/building-tools/observers/customGlyphWindowDisplayMenu.py"),

    "customInspectorPanel.py":
        ("https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/observers/customInspectorPanel.py",
         "https://gitlab.com/typemytype/robofont_com/-/blob/6e120385698eca5e1798e43f3a08e8ea1f717015/assets/code/building-tools/observers/customInspectorPanel.py"),

    "simpleExampleTool.py":
        ("https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/interactive/simpleExampleTool.py",
         "https://gitlab.com/typemytype/robofont_com/-/blob/4508da4bb38c70c5921445aca065df382ded0323/assets/code/building-tools/interactive/simpleExampleTool.py"),

    "polygonSelection.py":
        ("https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/interactive/polygonSelection.py",
         "https://gitlab.com/typemytype/robofont_com/-/blob/e650e018db71d8a9105a75d4f45a44f9e8ee1eb4/assets/code/building-tools/interactive/polygonSelection.py"),

}

mechanicExtensionsStatus = {
    'Add Overlap': {
        'url': 'https://github.com/asaumierdemers/AddOverlap/',
        'developer': 'Alexandre Saumier Demers',
        'developerURL': 'http://asaumierdemers.com',
        'observers': True, 'drawingTools': False,
        'updated': True,
    },
    'Adjust Anchors': {
        'url': 'https://github.com/adobe-type-tools/adjust-anchors-rf-ext/',
        'developer': 'Adobe Type Tools',
        'developerURL': 'https://github.com/adobe-type-tools',
        'observers': True, 'drawingTools': True,
        'updated': False,
    },
    'Anchor Overlay Tool': {
        'url': 'https://github.com/jenskutilek/AnchorOverlayTool/',
        'developer': 'Jens Kutilek',
        'developerURL': 'http://www.kutilek.de',
        'observers': True, 'drawingTools': True,
        'updated': False,
    },
    'AngleRatioTool': {
        'url': 'https://github.com/LettError/angleRatioTool/',
        'developer': 'LettError',
        'developerURL': 'http://letterror.com',
        'observers': False, 'drawingTools': True,
        'updated': False,
    },
    'AutoSaviour': {
        'url': 'https://github.com/typedev/autoSaviour-RF/',
        'developer': 'Alexander Lubovenko',
        'developerURL': 'https://github.com/typedev',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'Black DesignRecorder': {
        'url': 'https://github.com/BlackFoundry/DesignRecorder',
        'developer': 'BlackFoundry',
        'developerURL': 'http://black-foundry.com',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'Blue Zone Editor': {
        'url': 'https://github.com/andyclymer/BlueZoneEditor-roboFontExt/',
        'developer': 'Andy Clymer',
        'developerURL': 'http://www.andyclymer.com',
        'observers': True, 'drawingTools': True,
        'updated': False,
    },
    'Broad Nib Background': {
        'url': 'https://github.com/asaumierdemers/BroadNibBackground/',
        'developer': 'Alexandre Saumier Demers',
        'developerURL': 'http://asaumierdemers.com',
        'observers': True, 'drawingTools': True,
        'updated': False,
    },
    'Check Parallel Tool': {
        'url': 'https://github.com/jtanadi/CheckParallelTool/',
        'developer': 'Jesen Tanadi',
        'developerURL': 'http://jesentanadi.com',
        'observers': True, 'drawingTools': True,
        'updated': False,
    },
    'ContoursLock': {
        'url': 'https://github.com/thomgb/ContoursLock/',
        'developer': 'Thom Janssen',
        'developerURL': 'http://www.geenbitter.nl',
        'observers': True, 'drawingTools': True,
        'updated': False,
    },
    'Curve Equalizer': {
        'url': 'https://github.com/jenskutilek/Curve-Equalizer',
        'developer': 'Jens Kutilek',
        'developerURL': 'http://www.kutilek.de',
        'observers': True, 'drawingTools': True,
        'updated': False,
    },
    'Delorean: Interpolation Preview': {
        'url': 'https://github.com/cjdunn/delorean',
        'developer': 'CJ Dunn',
        'developerURL': 'https://cjtype.com',
        'observers': True, 'drawingTools': True,
        'updated': True,
    },
    'DesignSpaceEditor': {
        'url': 'https://github.com/LettError/designSpaceRoboFontExtension/',
        'developer': 'LettError',
        'developerURL': 'http://letterror.com',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'DrawBot': {
        'url': 'https://github.com/typemytype/drawBotRoboFontExtension/',
        'developer': 'DrawBot',
        'developerURL': 'http://drawbot.com',
        'observers': True, 'drawingTools': True,
        'updated': False,
    },
    'Edit Font Dimensions': {
        'url': 'https://github.com/ryanbugden/Edit-Font-Dimensions/',
        'developer': 'Ryan Bugden',
        'developerURL': 'https://ryanbugden.com',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'EditThatNextMaster': {
        'url': 'https://github.com/LettError/editThatNextMasterRoboFontExtension/',
        'developer': 'LettError',
        'developerURL': 'http://letterror.com',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'EventObserver': {
        'url': 'https://github.com/roboDocs/eventObserverExtension/',
        'developer': 'Frederik Berlaen',
        'developerURL': 'http://typemytype.com',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'Eyeliner': {
        'url': 'https://github.com/ryanbugden/Eyeliner/',
        'developer': 'Ryan Bugden',
        'developerURL': 'https://ryanbugden.com',
        'observers': True, 'drawingTools': True,
        'updated': True,
    },
    'Feature Preview': {
        'url': 'https://github.com/typemytype/featurePreviewRoboFontExtension/',
        'developer': 'Frederik Berlaen',
        'developerURL': 'http://typemytype.com',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'Glif Viewer': {
        'url': 'https://github.com/typemytype/glifViewerRoboFontExtension/',
        'developer': 'Frederik Berlaen',
        'developerURL': 'http://typemytype.com',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'Glyph Nanny': {
        'url': 'https://github.com/typesupply/glyph-nanny',
        'developer': 'Tal Leming',
        'developerURL': 'http://tools.typesupply.com',
        'observers': True, 'drawingTools': True,
        'updated': True,
    },
    'GlyphConstruction': {
        'url': 'https://github.com/typemytype/GlyphConstruction/',
        'developer': 'Frederik Berlaen',
        'developerURL': 'http://typemytype.com',
        'observers': True, 'drawingTools': True,
        'updated': False,
    },
    'GlyphMorf': {
        'url': 'https://github.com/thomgb/glyphmorfExtension/',
        'developer': 'Thom Janssen',
        'developerURL': 'http://hallotype.nl/glyph-morf',
        'observers': True, 'drawingTools': True,
        'updated': False,
    },
    'GlyphPalette': {
        'url': 'https://github.com/rafalbuchner/glyph-palette/',
        'developer': 'Rafał Buchner',
        'developerURL': 'http://www.rafalbuchner.com',
        'observers': True, 'drawingTools': True,
        'updated': False,
    },
    'GlyphWalker': {
        'url': 'https://github.com/luke-snider/GlyphWalker/',
        'developer': 'Lukas Schneider',
        'developerURL': 'http://www.revolvertype.com',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'Goldener': {
        'url': 'https://github.com/typemytype/goldnerRoboFontExtension/',
        'developer': 'Frederik Berlaen',
        'developerURL': 'http://typemytype.com',
        'observers': True, 'drawingTools': True,
        'updated': False,
    },
    'Ground Control': {
        'url': 'https://github.com/roboDocs/GroundControl/',
        'developer': 'Loïc Sander',
        'developerURL': 'https://github.com/loicsander',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'Guide Tool': {
        'url': 'https://github.com/typesupply/guidetool/',
        'developer': 'Tal Leming',
        'developerURL': 'https://github.com/typesupply',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'InspectorMini': {
        'url': 'https://github.com/mrecord/InspectorMini/',
        'developer': 'Mark Record',
        'developerURL': 'http://commercialtype.com',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'Interpolation Matrix': {
        'url': 'https://github.com/roboDocs/interpolationMatrix/',
        'developer': 'Loïc Sander',
        'developerURL': 'https://github.com/loicsander',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'Interpolation Slider': {
        'url': 'https://github.com/andyclymer/InterpolationSlider-RoboFontExt/',
        'developer': 'Andy Clymer',
        'developerURL': 'http://www.andyclymer.com',
        'observers': True, 'drawingTools': True,
        'updated': False,
    },
    'Italic Bowtie': {
        'url': 'https://github.com/FontBureau/fbOpenTools/tree/master/ItalicBowtie',
        'developer': 'FontBureau',
        'developerURL': 'http://fontbureau.typenetwork.com',
        'observers': True, 'drawingTools': True,
        'updated': True,
    },
    'ItalicGuide': {
        'url': 'https://github.com/sansplomb/ItalicGuide/',
        'developer': 'Jérémie Hornus',
        'developerURL': 'http://black-foundry.com',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'Kern-A-Lytics': {
        'url': 'https://github.com/adobe-type-tools/kernalytics-rf-ext/',
        'developer': 'Adobe Type Tools',
        'developerURL': 'https://github.com/adobe-type-tools',
        'observers': False, 'drawingTools': True,
        'updated': False,
    },
    'KernTool3': {
        'url': 'https://github.com/typedev/KernTool3/',
        'developer': 'Alexander Lubovenko',
        'developerURL': 'https://github.com/typedev',
        'observers': True, 'drawingTools': True,
        'updated': False,
    },
    'KerningChecker': {
        'url': 'https://github.com/typemytype/kerningCheckerRoboFontExtension/',
        'developer': 'Frederik Berlaen',
        'developerURL': 'http://typemytype.com',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'Layer Preview': {
        'url': 'https://github.com/typemytype/layerPreviewRoboFontExtension/',
        'developer': 'Frederik Berlaen',
        'developerURL': 'http://typemytype.com',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'LightMeter': {
        'url': 'https://github.com/LettError/LightMeter/',
        'developer': 'LettError',
        'developerURL': 'http://letterror.com',
        'observers': False, 'drawingTools': True,
        'updated': False,
    },
    'Logger': {
        'url': 'https://github.com/typemytype/loggerRoboFontExtension/',
        'developer': 'Frederik Berlaen',
        'developerURL': 'http://typemytype.com',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'MM2SpaceCenter': {
        'url': 'https://github.com/cjdunn/MM2SpaceCenter/',
        'developer': 'CJ Dunn',
        'developerURL': 'http://cjtype.com',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'Mark Positioning': {
        'url': 'https:/gitlab.com/typoman/robofont-mark-tool/',
        'developer': 'Bahman Eslami',
        'developerURL': 'http://bahman.design',
        'observers': False, 'drawingTools': True,
        'updated': False,
    },
    'Marky Mark': {
        'url': 'https://github.com/okay-type/Markymark-roboFontExt/',
        'developer': 'Jackson Showalter-Cavanaugh',
        'developerURL': 'http://www.okaytype.com',
        'observers': True, 'drawingTools': True,
        'updated': False,
    },
    'Mechanic': {
        'url': 'https://github.com/robofont-mechanic/mechanic-2/',
        'developer': 'RoboFont Mechanic',
        'developerURL': 'http://robofontmechanic.com',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'Outliner': {
        'url': 'https://github.com/typemytype/outlinerRoboFontExtension/',
        'developer': 'Frederik Berlaen',
        'developerURL': 'http://typemytype.com',
        'observers': True, 'drawingTools': False,
        'updated': True,
    },
    'PDFButtonSpace': {
        'url': 'https://github.com/thomgb/PDFButtonSpace/',
        'developer': 'Thom Janssen',
        'developerURL': 'http://www.hallotype.nl',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'Paste Special': {
        'url': 'https:/gitlab.com/typoman/robofont-special-clipboard/',
        'developer': 'Bahman Eslami',
        'developerURL': 'http://bahman.design',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'PenBallWizard': {
        'url': 'https://github.com/roboDocs/PenBallWizard/',
        'developer': 'Loïc Sander',
        'developerURL': 'https://github.com/loicsander',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'Plum': {
        'url': 'https://github.com/jackjennings/Plum/',
        'developer': 'Jack Jennings',
        'developerURL': 'ttp://ja.ckjennin.gs',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'Pop Up Tools': {
        'url': 'https://github.com/typesupply/popuptools/',
        'developer': 'Tal Leming',
        'developerURL': 'https://github.com/typesupply',
        'observers': True, 'drawingTools': True,
        'updated': False,
    },
    'Properties': {
        'url': 'https://github.com/BlackFoundry/Properties/',
        'developer': 'Jérémie Hornus',
        'developerURL': 'http://black-foundry.com',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'QuadraticConverter': {
        'url': 'https://github.com/sansplomb/QuadraticConverter/',
        'developer': 'Jérémie Hornus',
        'developerURL': 'http://www.typosansplomb.com/',
        'observers': True, 'drawingTools': True,
        'updated': False,
    },
    'Ramsay St.': {
        'url': 'https://github.com/typemytype/ramsayStreetRoboFontExtension/',
        'developer': 'Frederik Berlaen',
        'developerURL': 'http://typemytype.com',
        'observers': True, 'drawingTools': True,
        'updated': False,
    },
    'Red Arrow': {
        'url': 'https://github.com/jenskutilek/RedArrow/',
        'developer': 'Jens Kutilek',
        'developerURL': 'http://www.kutilek.de',
        'observers': True, 'drawingTools': True,
        'updated': False,
    },
    'RoboChrome': {
        'url': 'https://github.com/jenskutilek/RoboChrome/',
        'developer': 'Jens Kutilek',
        'developerURL': 'https://github.com/jenskutilek',
        'observers': True, 'drawingTools': True,
        'updated': False,
    },
    'RoboFontProject': {
        'url': 'https://github.com/typemytype/projectRoboFontExtension/',
        'developer': 'Frederik Berlaen',
        'developerURL': 'http://typemytype.com',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'RoboLasso': {
        'url': 'https://github.com/productiontype/RoboLasso/',
        'developer': 'Frederik Berlaen',
        'developerURL': 'http://typemytype.com',
        'observers': False, 'drawingTools': True,
        'updated': False,
    },
    'RoboREPL': {
        'url': 'https://github.com/typesupply/roborepl/',
        'developer': 'Tal Leming',
        'developerURL': 'http://typesupply.com',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'RoboToDo': {
        'url': 'https://github.com/jackjennings/RoboToDo/',
        'developer': 'Jack Jennings',
        'developerURL': 'http://ja.ckjennin.gs',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'Rotator': {
        'url': 'https://github.com/frankrolf/Rotator/',
        'developer': 'Frank Grießhammer',
        'developerURL': 'http://www.frgr.de',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'Scale, absolutely!': {
        'url': 'https://github.com/jansindl3r/Scale-absolutely/',
        'developer': 'Jan Šindler',
        'developerURL': 'https://www.futurefonts.xyz/jan-sindler',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'Shape Tool': {
        'url': 'https://github.com/typemytype/shapeToolRoboFontExtension',
        'developer': 'Frederik Berlaen',
        'developerURL': 'http://typemytype.com/',
        'observers': True, 'drawingTools': True,
        'updated': True,
    },
    'ShowSparks': {
        'url': 'https://github.com/LettError/showSparks/',
        'developer': 'LettError',
        'developerURL': 'http://letterror.com',
        'observers': False, 'drawingTools': True,
        'updated': False,
    },
    'Sidebear': {
        'url': 'https://github.com/ryanbugden/Sidebear/',
        'developer': 'Ryan Bugden',
        'developerURL': 'https://ryanbugden.com',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'Simple Kerning': {
        'url': 'https:/gitlab.com/typoman/robofont-kerning-tool/',
        'developer': 'Bahman Eslami',
        'developerURL': 'http://bahman.design',
        'observers': True, 'drawingTools': True,
        'updated': False,
    },
    'Slanter': {
        'url': 'https://github.com/typemytype/slanterRoboFontExtension/',
        'developer': 'Frederik Berlaen',
        'developerURL': 'http://typemytype.com',
        'observers': True, 'drawingTools': True,
        'updated': False,
    },
    'Speed Punk': {
        'url': 'https://github.com/yanone/speedpunk/',
        'developer': 'Yanone',
        'developerURL': 'https://yanone.de',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'SuperpolatorRemote': {
        'url': 'https://github.com/LettError/SuperpolatorRemote/',
        'developer': 'LettError',
        'developerURL': 'http://letterror.com',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'SymmetricalRoundShapeDrawingTool': {
        'url': 'https://github.com/LettError/symmetricalRoundShapeDrawingTool/',
        'developer': 'LettError',
        'developerURL': 'http://letterror.com',
        'observers': False, 'drawingTools': True,
        'updated': True,
    },
    'Theme Manager': {
        'url': 'https://github.com/connordavenport/Theme-Manager/',
        'developer': 'Connor Davenport and Andy Clymer',
        'developerURL': 'http://www.connordavenport.com',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'ToolManager': {
        'url': 'https://github.com/RafalBuchner/tool-manager/',
        'developer': 'Rafał Buchner',
        'developerURL': 'http://www.rafalbuchner.com',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'Tracer': {
        'url': 'https://github.com/typemytype/tracerRoboFontExtension/',
        'developer': 'Frederik Berlaen',
        'developerURL': 'http://typemytype.com',
        'observers': False, 'drawingTools': True,
        'updated': False,
    },
    'Underline Strikethrough': {
        'url': 'https://github.com/Typefounding/underlineStrikethrough/',
        'developer': 'Ben Kiel',
        'developerURL': 'http://www.typefounding.com',
        'observers': True, 'drawingTools': True,
        'updated': False,
    },
    'Unicode Info': {
        'url': 'https://github.com/jenskutilek/RFUnicodeInfo/',
        'developer': 'Jens Kutilek',
        'developerURL': 'http://www.kutilek.de/',
        'observers': True, 'drawingTools': False,
        'updated': True,
    },
    'WurstSchreiber': {
        'url': 'https://github.com/asaumierdemers/WurstSchreiber/',
        'developer': 'Alexandre Saumier Demers',
        'developerURL': 'http://asaumierdemers.com',
        'observers': True, 'drawingTools': True,
        'updated': True,
    },
    'ZoneChecker': {
        'url': 'https://github.com/LettError/zoneChecker/',
        'developer': 'LettError',
        'developerURL': 'http://letterror.com',
        'observers': False, 'drawingTools': True,
        'updated': False,
    },
    'showDist': {
        'url': 'https://github.com/frankrolf/showDist/',
        'developer': 'Frank Grießhammer',
        'developerURL': 'http://www.frgr.de',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
    'word-o-mat': {
        'url': 'https://github.com/ninastoessinger/word-o-mat/',
        'developer': 'Nina Stössinger',
        'developerURL': 'http://www.typologic.nl',
        'observers': True, 'drawingTools': False,
        'updated': False,
    },
}


filePath = "../_documentation/how-tos/updating-scripts-from-RF3-to-RF4.md"

# -- Instructions -- #
if __name__ == '__main__':
    newTableRows = ['<!-- START COMPARISON TABLES -->', header.format(tablename='scripts')]
    for scriptName, (beforeURL, afterURL) in scripts.items():
        newTableRows.append(formatRow(scriptName, beforeURL, afterURL))
    newTableRows.append(footer)

    with open(filePath, mode='r', encoding='utf-8') as mdFile:
        mdLines = [ll.rstrip() for ll in mdFile.readlines()]

    newLines = []
    copying = True
    for eachMdLine in mdLines:
        if '<!-- START COMPARISON TABLES -->' in eachMdLine:
            copying = False
        elif '<!-- END COMPARISON TABLES -->' in eachMdLine:
            newLines.extend(newTableRows)
            copying = True

        if copying:
            newLines.append(eachMdLine)

    newTxt = '\n'.join(newLines)
    with open(filePath, mode='w', encoding='utf-8') as mdFile:
        mdFile.write(newTxt)

    # MECHANIC TABLE #
    # AWFUL DUPLICATION, BUT CAN'T REFACTOR THE ENTIRE THING #
    with open(filePath, mode='r', encoding='utf-8') as mdFile:
        mdLines = [ll.rstrip() for ll in mdFile.readlines()]

    mechanicTable = ['<!-- START MECHANIC TABLE -->', headerMechanic.format(tablename='extension')]
    for name, extension in mechanicExtensionsStatus.items():
        mechanicTable.append(formatMechanicRow(name, **extension))
    mechanicTable.append(footer)

    newLines = []
    copying = True
    for eachMdLine in mdLines:
        if '<!-- START MECHANIC TABLE -->' in eachMdLine:
            copying = False
        elif '<!-- END MECHANIC TABLE -->' in eachMdLine:
            newLines.extend(mechanicTable)
            copying = True

        if copying:
            newLines.append(eachMdLine)

    newTxt = '\n'.join(newLines)
    with open(filePath, mode='w', encoding='utf-8') as mdFile:
        mdFile.write(newTxt)
