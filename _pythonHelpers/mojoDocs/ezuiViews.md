An overview of all the custom RoboFont views available in [`ezui`](https://typesupply.github.io/ezui/index.html).

RoboFont extends `ezui` by adding some custom views like `GlyphPreview`, `GlyphSequence`, `CodeEditor`, `GlyphLineView`. To use any of these views, you need to type an asterisk followed by the object name and then provide an identifier prefixed by `@` for callbacks, something like:
```python
"""
* GlyphPreview @preview
"""
```

You can pass the arguments for the object initiation into the `descriptionData` dictionary. Check the following examples for some references.

## `GlyphPreview`

![GlyphPreview](../../../../../../images/API/mojo/ezuiViews/GlyphPreview.png)

Please, open a font and select a glyph before running this example.

```python
import ezui

class Controller(ezui.WindowController):

    def build(self):
        content = """
        * GlyphPreview @preview
        """
        descriptionData = dict(
            preview=dict(
                height=300,
                width=300,
                glyph=CurrentGlyph()
                )
            )

        font = CurrentFont()
        self.w = ezui.EZWindow(
            title=f"{font.info.familyName} {font.info.styleName}",
            content=content,
            descriptionData=descriptionData,
            size="auto",
            controller=self
        )
    
    def started(self):
        self.w.open()


Controller()
```

## `GlyphSequence`

![GlyphSequence](../../../../../../images/API/mojo/ezuiViews/GlyphSequence.png)

Please, open a font before running this example.

```python
import ezui

class Controller(ezui.WindowController):

    def build(self):
        font = CurrentFont()

        content = """
        * GlyphSequence @sequence
        """
        descriptionData = dict(
            sequence=dict(
                height=22,
                width=200,
                font=font,
                )
            )
        self.w = ezui.EZWindow(
            title=f"{font.info.familyName} {font.info.styleName}",
            content=content,
            descriptionData=descriptionData,
            size="auto",
            controller=self
        )
    
    def started(self):
        self.w.open()
    
    def sequenceCallback(self, sender):
        print(sender.get())

Controller()
```

## `CodeEditor`

![CodeEditor](../../../../../../images/API/mojo/ezuiViews/CodeEditor.png)

```python
import ezui

class Controller(ezui.WindowController):

    def build(self):
        content = """
        * CodeEditor @editor
        """
        descriptionData = dict(
            editor=dict(
                height=300,
                width=400,
                text='print("Hello World")',
                usesTabs=False,
                indentSize=4,
                showLineNumbers=True,
                )
            )
        self.w = ezui.EZWindow(
            title="Code Editor",
            content=content,
            descriptionData=descriptionData,
            size="auto",
            controller=self
        )
    
    def started(self):
        self.w.open()


Controller()
```

## `GlyphLineView`

![GlyphLineView](../../../../../../images/API/mojo/ezuiViews/GlyphLineView.png)

To run this example, you'll need an open font with at least `H`, `e`, `l` and `o`.

```python
import ezui

class Controller(ezui.WindowController):

    def build(self):
        content = """
        * GlyphLineView @lineView
        """
        font=CurrentFont()
        descriptionData = dict(
            lineView=dict(
                width=600,
                height=250,
                font=font,
                glyphs=[font[c] for c in "Hello"],
                )
            )
        self.w = ezui.EZWindow(
            title="Glyph Line View",
            content=content,
            descriptionData=descriptionData,
            size="auto",
            controller=self
        )

    def started(self):
        self.w.open()


Controller()
```

