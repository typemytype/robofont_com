"""
Moves unused assets (images & python scripts) to _unused folders

"""

# -- Modules -- #
from pathlib import Path
import shutil


# -- Constants -- #
DOCS_FOLDER = Path('../_documentation')
IMAGES_FOLDER = Path('../images')
SCRIPTS_FOLDER = Path('../assets')


# -- Objects, Functions, Procedures -- #
def loadAllTexts():
    txts = []
    for eachPath in DOCS_FOLDER.glob('**/*.md'):
        with open(eachPath, mode='r', encoding='utf-8') as mdFile:
            txts.append(mdFile.read())
    return '\n'.join(txts)

def retrievePaths(folder, extensions):
    return [pp for pp in folder.glob("**/*") if pp.suffix in extensions]

def moveTo(files, refTxt, destination):
    unusedFolder = Path(destination)
    for eachFile in files:
        if eachFile.name not in refTxt:
            shutil.move(eachFile, unusedFolder / eachFile.name)

def moveUnused():
    imgs = retrievePaths(IMAGES_FOLDER, set([".jpg", ".png"]))
    scripts = retrievePaths(SCRIPTS_FOLDER, set([".py"]))
    txts = loadAllTexts()

    moveTo(imgs, txts, "../images/_unused")
    moveTo(scripts, txts, "../assets/code/_unused")


if __name__ == '__main__':
    moveUnused()
