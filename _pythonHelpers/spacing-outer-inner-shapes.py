txt = 'noncnan'
f = CurrentFont()
print(f)

c0 = 0, 0.4, 1
c1 = 1, 0, 0.9

size(960, 200)
translate(50, 50)
scale(0.2)

for i, char in enumerate(txt):
    g = f[char]
    g1 = f[char].getLayer('inside')
    g2 = f[char].getLayer('outside')
    fill(*c0)
    stroke(*c0)
    drawGlyph(g1)
    fill(*c1)
    stroke(*c1)
    drawGlyph(g2)
    fill(1)
    stroke(1)
    drawGlyph(g)
    if i != 0:
        with savedState():
            stroke(1)
            strokeWidth(5)
            lineDash(20, 10)
            line((0, 0), (0, 500))

    translate(g1.width, 0)

saveImage('outer-inner-shapes.png')