# menuTitle: make base46 webfonts

import os
from base64 import b64encode
from fontTools import subset

"""
Generate webfonts for use on the robofont.com website

- obfuscate font names
- add license note
- save as woff
- convert to base64
- make @fontface rule
- save to scss file

"""

folder   = '/_code/roboType/binaries'
scssPath = '/_code/robofont_com/_sass/_fonts.scss'

styles = [
    'Roman', 'Italic', 'Bold', 'Bold Italic',
    'Mono',
    'Narrow Bold', 'Narrow Bold Italic',
    'Icons',
]

css = '''\
//
// FONTS
//

'''

for style in styles:
    # get source ttf webfont
    fileName = f'RoboType-{style}_web.ttf'.replace(' ', '_')
    ttfPath  = os.path.join(folder, fileName)
    woffPath = ttfPath.replace('.ttf', '.woff')

    # obfuscate font names
    options = subset.Options()
    options.obfuscate_names = True
    font = subset.load_font(ttfPath, options)
    subsetter = subset.Subsetter(options)
    subsetter.populate(glyphs=font.getGlyphOrder())
    subsetter.subset(font)

    # add license note
    font['name'].setName("for exclusive use on robofont.com", 13, 3, 1, 0x409) # Windows English

    # save as woff
    font.flavor = 'woff'
    if os.path.exists(woffPath):
        os.remove(woffPath)
    subset.save_font(font, woffPath, options)
    font.close()

    # convert to base64
    with open(woffPath, 'rb') as f:
        woffData = f.read()
    woffDataB64 = b64encode(woffData)

    # make @fontface rule
    fontName = f"RoboType-{style.replace(' ', '')}"
    fontFace = f'''\
@font-face {{
    font-family: "{fontName}";
    src: url(data:application/x-font-woff;charset=utf-8;base64,{woffDataB64.decode()}) format("woff");
    font-style: normal;
    font-weight: normal;
}}

'''
    css += fontFace

# save to scss file
with open(scssPath, mode='w', encoding='utf-8') as f:
    f.write(css)
