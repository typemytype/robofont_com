import drawBot as dB
from math import cos, sin, radians

MARGIN = 50

RED = '#ff5e56'
YELLOW = '#ffbd2e'
GREEN = '#28ca41'

def background(hexClr='#ffffff'):
    dB.fill(*HEX2RGB(hexClr))
    dB.rect(0, 0, dB.width(), dB.height())

def HEX2RGB(hexColor):
    offset = 1 if hexColor.startswith('#') else 0
    rr = int(hexColor[offset:offset+2], 16)
    gg = int(hexColor[offset+2:offset+4], 16)
    bb = int(hexColor[offset+4:], 16)
    return rr/255, gg/255, bb/255

def regularPolygon(sides, radius):
    vertices = []
    sliceAngle = 360/sides
    for ii in range(sides):
        xx = cos(radians(ii*sliceAngle)) * radius
        yy = sin(radians(ii*sliceAngle)) * radius
        vertices.append((xx, yy))
    dB.polygon(*vertices)


def lineAttributes(thickness=1):
    dB.strokeWidth(thickness)
    dB.stroke(0)
    dB.fill(None)

def shapeAttributes(hexClr='#000000'):
    dB.fill(*HEX2RGB(hexClr))
    dB.stroke(None)

def typeAttributes(hexClr='#000000', pointSize=14):
    shapeAttributes(hexClr)
    dB.font('RoboType-Roman', pointSize)

def drawButtons():
    with dB.savedState():
        dB.translate(10, 0)
        for hexClr in [RED, YELLOW, GREEN]:
            shapeAttributes(hexClr)
            dB.oval(0, 408, 10, 10)
            dB.translate(16, 0)

def drawWindow():
    # frame
    lineAttributes(thickness=2)
    dB.rect(0, 0, 600, 400)
    dB.rect(0, 400, 600, 25)

    drawButtons()

    # window title
    typeAttributes()
    dB.text('My Awesome Tool', (600/2, 408), align='center')

def drawViews():
    # frame
    shapeAttributes('#fffbbb')
    dB.rect(20, 20, 160, 360)
    dB.rect(200, 20, 380, 360)

def drawSubViews():
    subViewsClr = '#ff75d4'
    shapeAttributes(subViewsClr)

    # left view
    dB.oval(50, 220, 100, 100)
    dB.rect(50, 90, 100, 100)

    # right view
    dB.oval(240, 90, 90, 90)
    with dB.savedState():
        dB.translate(450, 150)
        dB.rotate(-30)
        regularPolygon(sides=3, radius=80)

    typeAttributes(subViewsClr, 100)
    dB.text('hello', (310, 280))

def drawConnection(pt1, pt2):
    lineAttributes()
    dB.line(pt1, pt2)
    shapeAttributes()
    xx, yy = pt2
    dB.oval(xx-2.5, yy-2.5, 5, 5)

def drawCaptions():
    typeAttributes(pointSize=20)
    quota = 384
    dB.text("Window", (640, quota))
    drawConnection((635, quota+5), (590, quota+5))

    quota = 60
    dB.text("Views", (640, quota))
    drawConnection((635, quota+5), (550, quota+5))

    quota = 160
    dB.text("Layers", (640, quota))
    drawConnection((635, quota+5), (450, quota+5))


if __name__ == '__main__':
    dB.newPage(800, 520)
    background()

    with dB.savedState():
        dB.translate(MARGIN, MARGIN)
        drawWindow()
        drawViews()
        drawSubViews()
        drawCaptions()

    dB.saveImage('../../../images/building-tools/merz/merz_diagram.png')
